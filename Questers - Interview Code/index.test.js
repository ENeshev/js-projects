const { map, filter, reduce }  = require('./main.js');


test('Double the number of the array', () => {
    expect(map([1, 2, 3], (el) => el * 2)).toEqual([2, 4, 6]);
  });

test('Filter Numbers less than 2', () => {
  expect(filter([1, 2, 3], (el) => el < 2)).toEqual([1]);
});

test('Concat User Names', () => {
  const testUsers = [
    {
      name: 'Pesho',
      age: 20,
    },
    {
      name: 'Gosho',
      age: 30,
    },
    {
      name: 'Ivan',
      age: 40,
    },
  ];
  expect(reduce(testUsers, (acc, el) => acc += el.name, '')).toEqual('PeshoGoshoIvan');
});