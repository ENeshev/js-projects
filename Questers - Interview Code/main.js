//Implement the following array methods. Each method should accept an array as the first argument.


const find = (arr, fn) => { 
    for (const element of arr) {
        if (fn(element)) return element;  
    }
}


const map = (arr, fn) => {
    const result = [];

    for (const element of arr) {
        result.push(fn(element));
    }

    return result;
}


const filter = (arr, fn) => {
    const result = [];

    for (const element of arr) {
        if (fn(element)) result.push(element);
    }

    return result;
}

const includes = (arr, value) => {
    
    for (const element of arr) {
        if (element === value) return true;
    }

    return false;
}


const reduce = (arr, fn, init) => {
       
    let result = init;
    
    for (const element of arr) {
        
        result = fn(result, element);
    }

    return result; 
}



module.exports = {
    map,
    filter,
    reduce
}