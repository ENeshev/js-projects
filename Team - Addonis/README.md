# [The Binary Three Dudes Addons](https://addonis-project.web.app/)

![home page view](./src/images/screenshots/1_home.jpg)

## About the app

The BTD Addons is a web app for browsing, buying and downloading/uploading add-ons for VS Code, Atom or PyCharm.  
It is the Final Project for the Telerik Academy Alpha JavaScript track.

### Authors: **The Binary Three Dudes**

- #### [Emil Neshev](https://gitlab.com/ENeshev/js-projects)
- #### [Miroslav Todorov](https://gitlab.com/miro.todorov.telerikacademy)
- #### [Milen Nenkov](https://gitlab.com/milen-nenkov)

## Starting the app

The production build can be accessed on our website: [BTD Addons](https://addonis-project.web.app/home)

To start the local development build, you must first clone the repo

```
mkdir addonis-clone
cd addonis-clone
git clone https://gitlab.com/t9273/addonis.git
```

After that, install all npm packages with

```
npm install
```

And finally run the local server with

```
npm start
```

This will open a new tab in your default browser. Alternately, you can copy/paste the following url to the address bar `localhost:3000`

## Technologies Used

The project is written in JavaScript and React

We have used the following libraries:

- Firebase - Handles back end functionality for Authentication, Database and File Storage
- Material UI - a React UI components library
- FontAwesome - pre-built icons
- React Paypal - PayPal SDK component
- Chart.js - Chart creation tool
- Moment - Utility to help parse and display dates
- HTML React Parser - A parser that converts an HTML string to one or more React elements
- React Customizable Progress Bar - A component for displaying custom progress bars
- React Multi Carousel - A highly customizable carousel component
- React Query - Performant and powerful data synchronization for React
- React Router Dom - A lightweight, fully-featured routing library for the React JavaScript library

## App Views

### Home View

![home view screenshot](./src/images/screenshots/1_home.jpg)

![home view without a logged in user](./src/images/screenshots/1_home_login.jpg)

![home view with user menu opened](./src/images/screenshots/1_home_user_menu.jpg)

### Addons Paginated View

![addons view screenshot](./src/images/screenshots/2_addons.jpg)

![addons sort menu open](./src/images/screenshots/2_addons_filter_sort.jpg)

![searching for addons view](./src/images/screenshots/3_search.jpg)

### Addon Details View

![addon details](./src/images/screenshots/6_addon_details.jpg)

![buying an addon](./src/images/screenshots/8_addon_purchase_paypal.jpg)

### Authentication View

![register form](./src/images/screenshots/3_signup_form.jpg)

![login view](./src/images/screenshots/3_login_form.jpg)

### User Profile View

![user info](./src/images/screenshots/5_user_profile.jpg)

![user notifications](./src/images/screenshots/5_user_profile_notifications.jpg)

![user uploaded addons](./src/images/screenshots/5_user_profile_uploads.jpg)

![user uploaded addons stats](./src/images/screenshots/5_user_profile_uploads_stats.jpg)

### Admin Dashboard View

![pending addons view](./src/images/screenshots/7_admin_dashboard_pending_request.jpg)

![all users view](./src/images/screenshots/7_admin_dashboard_users.jpg)

### Donations View

![donations](./src/images/screenshots/9_donation.jpg)

### About View

![about us page](./src/images/screenshots/10_about_info.jpg)
