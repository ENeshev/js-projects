import milenPhoto from '../images/team pics/Milen.jpeg';
import emoPhoto from '../images/team pics/Emo.jpeg';
import miroPhoto from '../images/team pics/Miro.png';


export const aboutInfo = {
  milen: {
    name: 'Milen Nenkov',
    photo: milenPhoto,
    town: 'Veliko Tarnovo',
    currentCity: 'Blagoevgrad',
    biography: `Milen has spent his entire career working in the service industry. 
      During the pandemic he has decided it is time to pursue a different path and enrolled in various Udemy courses -
      most notable 100 Days Of Python and The Complete WebDev Bootcamp. He is currently about to graduate from Telerik Academy
      with an Alpha JavaScript diploma.`,
    mobile: '+359 899 758 578',
    email: 'nenkovmilen@gmail.com',
    github: 'https://gitlab.com/milen-nenkov',
    linkedIn: 'https://www.linkedin.com/in/milen-nenkov-a42528142/',
    facebook: 'https://www.facebook.com/milen.nenkov.5/',
    title: 'CTO, BTDT Inc',
  },
  emo: {
    name: 'Emil Neshev',
    photo: emoPhoto,
    town: 'Sofia',
    currentCity: 'Sofia',
    biography: `Emo is mad about computers. He wanted to become a robot as a child. Alas it didn\'t happened.
    Later he studied at the Computer Science School for Micro processors and
    technologies.\nTook different job opportunities supporting Web site content and marketing and spent hist last 7+
    years as a data analyst and reporting tools designer.\nNow he's pursing his passion for Web development
    ...still hoping for that robot thing when technology will perfect our capabilities to model our society.`,
    email: 'e.neshev@gmail.com',
    linkedIn: 'https://www.linkedin.com/in/emil-neshev-668194118/',
    github: 'https://gitlab.com/ENeshev/js-projects',
    title: 'CEO, BTDT Inc',
  },
  miro: {
    name: 'Miroslav Todorov',
    photo: miroPhoto,
    town: 'Vidin',
    currentCity: 'Sofia',
    biography: `Miro has a background in Telecommunications. He has a Mastery Degree of Telecommunications and almost 10 years
    of experience in Optics Communication. In 2021 he decided to start a new chapter and start preparing for the Alpha JS program
    at Telereik Academy. In September 2021 he was accepted for the program and that's how his journey starts. Till now he, as a 
    member of The Binary Three Dudes, has finished his first JS project - 'Mega Awesome Gif Project '`,
    mobile: '+359 889 17 20 26',
    email: 'miro_todorov_@abv.bg',
    title: 'Board Chairman, BTDT Inc',
    linkedIn: 'https://www.linkedin.com/in/miroslav-todorov-5773b26b/',
    github: 'https://gitlab.com/miro.todorov.telerikacademy',
    facebook: 'https://www.facebook.com/miroslav.todorov.73744/',
  },
};