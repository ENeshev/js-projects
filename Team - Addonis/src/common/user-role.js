import { makeSymmetricalEnum } from './makeSymmetricalEnum';

export const userRole = {
  ADMIN: 1,
  DELETED: 2,
  ORDINARY: 3,
  BANNED: 4,
};

makeSymmetricalEnum(userRole);