import React from 'react'
import { Line } from 'react-chartjs-2'
import { Chart, registerables } from 'chart.js';
import Box from '@mui/material/Box';
Chart.register(...registerables);



const getDownloadsPerMonth = ( addon, prop, lowerLimit , upperLimit) =>{
  return Object.values(addon[prop])
  .map(val => new Date(val))
  .filter(date => date >= lowerLimit && date <= upperLimit)
  .reduce((acc, date) => {
    const month = `${date.getFullYear()}-${date.getMonth() + 1}`;
    acc[month] = acc[month] || 0;
    acc[month] = acc[month] + 1;
    return acc;
  }, {});
}

const consecutiveMonths = (startDate, endDate) => {
  const arr = new Array();
  let dt = new Date(startDate);

  while(dt.getTime() < endDate.getTime()) {
    arr.push(`${dt.getFullYear()}-${dt.getMonth() + 1}`)
    dt = new Date(dt.getFullYear(), dt.getMonth() + 1, 1)
  }
  return arr;
}
export default function AddonChart ({addonData, lowerLimit, upperLimit}) {
  
  
  const addon = addonData;
  const lowLimit = lowerLimit || new Date(new Date().getFullYear(), new Date().getMonth() - 12, 1);
  const upLimit = upperLimit || new Date();
  const dateRange = consecutiveMonths(lowLimit, upLimit) 

  const monthsNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  const downloadsPerMonth = getDownloadsPerMonth( addon, "downloads", lowLimit, upLimit );
  const revenuePerMonth = getDownloadsPerMonth(addon, "purchases", lowLimit, upLimit);

  const dataValues = dateRange.map(month => downloadsPerMonth[`${month}`] || 0);
  const dataRevenue = dateRange.map(month => revenuePerMonth[`${month}`] || 0);
  
  return (
    <Box sx={{maxWidth: '100%'}}>
      {addon.price === 'FREE' ? 
      (<Line 
        data={{
          labels: [...dateRange],
          datasets: [
            {
              label: 'Downloads',
              data: [...dataValues],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
              ],
              borderWidth: 2,
            },
          ],
        }}
        height={200}
        width={200}
        options={{
          maintainAspectRatio: false,
          scales: {
            y: {
              beginAtZero: true,
            }
          },
          legend: {
            labels: {
              fontSize: 25,
            },
          },
          plugins: {
            tooltip: {
                callbacks: {
                    title: function(context) {
                      let title = monthsNames[context[0].label.split('-')[1] - 1];
                      return title;
                    },
                    label: function(context) {
                      return context.dataset.label === 'Revenue' ? `Revenue: ${context.raw} $` : `Downloads: ${context.raw}`
                    }
                }
            }
        }
    
        }}
        />) :
      (<Line 
        data={{
          labels: [...dateRange],
          datasets: [
            {
              label: 'Downloads',
              data: [...dataValues],
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
              ],
              borderWidth: 2,
            },
            {
              label: 'Revenue',
              data:[...dataRevenue].map(count => count * addon.price),
              backgroundColor: [
                'rgba(0, 99, 132, 0.2)',
              ],
              borderColor: [
                'rgba(0, 99, 132, 1)',
              ],
              borderWidth: 2,
            }
          ],
        }}
        height={200}
        width={200}
        options={{
          maintainAspectRatio: false,
          scales: {
            y: {
              beginAtZero: true,
            }
          },
          legend: {
            labels: {
              fontSize: 25,
            },
          },
          plugins: {
            tooltip: {
                callbacks: {
                    title: function(context) {
                      let title = monthsNames[context[0].label.split('-')[1] - 1];
                      return title;
                    },
                    label: function(context) {
                      return context.dataset.label === 'Revenue' ? `Revenue: ${context.raw} $` : `Downloads: ${context.raw}`
                    }
                }
            }
        }
    
        }}
        />)

      }
    </Box>
    
  )
}