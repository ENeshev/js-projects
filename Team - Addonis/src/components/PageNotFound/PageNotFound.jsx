import Container from "@mui/material/Container";
import Grid  from '@mui/material/Grid';
import ErrorCard from "../ErrorCard/ErrorCard";
export default function NotFound() {
  return (
    <Container sx={{ minHeight: 'calc(100vh - 118px)', minWidth: '100%', display:'flex', alignContent:'center'}} >
      <Grid container
      sx={{ display: 'flex', justifyContent: 'center'}}
      >
        <Grid
        item
        display='block'
        alignItems='center'
        mt={5}>
          <ErrorCard header='404 :(' message='The page you were trying to reach does not exist' />
        </Grid>
      </Grid>
      
    </Container>
  )
};
