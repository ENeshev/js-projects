import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import AppContext from '../../providers/AppContext';
import { ThemeProvider, useTheme } from '@mui/material/styles';
import  Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { Add } from '@mui/icons-material';
import { Licenses } from '../../common/Licenses';
import MenuItem from '@mui/material/MenuItem';
import { getTags } from '../../services/tags.services';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import Avatar from '@mui/material/Avatar';
import { addOnState } from '../../common/addOnState';
import { getDownloadURL, ref as storageRef, uploadBytes } from 'firebase/storage';
import { storage } from '../../configs/firebase.config';
import { createPendingAddOn, updatePendingAddonPropURL} from '../../services/addons.services';
import UploadDialog from './UploadDialog';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}
export default function UploadForm() {

  const { userData } = useContext(AppContext);

  const [tags, setTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);

  const [imageDataUrl, setImageDataUrl] = useState(null);
  const theme = useTheme();
  const imageMimeType = /image\/(png|jpg|jpeg|gif)/i;
  const fileMimeType = /application\/(x-zip-compressed)/i;
  const navigate = useNavigate();
  const [openDialog, setOpenDialog] = useState(false);
  let [errMessage, setErrMessage] = useState('');

  const initialValues = {
    name: '',
    file: '',
    url: '',
    license: '',
    downloads: 0,
    tags: [],
    author: userData.handle,
    image: '',
    uploadedOn: new Date().valueOf(),
    state: addOnState.PENDING,
    id: ''
  }

  const [uploadData, setUploadData] = useState(initialValues);


  useEffect(() => {
    getTags()
    .then(res => setTags(Object.keys(res)))
    .catch(e => console.log(e))
  }, [])

  useEffect(() => {
    let fileReader,
    isCancel = false;
  if (uploadData.image) {
    fileReader = new FileReader();
    fileReader.onload = (e) => {
      const { result } = e.target;
      if (result && !isCancel) {
        setImageDataUrl(result);
      }
    };
    fileReader.readAsDataURL(uploadData.image);
  }
  return () => {
    isCancel = true;
    if (fileReader && fileReader.readyState === 1) {
      fileReader.abort();
    }
  };
  }, [uploadData.image])


  const imgChangeHandler = (e) => {
    const file = e.target?.files[0];
    if (!file.type.match(imageMimeType)) {
      setErrMessage('image file type is not valid');
      return;
    }

    setUploadData({...uploadData, image: file})
  };


  const fileChangeHandler = (e) => {
    const file = e.target?.files[0];
    if(!file.type.match(fileMimeType)) {
      setErrMessage(prev => prev + '\n file type is not valid');
      return;
    } else {
      setErrMessage('');
    }

    setUploadData({...uploadData, file: file})
  }


  const handleUpload = (e) => {
    e.preventDefault();

    if(!uploadData.file) {
      setErrMessage('Please choose a file to upload');
      return;
    };

    if(!uploadData.name) {
      setErrMessage('You must provide a name for your Addon');
      return;
    };
    
    return createPendingAddOn(uploadData.name,
      uploadData.url,
      uploadData.license,
      uploadData.downloads,
      uploadData.tags,
      uploadData.author,
      uploadData.uploadedOn,
      uploadData.state)
      .then(res => {
        setUploadData({...uploadData, id : res.id});
        if(uploadData.image) {
          const imageStoreRef = storageRef(storage, `images/addOns/${res.id}/${uploadData.image.name}`);
          
          uploadBytes(imageStoreRef, uploadData.image)
          .then(snapshot => {
            return getDownloadURL(snapshot.ref)
            .then(url => {
              return updatePendingAddonPropURL(res.id, 'image', url);
            })
          })
          .catch(e => console.log(e));
        }

        const addOnStoreRef = storageRef(storage, `addOns/${res.id}/${uploadData.file.name}`);
        uploadBytes(addOnStoreRef, uploadData.file)
        .then(snpshot => {
          return getDownloadURL(snpshot.ref)
          .then(url => {
            return updatePendingAddonPropURL(res.id,'file', url);
          })
        })

        setOpenDialog(true);
        setUploadData(initialValues);
      })
      .catch(e => console.log(e.message));
  };

  const handleTags = (e) => {
    const {
      target: { value },
    } = e;
    
    setUploadData({
      ...uploadData,
      tags: typeof value === 'string' ? value.split(',') : value,
    })
  }

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {openDialog && <UploadDialog navigate={navigate}  openDialog={openDialog}  setOpenDialog={setOpenDialog} />}
      <Container component='main' maxWidth='xs'>
        <Box
        sx={{
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        }}
        >
          <Typography component='h1' variant='h5'>
            Submit Add On for admin review
          </Typography>
          <Box
          component='form'
          noValidate
          onSubmit={handleUpload}
          sx={{ marginTop: 3}}
          >
            <Grid container
            spacing={2}
            margin={2}
            >
              <Grid
              item xs={12}
              sm={12}
              sx={{ width: "60vw"}}
              >
                <TextField
                  name='addOnName'
                  required
                  autoComplete=''
                  fullWidth
                  id='addOnName'
                  label='AddOn Name'
                  onChange={(e) => {
                    setUploadData({...uploadData, name: e.target.value})
                    setErrMessage('');
                  }}
                  value={uploadData.name}
                  autoFocus
                />
              </Grid>              
              <Grid item xs={12} sm={6}>
                <TextField
                id='license'
                select
                label='License'
                value={uploadData.license}
                helperText='Please select your License'
                onChange={(e) => setUploadData({...uploadData, license: e.target.value})}
                fullWidth
                >
                  {Object.keys(Licenses).map((license) => (
                  <MenuItem key={license} value={license}>
                    {license}
                  </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                fullWidth
                label='http://'
                id='http'
                >
                </TextField>
              </Grid>
              <Grid item xs={12}>
                <InputLabel id="tags">Tags</InputLabel>
                <Select
                  labelId="demo-multiple-name-label"
                  id="demo-multiple-name"
                  multiple
                  value={uploadData.tags}
                  onChange={handleTags}
                  input={<OutlinedInput label="Name" />}
                  MenuProps={MenuProps}
                  fullWidth
                >
                  {tags.map((tag) => (
                    <MenuItem
                      key={tag}
                      value={tag}
                      style={getStyles(tag, selectedTags, theme)}
                    >
                      {tag}
                    </MenuItem>
                  ))}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6}> 
                <Button
                component='label'
                startIcon={<Add/>}
                sx={{ maxWidth: '180px', maxHeight: '40px' }}
                > Add File Image
                <input
                  onChange={imgChangeHandler}
                  type={'file'}
                  accept='.jpeg, .jpg, .gif, .png'
                  hidden
                />
                </Button>
                {imageDataUrl && (
                <Avatar
                sx={{ width: '100px', height: '100px' }}
                src={imageDataUrl}
                alt='avatar preview'
                />)}
              </Grid>
              <Grid item xs={12} sm={6}>
                <Button
                component='label'
                startIcon={<Add/>}
                sx={{ maxWidth: '180px', maxHeight: '40px' }}
                > Add File
                <input
                  onChange={fileChangeHandler}
                  type={'file'}
                  accept='.zip, .jar'
                  hidden
                />

                </Button>
                <TextField
                fullWidth
                value={uploadData?.file ? uploadData?.file?.name : '' }
                disabled
                />
              </Grid>
              <Grid item xs={4}>
              {errMessage && <span style={{ color: 'red', textAlign: 'center' } }>{errMessage}</span>}
                <Button
                type='submit'
                onClick={handleUpload}
                variant="outlined"
                >Submit</Button>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>

    </ThemeProvider>
  );
}