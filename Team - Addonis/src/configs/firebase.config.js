import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyCIK_46iageaR2L6aUmdHSgt-7DUO_alkE',
  authDomain: 'addonis-project.firebaseapp.com',
  projectId: 'addonis-project',
  storageBucket: 'gs://addonis-project.appspot.com',
  messagingSenderId: '70822121487',
  appId: '1:70822121487:web:8d95ef12f390d2c098137a',
  databaseURL:
    'https://addonis-project-default-rtdb.europe-west1.firebasedatabase.app/',
};

const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);
