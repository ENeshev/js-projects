import { useContext } from 'react';
import AppContext from '../providers/AppContext';
import { Navigate } from 'react-router';
import { userRole } from '../common/user-role';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Button } from '@mui/material';

export default function Authenticated({ children }) {
  const { user, userData } = useContext(AppContext);

  if (!user) {
    return <Navigate to='/home' />;
  }

  if(!userData.verifiedEmail) {
    return (
      <Box minHeight={'65vh'} textAlign='center' mt={8}>
        <Typography variant='h5'>
          You must verify your email in order to upload addons !
        </Typography>
      </Box>
    )
  }

  if(userData.role === userRole.BANNED) {
    return (
      <Box minHeight={'65vh'} textAlign='center' mt={8}>
        <Typography variant='h5'>
          You are currently Banned. For more information please contact our team.
        </Typography>
        <a href='/about'>
          <Button>Contacts</Button>
        </a>
      </Box>
    )
  }

  return children;
}
