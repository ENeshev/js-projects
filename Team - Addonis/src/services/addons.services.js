import { db } from '../configs/firebase.config';
import {
  get,
  push,
  ref,
  query,
  equalTo,
  orderByChild,
  update,
} from 'firebase/database';
import { addOnState } from '../common/addOnState';

export const addonsFromSnapshot = (snapshot) => {
  if (!snapshot.exists()) {
    return [];
  }

  const addonSnapshot = snapshot.val();

  return Object.keys(addonSnapshot).map((addonId) => {
    const addon = addonSnapshot[addonId];

    const averageRating = addon.rating
      ? (
          Object.values(addon.rating).reduce((a, b) => a + b, 0) /
          Object.keys(addon.rating).length
        ).toFixed(2)
      : 0;

    return {
      ...addon,
      uid: addonId,
      author: addon.author,
      file: addon.file,
      image: addon.image,
      license: addon.license,
      name: addon.name,
      state: addon.state,
      uploadedOn: addon.uploadedOn,
      tags: addon.tags ? Object.keys(addon.tags) : [],
      ide: addon.ide ? Object.keys(addon.ide) : [],
      rating: addon.rating ? addon.rating : {},
      averageRating,
      downloads: addon.downloads ? addon.downloads : {},
      downloadCount:
        addon.downloads === 0 ? 0 : Object.keys(addon.downloads).length,
      price: addon.price ? addon.price : 'FREE',
      purchasedBy: addon.purchasedBy ? Object.keys(addon.purchasedBy) : [],
      purchases: addon.purchasedBy ? addon.purchasedBy : {},
    };
  });
};

export const getAllAddons = () => {
  return get(query(ref(db, 'addons')));
};

export const getAllPendingAddons = () => {
  return get(
    query(ref(db, 'addons'), orderByChild('state'), equalTo(addOnState.PENDING))
  )
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return addonsFromSnapshot(snapshot);
    })
    .catch(console.error);
};

export const getAllApproveAddons = () => {
  return get(
    query(
      ref(db, 'addons'),
      orderByChild('state'),
      equalTo(addOnState.APPROVED)
    )
  )
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return addonsFromSnapshot(snapshot);
    })
    .catch(console.error);
};

export const getAllAddonsByStatus = (status) => {
  return get(query(ref(db, 'addons'), orderByChild('state'), equalTo(status)))
    .then((snapshot) => {
      if (!snapshot.exists()) return [];
      return addonsFromSnapshot(snapshot);
    })
    .catch(console.error);
};

export const createAddOn = (
  name,
  price,
  url,
  license,
  downloads,
  tags,
  ide,
  author,
  state
) => {

  return push(ref(db, 'addons'), {
    name,
    price,
    url,
    license,
    downloads,
    tags: tags.reduce((acc, tag) => {
      acc[tag] = true;
      return acc;
    }, {}),
    ide: ide.reduce((acc, id) => {
      acc[id] = true;
      return acc;
    }, {}),
    author,
    uploadedOn: new Date().valueOf(),
    state,
  }).then((result) => {
    return {id: result.key};

  });
};

export const getAddonsByAuthor = (handle) => {
  return get(
    query(ref(db, 'addons'), orderByChild('author'), equalTo(handle))
  ).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }
    return addonsFromSnapshot(snapshot);
  });
};

export const getAddonById = (id) => {
  return get(ref(db, `addons/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Addon with ${id} does not exist`);
    }

    const addOn = result.val();
    addOn.uploadedOn = new Date(addOn.uploadedOn);
    addOn.id = id;

    return addonsFromSnapshot(result)[0];
  });
};

export const getAddonByName = (name) => {
  return get(
    query(ref(db, 'addons'), orderByChild('name'), equalTo(name))
  ).then((snapshot) => {
    if (!snapshot.exists()) {
      return;
    }
    return addonsFromSnapshot(snapshot)[0];
  });
};

export const updateAddonPropURL = (addonId, prop, url) => {
  return update(ref(db), { [`addons/${addonId}/` + prop]: url });
};

export const editExistingAddon = (uid, name, license, url, tags, ide, price) => {

  update(ref(db), { [`addons/${uid}/name`]: name });
  update(ref(db), { [`addons/${uid}/license`]: license });
  update(ref(db), { [`addons/${uid}/url`]: url });
  update(ref(db), { [`addons/${uid}/price`]: price });
  update(ref(db), {
    [`addons/${uid}/ide`]: ide.reduce((acc, el) => {
      acc[el] = true;
      return acc;
    }, {}),
  });
  update(ref(db), {
    [`addons/${uid}/tags`]: tags.reduce((acc, el) => {
      acc[el] = true;
      return acc;
    }, {}),
  });
};

export const updateDownloads = (addonId, handle) => {
  return update(ref(db, `addons/${addonId}/downloads`), {
    [handle]: new Date().valueOf(),
  });
};

export const updateRating = (addonId, handle, rating) => {
  return update(ref(db, `addons/${addonId}/rating`), { [handle]: rating });
};

export const recordPurchase = (addonId, handle) => {

  return update(ref(db, `addons/${addonId}/purchasedBy`), {
    [handle]: new Date().valueOf(),
  });
};

export const updateAddonState = (addonId, state) => {

  return update(ref(db), {
    [`addons/${addonId}/state`]: state,
  });
};

export const searchAddons = (query) => {
  return getAllAddons().then((snapshot) =>
    addonsFromSnapshot(snapshot).filter((addon) =>
      addon.name.toLowerCase().includes(query)
    )
  );
};

export const addToFeatured = (addonId) => {
  return update(ref(db), {
    [`addons/${addonId}/featured`]: true,
  });
};

export const removeFromFeatured = (addonId) => {
  return update(ref(db), {
    [`addons/${addonId}/featured`]: null,
  });
};
