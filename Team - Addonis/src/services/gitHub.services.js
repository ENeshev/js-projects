export const fetchReadme = (repoPath) => {
  return fetch(`https://api.github.com/repos/${repoPath}/readme`, {
    headers: {
      accept: 'application/vnd.github.v3.html',
    },
  }).then((data) => data.text());
};

export const fetchGithubData = (repoPath) => {
  return fetch(`https://api.github.com/repos/${repoPath}`)
    .then((data) => data.json())
    .catch((e) => console.log(e));
};

export const fetchBranches = (repoPath) => {
  return fetch(`https://api.github.com/repos/${repoPath}/branches`)
    .then((data) => data.json())
    .catch((e) => console.log(e));
};

export const fetchPulls = (repoPath) => {
  return fetch(`https://api.github.com/repos/${repoPath}/pulls`)
    .then((data) => data.json())
    .catch((e) => console.log(e));
};
