import { onValue, push, ref, remove } from 'firebase/database';
import { db } from '../configs/firebase.config';

export const notificationsFromSnapshot = (snapshot) => {
  if (!snapshot.exists()) {
    return [];
  }

  const notifications = snapshot.val();
  return Object.keys(notifications).map((id) => {
    const notification = notifications[id];
    return {
      ...notification,
      sentOn: new Date(notification.sentOn),
      id,
    };
  });
};

export const liveUserNotifications = (handle, listen) => {
  return onValue(ref(db, `users/${handle}/notifications`), listen);
};

export const addUserNotification = (handle, content, author) => {
  return push(ref(db, `users/${handle}/notifications`), {
    content,
    author,
    sentOn: Date.now(),
  });
};

export const deleteNotification = (handle, id) => {
  return remove(ref(db, `users/${handle}/notifications/${id}`));
};
