import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import AboutCard from '../../components/AboutCard/AboutCard';
import { aboutInfo } from '../../common/aboutPeopleInfo';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import CallIcon from '@mui/icons-material/Call';
import { constants } from '../../common/constants';
import { Box } from '@mui/system';

export default function About() {
  return (
    <Container>
      <Typography mt={5} variant='h4' align='center'>
        An awesome market place for all your favorite addons brought to you by
        The BTD team
      </Typography>
      <Grid container p={9} direction='row'>
        {Object.keys(aboutInfo).map((user, index) => {
          return (
            <Grid key={index} item p={2}>
              <AboutCard userInfo={aboutInfo[user]} />
            </Grid>
          );
        })}
      </Grid>
      <Grid
        container
        direction='column'
        sx={{ display: 'flex', justifyContent: 'center' }}
      >
        <Grid item>
          <Box style={{ display: 'flex', justifyContent: 'center' }}>
            <img src={constants.BTD_LOGO} alt='btd_logo' />
          </Box>
        </Grid>
        <Grid item ml='auto' mr='auto' mb={2}>
          <Grid container>
            <Grid item display={'flex'} alignItems='center'>
              <AlternateEmailIcon fontSize='medium' sx={{ color: '#1877f2' }} />
              <Typography
                sx={{
                  display: 'inline-flex',
                  alignItems: 'center',
                  marginRight: 3,
                }}
              >
                btd@yourservice.com
              </Typography>
            </Grid>
            <Grid item display='flex' alignItems={'center'}>
              <CallIcon fontSize='medium' sx={{ color: '#1877f2' }} />
              <Typography sx={{ display: 'inline-flex', alignItems: 'center' }}>
                +359 889 887 684
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
}
