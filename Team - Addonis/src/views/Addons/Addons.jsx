import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Pagination from '@mui/material/Pagination';
import KeyboardArrowDown from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUp from '@mui/icons-material/KeyboardArrowUp';
import { useEffect, useState } from 'react';
import AddonCard from '../../components/AddonCard/AddonCard';
import {
  getAllAddonsByStatus,
  searchAddons,
} from '../../services/addons.services';
import { constants } from '../../common/constants';
import { useLocation, useSearchParams } from 'react-router-dom';
import { addOnState } from '../../common/addOnState';
import { fetchGithubData } from '../../services/gitHub.services';

export default function Addons({ pending }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const [addons, setAddons] = useState([]);
  const [page, setPage] = useState(1);
  const [pageResults, setPageResults] = useState([]);
  const [addonsByIde, setAddonsByIde] = useState([]);
  const [activeIde, setActiveIde] = useState('all');
  const [isAscending, setIsAscending] = useState(true);
  const open = Boolean(anchorEl);

  const location = useLocation();
  const status = location?.state?.status;

  const [searchParam] = useSearchParams();
  const query = searchParam.get('query');
  const searchIde = searchParam.get('ide');
  const searchTag = searchParam.get('tag');

  useEffect(() => {
    if (pending) {
      getAllAddonsByStatus(addOnState.PENDING)
        .then((res) => {
          setAddons(res);
          setAddonsByIde(res);
        })
        .catch(console.error);
    }
  }, [pending]);

  useEffect(() => {
    if (status) {
      getAllAddonsByStatus(addOnState.APPROVED)
        .then((res) => {
          setAddons(res);
          setAddonsByIde(res);
        })
        .catch(console.error);
    }
  }, [status]);

  useEffect(() => {
    if (query) {
      searchAddons(query).then((res) => {
        const approvedAddons = res.filter(
          (addon) => addon.state === addOnState.APPROVED
        );
        setAddons(approvedAddons);
        setAddonsByIde(approvedAddons);
      });
    }
  }, [query]);

  useEffect(() => {
    if (searchIde) {
      // setActiveIde(searchIde);
      getAllAddonsByStatus(addOnState.APPROVED).then((res) => {
        const filtered = res.filter((addon) => addon.ide.includes(searchIde));
        setAddons(filtered);
        setAddonsByIde(filtered);
      });
    }
  }, [searchIde]);

  useEffect(() => {
    if (searchTag) {
      getAllAddonsByStatus(addOnState.APPROVED).then((res) => {
        const filtered = res.filter((addon) => addon.tags.includes(searchTag));
        setAddons(filtered);
        setAddonsByIde(filtered);
      });
    }
  }, [searchTag]);

  useEffect(() => {
    setPageResults(
      addonsByIde.slice(
        (page - 1) * constants.ADDONS_PER_PAGE,
        page * constants.ADDONS_PER_PAGE
      )
    );
  }, [addonsByIde, page]);

  useEffect(() => {
    const addonPromises = addons.map((addon) => {
      return fetchGithubData(addon.url.replace('https://github.com/', '')).then(
        (data) => ({
          ...addon,
          lastCommit: new Date(data.updated_at).valueOf(),
        })
      );
    });

    Promise.all(addonPromises).then((data) => setAddons(data));
  }, []);

  const handleSortClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleSortClose = () => {
    setAnchorEl(null);
  };

  const handlePage = (event, value) => {
    setPage(value);
  };

  const handleFilterAddonsByIde = (addonIde) => {
    const ideAddons = addons?.filter((addon) => {
      return addon.ide.includes(addonIde);
    });

    setAddonsByIde(ideAddons);
    setPage(1);
    setActiveIde(addonIde);
  };

  const handleAllAddons = () => {
    setAddonsByIde(addons);
    setPage(1);
    setActiveIde('all');
  };

  const handleSortByProp = (prop) => {
    setIsAscending(!isAscending);
    const order = isAscending ? 1 : -1;

    setAddonsByIde(() =>
      [...addons].sort((a, b) => (a[prop] > b[prop] ? order * 1 : order * -1))
    );
    setPage(1);
    handleSortClose();
  };

  return pageResults.length === 0 && !query ? (
    <Box minHeight={'calc(100vh - 144px)'} mt={30} textAlign='center'>
      <img src={constants.SPINNER_URL} alt='loading spinner' />
    </Box>
  ) : (
    <>
      {pending && (
        <Typography variant='h4' textAlign={'center'}>
          Pending Requests
        </Typography>
      )}
      <Box
        minHeight={'calc(100vh - 144px)'}
        maxWidth={1000}
        mt={3}
        ml={'auto'}
        mr={'auto'}
      >
        <Grid
          container
          justifyContent={'space-between'}
          alignItems={'flex-end'}
        >
          <Grid item>
            <Typography variant='h6'>
              {searchIde ? `Filtered by ${searchIde}` : 'Filter By'}
            </Typography>
            {!searchIde && (
              <ButtonGroup>
                <Button
                  onClick={handleAllAddons}
                  variant={activeIde === 'all' ? 'contained' : 'outlined'}
                >
                  All Addons
                </Button>
                <Button
                  onClick={() => handleFilterAddonsByIde('VS Code')}
                  variant={activeIde === 'VS Code' ? 'contained' : 'outlined'}
                >
                  VS Code
                </Button>
                <Button
                  onClick={() => handleFilterAddonsByIde('Atom')}
                  variant={activeIde === 'Atom' ? 'contained' : 'outlined'}
                >
                  Atom
                </Button>
                <Button
                  onClick={() => handleFilterAddonsByIde('PyCharm')}
                  variant={activeIde === 'PyCharm' ? 'contained' : 'outlined'}
                >
                  PyCharm
                </Button>
              </ButtonGroup>
            )}
          </Grid>
          {searchTag && (
            <Grid item pb={'6px'}>
              <Typography variant='subtitle1'>
                {`Showing results for tag: ${searchTag}`}
              </Typography>
            </Grid>
          )}
          <Grid item>
            <Button
              id='sort-button'
              aria-controls={open ? 'sort-button' : undefined}
              aria-expanded={open ? 'true' : undefined}
              aria-haspopup={true}
              onClick={handleSortClick}
              endIcon={
                anchorEl === null ? <KeyboardArrowDown /> : <KeyboardArrowUp />
              }
            >
              Sort By
            </Button>
            <Menu
              elevation={0}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              id='sort-menu'
              MenuListProps={{ 'aria-labelledby': 'sort-button' }}
              anchorEl={anchorEl}
              open={open}
              onClose={handleSortClose}
            >
              <MenuItem onClick={() => handleSortByProp('name')} disableRipple>
                Name
              </MenuItem>
              <MenuItem
                onClick={() => handleSortByProp('author')}
                disableRipple
              >
                Creator
              </MenuItem>
              <MenuItem
                onClick={() => handleSortByProp('downloadCount')}
                disableRipple
              >
                Downloads
              </MenuItem>
              <MenuItem
                onClick={() => handleSortByProp('uploadedOn')}
                disableRipple
              >
                Uploaded
              </MenuItem>
              <MenuItem
                onClick={() => handleSortByProp('lastCommit')}
                disableRipple
              >
                Last Commit
              </MenuItem>
            </Menu>
          </Grid>
        </Grid>
        <Box maxWidth={1000} mt={3} ml={'auto'} mr={'auto'}>
          {query && (
            <Typography mb={2} variant='h6'>
              Searching for: {query}
            </Typography>
          )}
          <Grid container rowGap={2} mb={3}>
            {query && pageResults.length === 0 ? (
              <Typography variant='h4'>No results found</Typography>
            ) : (
              pageResults.map((addon) => (
                <Grid key={addon.uid} item lg={3} md={4} sm={6}>
                  <AddonCard
                    title={addon.name ? addon.name : 'N/A'}
                    key={addon.uid}
                    author={addon.author ? addon.author : 'N/A'}
                    downloads={addon.downloadCount}
                    rating={+addon.averageRating}
                    imgSrc={
                      addon.image ? addon.image : constants.DEFAULT_LOGO_URL
                    }
                    price={addon.price ? addon.price : 'FREE'}
                  />
                </Grid>
              ))
            )}
          </Grid>
          <Grid container mb={3} justifyContent={'center'}>
            {addonsByIde?.length > constants.ADDONS_PER_PAGE && (
              <Grid item>
                <Pagination
                  count={Math.ceil(
                    addonsByIde.length / constants.ADDONS_PER_PAGE
                  )}
                  defaultPage={1}
                  page={page}
                  onChange={handlePage}
                  siblingCount={0}
                />
              </Grid>
            )}
          </Grid>
        </Box>
      </Box>
    </>
  );
}
