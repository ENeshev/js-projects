import { PayPalButtons, usePayPalScriptReducer } from '@paypal/react-paypal-js';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import ProgressBar from 'react-customizable-progressbar';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import './Donate.css';
import { constants } from '../../common/constants';
import { useContext, useEffect, useState } from 'react';
import AppContext from '../../providers/AppContext';
import {
  donationsFromSnapshot,
  liveDonations,
  recordDonation,
} from '../../services/donation.services';
import moment from 'moment';

export default function Donate() {
  const { userData } = useContext(AppContext);

  const [{ options }, dispatch] = usePayPalScriptReducer();

  const [donationAmount, setDonationAmount] = useState(null);
  const [donations, setDonations] = useState([]);

  useEffect(() => {
    return liveDonations((snapshot) => {
      setDonations(donationsFromSnapshot(snapshot));
    });
  }, []);

  const handleDonation = () => {
    const user = userData?.handle ? userData.handle : 'Anonymous';
    recordDonation(user, donationAmount);
  };

  const handleChange = (e) => {
    setDonationAmount(e.target.value);
    dispatch({
      type: 'resetOptions',
      value: {
        ...options,
      },
    });
  };

  const monthlyDonations = donations
    .filter(
      (donation) => donation.createdOn >= moment().startOf('month').valueOf()
    )
    .reduce((acc, donation) => acc + +donation.amount, 0);

  return (
    <Box minHeight={'72vh'} maxWidth={1200} ml={'auto'} mr={'auto'}>
      <Grid container rowSpacing={6} ml={1} mt={5}>
        <Grid item display='flex' alignItems={'center'} xs={6}>
          <Grid container>
            <Grid item mb={10}>
              <Typography variant='h5'>
                Please help us maintain this website by making a small donation
              </Typography>
            </Grid>
            <Grid item>
              <Grid container>
                <Grid item mr={20}>
                  <FormControl>
                    <FormLabel id='demo-controlled-radio-buttons-group'>
                      Donation Amount
                    </FormLabel>
                    <RadioGroup
                      aria-labelledby='demo-controlled-radio-buttons-group'
                      name='controlled-radio-buttons-group'
                      value={donationAmount}
                      onChange={handleChange}
                    >
                      <FormControlLabel
                        value={2}
                        control={<Radio />}
                        label='$2'
                      />
                      <FormControlLabel
                        value={5}
                        control={<Radio />}
                        label='$5'
                      />
                      <FormControlLabel
                        value={10}
                        control={<Radio />}
                        label='$10'
                      />
                      <FormControlLabel
                        value={20}
                        control={<Radio />}
                        label='$20'
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid>

                <Grid item alignSelf={'center'} width={200}>
                  <PayPalButtons
                    disabled={donationAmount === null}
                    style={{
                      layout: 'horizontal',
                      label: 'donate',
                      color: 'blue',
                    }}
                    createOrder={(_, actions) => {
                      return actions.order
                        .create({
                          purchase_units: [
                            {
                              amount: {
                                value: donationAmount,
                                breakdown: {
                                  item_total: {
                                    currency_code: 'USD',
                                    value: donationAmount,
                                  },
                                },
                              },
                              items: [
                                {
                                  name: 'Donation',
                                  quantity: '1',
                                  unit_amount: {
                                    currency_code: 'USD',
                                    value: donationAmount,
                                  },
                                  category: 'DONATION',
                                },
                              ],
                            },
                          ],
                        })
                        .then((orderId) => {
                          return orderId;
                        });
                    }}
                    onApprove={(data, actions) => {
                      return actions.order
                        .capture()
                        .then(() => handleDonation());
                    }}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item display={'flex'} ml={15} alignItems={'center'}>
          <ProgressBar
            radius={100}
            steps={constants.MONTHLY_DONATION_TARGET}
            progress={
              monthlyDonations > constants.MONTHLY_DONATION_TARGET
                ? constants.MONTHLY_DONATION_TARGET
                : monthlyDonations
            }
            strokeWidth={12}
            strokeColor={
              monthlyDonations < constants.MONTHLY_DONATION_TARGET
                ? monthlyDonations > constants.MONTHLY_DONATION_TARGET / 2
                  ? 'yellow'
                  : 'red'
                : 'green'
            }
            strokeLinecap='round'
            trackStrokeWidth={14}
            trackStrokeLinecap='round'
            cut={120}
            rotate={-210}
          >
            <Typography variant='body1' className='indicator'>
              {monthlyDonations > constants.MONTHLY_DONATION_TARGET
                ? 'Target met 🎉🥳🎉'
                : `$${monthlyDonations} out of $${constants.MONTHLY_DONATION_TARGET}`}
            </Typography>
          </ProgressBar>
        </Grid>
      </Grid>
      <Box mt={6}>
        <Typography variant='h6'>Top 3 contributors</Typography>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: '500px' }}>
            <TableBody>
              {[...donations]
                .sort((a, b) => (a.amount > b.amount ? -1 : 1))
                .slice(0, 3)
                .map((donation) => (
                  <TableRow
                    key={donation.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component={'th'} scope='row'>
                      {donation.donatedBy}
                    </TableCell>
                    <TableCell>
                      {moment(donation.createdOn).format('lll')}
                    </TableCell>
                    <TableCell>{`$${donation.amount}`}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Box>
  );
}
