// import * as React from 'react';
import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';
import { styled, alpha } from '@mui/material/styles';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Badge from '@mui/material/Badge';
import AppContext from '../../providers/AppContext';
import { logOutUser } from '../../services/auth.services';
import {
  liveUserNotifications,
  notificationsFromSnapshot,
} from '../../services/notification.services';
import { userRole } from '../../common/user-role';
import { addOnState } from '../../common/addOnState';
import { constants } from '../../common/constants';

// here is some code for Search
const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));
export default function NavigationBar() {
  const { user, userData } = useContext(AppContext);
  let handle;
  if (user) {
    handle = userData.handle;
  }

  const { setContext } = useContext(AppContext);
  const navigate = useNavigate();

  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [notificationsCount, setNotificationsCount] = useState(0);
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    return liveUserNotifications(handle, (snapshot) => {
      if (!snapshot.exists()) {
        setNotificationsCount(0);
      }
      setNotificationsCount(notificationsFromSnapshot(snapshot).length);
    });
  }, [notificationsCount.length, handle]);

  // here starts the state to control the user since we implement real authentication
  const handleLoginClick = () => {
    navigate('/signIn');
  };

  const handleLogOutClick = () => {
    logOutUser()
      .then(() => {
        setContext({
          user: null,
          userData: null,
        });
        navigate('home');
      })
      .catch(console.error);
  };
  // here ends the state to control the user

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleHome = () => {
    handleCloseNavMenu();
    navigate('home');
  };

  const navigateToProfile = () => {
    handleCloseUserMenu();
    navigate(`/users/${userData.handle}`);
  };

  const navigateToAddons = () => {
    handleCloseNavMenu();
    navigate('/addons', { state: { status: addOnState.APPROVED } });
  };

  const navigateToDashboard = () => {
    // handleCloseUserMenu();
    navigate('dashboard');
  };

  const navigateToUpload = () => {
    navigate('upload');
  };

  const handleSearch = (e) => {
    if (e.nativeEvent.code !== 'Enter') {
      setSearchQuery(e.target.value);
    } else {
      setSearchQuery('');
      navigate(`/addons?query=${searchQuery}`);
    }
  };

  return (
    <AppBar position='static'>
      <Container maxWidth='xl'>
        <Toolbar disableGutters>
          {/* Here starts NavBar responsive part menu */}
          {/* <AdbIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} /> */}
          <img
            src={constants.BTD_AVATAR}
            alt='team_logo'
            style={{ width: 43, marginRight: 5 }}
          />
          <Typography
            variant='h6'
            noWrap
            component='a'
            href='/'
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            BTD
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size='large'
              aria-label='account of current user'
              aria-controls='menu-appbar'
              aria-haspopup='true'
              onClick={handleOpenNavMenu}
              color='inherit'
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id='menu-appbar'
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              <MenuItem onClick={handleHome}>
                <Typography textAlign='center'>Home</Typography>
              </MenuItem>
              <MenuItem onClick={navigateToAddons}>
                <Typography textAlign='center'>Addons</Typography>
              </MenuItem>
            </Menu>
          </Box>

          {/* Here starts the visible Navbar Menu */}
          {/* <AdbIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} /> */}
          <Typography
            variant='h5'
            noWrap
            component='a'
            href='/'
            sx={{
              mr: 2,
              display: { xs: 'flex', md: 'none' },
              flexGrow: 1,
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
            }}
          >
            BTD
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            <Button
              onClick={handleHome}
              sx={{ my: 2, color: 'white', display: 'block' }}
            >
              Home
            </Button>
            <Button
              onClick={navigateToAddons}
              sx={{ my: 2, color: 'white', display: 'block' }}
            >
              Addons
            </Button>
          </Box>

          {/* Search Field starts here */}
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              onChange={handleSearch}
              onKeyDown={handleSearch}
              value={searchQuery}
              placeholder='Search…'
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>

          {/* Here starts the Login Button */}

          {!user && (
            <Button onClick={handleLoginClick} color='inherit'>
              Login
            </Button>
          )}

          {/* Here starts the profile layout and menu */}
          {user && (
            <Box sx={{ flexGrow: 0 }}>
              {/* notification Icon  */}
              <IconButton
                size='large'
                aria-label='show new notifications'
                color='inherit'
                onClick={() =>
                  navigate(`users/${userData.handle}`, {
                    state: { notifications: true },
                  })
                }
              >
                <Badge badgeContent={notificationsCount} color='error'>
                  <NotificationsIcon />
                </Badge>
              </IconButton>
              <Tooltip title='Open settings'>
                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  {userData.avatarURL ? (
                    <Avatar alt='user avatar' src={`${userData.avatarURL}`} />
                  ) : (
                    <Avatar
                      alt='user avatar'
                      src={`https://robohash.org/${handle}?set=set3`}
                    />
                  )}
                </IconButton>
              </Tooltip>
              <Menu
                sx={{ mt: '45px' }}
                id='menu-appbar'
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                <MenuItem onClick={navigateToProfile}>
                  <Typography textAlign='center'>Profile</Typography>
                </MenuItem>
                <MenuItem onClick={handleCloseUserMenu}>
                  <Typography onClick={navigateToUpload} textAlign='center'>
                    Upload
                  </Typography>
                </MenuItem>
                {userData.role === userRole.ADMIN && (
                  <MenuItem onClick={handleCloseUserMenu}>
                    <Typography
                      onClick={navigateToDashboard}
                      textAlign='center'
                    >
                      Dashboard
                    </Typography>
                  </MenuItem>
                )}
                <MenuItem onClick={handleCloseUserMenu}>
                  <Typography onClick={handleLogOutClick} textAlign='center'>
                    Logout
                  </Typography>
                </MenuItem>
              </Menu>
            </Box>
          )}
        </Toolbar>
      </Container>
    </AppBar>
  );
}
