// import * as React from 'react';
import { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
// import FormControlLabel from '@mui/material/FormControlLabel';
// import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import AppContext from '../../providers/AppContext';
import { loginUser } from '../../services/auth.services';
import { getUserData, verifiedMail } from '../../services/users.services';

const theme = createTheme();

export default function SignIn() {
  const { userData, setContext } = useContext(AppContext);
  const navigate = useNavigate();
  let [errMessage, setErrMessage] = useState('');

  const handleLogin = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const formObject = {
      email: data.get('email'),
      password: data.get('password'),
    };
    if (!formObject.email.includes('@')) {
      return setErrMessage('Invalid E-mail !');
    }

    return loginUser(formObject.email, formObject.password)
      .then((u) => {
        return getUserData(u.user.uid).then((snapshot) => {
          if (snapshot.exists()) {
            setContext({
              user: u.user,
              userData: snapshot.val()[Object.keys(snapshot.val())[0]],
            });
            navigate('../home');

            if (userData.verifiedMail !== u.user.emailVerified) {
              verifiedMail(userData.handle, u.user.emailVerified);
              setContext({
                user: u.user,
                userData: {
                  ...userData,
                  verifiedEmail: u.user.emailVerified,
                },
              });
            }
            // navigate('../home');
          }
        });
      })
      .catch((error) => {
        if (error.message.includes('user-not-found')) {
          return setErrMessage('Wrong E-mail !');
        }
        if (error.message.includes('wrong-password')) {
          return setErrMessage('Wrong password !');
        }
      })
      .catch(console.error);
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
          minHeight={'69vh'}
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component='h1' variant='h5'>
            Sign in
          </Typography>
          <Box
            component='form'
            onSubmit={handleLogin}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin='normal'
              required
              fullWidth
              id='email'
              label='Email Address'
              name='email'
              autoComplete='email'
              autoFocus
            />
            <TextField
              margin='normal'
              required
              fullWidth
              name='password'
              label='Password'
              type='password'
              id='password'
              autoComplete='current-password'
            />
            {errMessage && <span style={{ color: 'red' }}>{errMessage}</span>}
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item>
                <Link href='/signUp' variant='body2'>
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        {/* <Copyright sx={{ mt: 8, mb: 4 }} /> */}
      </Container>
    </ThemeProvider>
  );
}
