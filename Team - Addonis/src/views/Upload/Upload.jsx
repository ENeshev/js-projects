import { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { ThemeProvider, useTheme } from '@mui/material/styles';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Add from '@mui/icons-material/Add';
import { Licenses } from '../../common/Licenses';
import MenuItem from '@mui/material/MenuItem';
import { Tags } from '../../common/Tags';
import Avatar from '@mui/material/Avatar';
import InputAdornment from '@mui/material/InputAdornment';

import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from 'firebase/storage';
import { storage } from '../../configs/firebase.config';
import {
  createAddOn,
  updateAddonPropURL,
} from '../../services/addons.services';
import UploadDialog from '../../components/UploadDialog/UploadDialog';
import AppContext from '../../providers/AppContext';
import { addToUserAddonList } from '../../services/users.services';
import Alert from '@mui/material/Alert';
import { addUserNotification } from '../../services/notification.services';
import { addOnState } from '../../common/addOnState';
import { IDEs } from '../../common/IDEs';

export default function Upload() {
  const { userData } = useContext(AppContext);
  const [tags, setTags] = useState(Object.keys(Tags));
  const ides = Object.keys(IDEs);

  const [imageDataUrl, setImageDataUrl] = useState(null);
  const theme = useTheme();
  const imageMimeType = /image\/(png|jpg|jpeg|gif|svg)/i;
  const fileMimeType = /application\/(x-zip-compressed)/i;
  const navigate = useNavigate();
  const [openDialog, setOpenDialog] = useState(false);
  let [errMessage, setErrMessage] = useState('');

  const initialValues = {
    name: '',
    price: '',
    file: '',
    url: '',
    license: '',
    downloads: 0,
    tags: [],
    ide: [],
    author: userData?.handle,
    image: '',
    uploadedOn: new Date().valueOf(),
    state: addOnState.PENDING,
  };

  const [uploadData, setUploadData] = useState(initialValues);

  useEffect(() => {
    let fileReader,
      isCancel = false;
    if (uploadData.image) {
      fileReader = new FileReader();
      fileReader.onload = (e) => {
        const { result } = e.target;
        if (result && !isCancel) {
          setImageDataUrl(result);
        }
      };
      fileReader.readAsDataURL(uploadData.image);
    }
    return () => {
      isCancel = true;
      if (fileReader && fileReader.readyState === 1) {
        fileReader.abort();
      }
    };
  }, [uploadData.image]);


  const imgChangeHandler = (e) => {
    const file = e.target?.files[0];
    if (!file.type.match(imageMimeType)) {
      setErrMessage('image file type is not valid');
      return;
    }

    setUploadData({ ...uploadData, image: file });
  };

  const fileChangeHandler = (e) => {
    const file = e.target?.files[0];
    if (!file.type.match(fileMimeType)) {
      setErrMessage((prev) => prev + '\n file type is not valid');
      return;
    } else {
      setErrMessage('');
    }

    setUploadData({ ...uploadData, file: file });
  };

  const handleUpload = (e) => {
    e.preventDefault();

    if (!uploadData.file) {
      setErrMessage('Please choose a file to upload');
      return;
    }

    if (!uploadData.name) {
      setErrMessage('You must provide a name for your Addon');
      return;
    }

    if (!uploadData.url) {
      setErrMessage(
        'You must provide an online location address for your Addon'
      );
      return;
    }

    if (!uploadData.ide) {
      setErrMessage('Your Addon should have IDE selected');
      return;
    }

    return createAddOn(
      uploadData.name,
      +uploadData.price === 0 ? 'FREE' : +uploadData.price,
      uploadData.url,
      uploadData.license,
      uploadData.downloads,
      uploadData.tags,
      uploadData.ide,
      uploadData.author,
      uploadData.state
    )
      .then((res) => { 
        if (uploadData.image) {
          const imageStoreRef = storageRef(
            storage,
            `images/addOns/${res.id}/${uploadData.image.name}`
          );

          uploadBytes(imageStoreRef, uploadData.image)
            .then((snapshot) => {
              return getDownloadURL(snapshot.ref).then((url) => {
                return updateAddonPropURL(res.id, 'image', url);
              });
            })
            .catch((e) => console.log(e.message));
        }

        const addOnStoreRef = storageRef(
          storage,
          `addOns/${res.id}/${uploadData.file.name}`
        );
        uploadBytes(addOnStoreRef, uploadData.file).then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            return updateAddonPropURL(res.id, 'file', url);
          });
        });

        addToUserAddonList(userData.handle, userData.uid, res.id).catch((e) =>
          console.log(e)
        );

        addUserNotification(
          userData.handle,
          `${uploadData.name} Addon added for approval`,
          userData.handle
        );

        setOpenDialog(true);
        setUploadData(initialValues);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleChange = (e = 0, prop) => {
    setUploadData({ ...uploadData, [prop]: e || '' });
  };

  const handleTags = (value) => {
    setUploadData({
      ...uploadData,
      tags: typeof value === 'string' ? value.split(',') : value,
    });
  };

  return (
    <div>
      {userData?.handle && (
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {openDialog && (
            <UploadDialog
              navigate={navigate}
              openDialog={openDialog}
              setOpenDialog={setOpenDialog}
            />
          )}
          <Container component='main' maxWidth='xs'>
            <Box
              sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Typography component='h1' variant='h5'>
                Submit Add On for admin review
              </Typography>
              <Box
                component='form'
                noValidate
                onSubmit={handleUpload}
                sx={{ marginTop: 3 }}
              >
                <Grid container spacing={2} margin={2}>
                  <Grid item xs={12} sm={6} sx={{ width: '60vw' }}>
                    <TextField
                      name='addOnName'
                      required
                      autoComplete='none'
                      fullWidth
                      id='addOnName'
                      label='AddOn Name'
                      
                      onChange={(e) => {
                        handleChange(e.target.value, 'name');
                        setErrMessage('');
                      }}
                      value={uploadData.name}
                      error={uploadData.name === ''}
                      helperText={
                        uploadData.name === '' ? 'Please enter name' : ' '
                      }
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} sx={{width: '10vw'}}> 
                  <TextField
                    label="Price"
                    autoFocus
                    type='number'
                    fullWidth
                    value={uploadData.price}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    }}
                    onChange={(e) => { 
                      const decimalPositive = /^[+]?([0-9]*(?:[\.][0-9]*)?|\.[0-9]+)$/;
                      decimalPositive.test(e.target.value) ? handleChange(e.target.value, 'price') : handleChange(null, 'price');
                    }}
                  />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      id='license'
                      select
                      label='License'
                      value={uploadData.license}
                      helperText='Please select your License'
                      onChange={(e) => handleChange(e.target.value, 'license')}
                      fullWidth
                    >
                      {Object.keys(Licenses).map((license) => (
                        <MenuItem key={license} value={license}>
                          {license}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required={true}
                      fullWidth
                      autoComplete='none'
                      error={uploadData.url === ''}
                      helperText={
                        uploadData.url === ''
                          ? 'Please enter provide a link for online addon storage'
                          : ''
                      }
                      onChange={(e) => {
                        handleChange(e.target.value, 'url');
                        setErrMessage('');
                      }}
                      label='http://'
                      id='http'
                    ></TextField>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                
                    <Stack spacing={3}>
                      <Autocomplete
                        multiple
                        id='tags-outlined'
                        options={tags}
                        onChange={(event, value) => handleTags(value)}
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label='Select Tag'
                            placeholder='Select Tag'
                          />
                        )}
                      />
                    </Stack>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Stack spacing={3}>
                      <Autocomplete
                        id='ides-outlined'
                        multiple
                        options={ides}
                        onChange={(event, value) => {
                          setUploadData({ ...uploadData, ide: value });
                          setErrMessage('');
                        }}
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField
                            error={!uploadData.ide}
                            helperText={
                              uploadData.ide === '' ? 'Please choose IDE' : ''
                            }
                            {...params}
                            label='Selected IDE'
                            placeholder='Select IDE'
                          />
                        )}
                      />
                    </Stack>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      component='label'
                      startIcon={<Add />}
                      sx={{ maxWidth: '180px', maxHeight: '40px' }}
                    >
                      {' '}
                      Add File Image
                      <input
                        onChange={imgChangeHandler}
                        type={'file'}
                        accept='.jpeg, .jpg, .gif, .png, .svg'
                        hidden
                      />
                    </Button>
                    {imageDataUrl && (
                      <Avatar
                        sx={{ width: '100px', height: '100px' }}
                        src={imageDataUrl}
                        alt='avatar preview'
                      />
                    )}
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Button
                      component='label'
                      startIcon={<Add />}
                      sx={{ maxWidth: '180px', maxHeight: '40px' }}
                    >
                      {' '}
                      Add File
                      <input
                        onChange={fileChangeHandler}
                        type={'file'}
                        accept='.zip, .jar'
                        hidden
                      />
                    </Button>
                    <TextField
                      fullWidth
                      value={uploadData?.file ? uploadData?.file?.name : ''}
                      error={uploadData.file === ''}
                      helperText={
                        uploadData.file === ''
                          ? 'Please select a file to upload'
                          : ''
                      }
                      disabled
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <Button
                      type='submit'
                      onClick={handleUpload}
                      variant='outlined'
                    >
                      Submit
                    </Button>
                  </Grid>
                  {errMessage && <Alert severity='error'>{errMessage}</Alert>}
                </Grid>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      )}
    </div>
  );
}
