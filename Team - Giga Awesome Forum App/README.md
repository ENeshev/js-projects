# The Binary Three Dudes Board Games Forum App

![home page view](./src/images-src/screenshots/Home.PNG)

### Authors: **The Binary Three Dudes**

- #### Emil Neshev
- #### Miroslav Todorov
- #### Milen Nenkov

## Starting the app

The BTD Board Games Forum is a SPA built for the Front End Project from the Alpha JavaScript Track of Telerik Academy.  
It has the following functionality:

- User authentication handled by FireBase Auth
- All data is stored in a database from FireBase Realtime
- Public part includes checking out the categories, threads and users views
- Private part includes everything in the public plus posts, news and the map view.
- Registered users can like, dislike all topics and posts. They can also edit and delete their own posts.
- Admins can edit and delete every post plus remove any categories and topics.
- The news view loads data from the NewsAPI and displays the last 20 articles that are tagged with Board Games
- The map view displays a Google Map with markers that show locations of known board game stores. Users can add new locations to the database

Upon cloning, you must install all dependencies by using the `npm install` command in the terminal.  
To run the app: `npm start`. This will run the development build in your browser.

## Technologies Used

The project is written in JavaScript and React

We have used the following libraries:

- Firebase - Handles back end functionality for Authentication and Database Storage
- Ant Design - a React UI library
- FontAwesome - pre-built icons
- React Google Map Api - Google Map components
- React Geocode - geo location for Google Map
- Moment - Utility to help parse and display dates

## App Views

#### Home View

![](./src/images-src/screenshots/Home.PNG)

#### Topics View

![](./src/images-src/screenshots/Topics.PNG)

#### Posts View

![](./src/images-src/screenshots/Posts.PNG)

#### News View

![](./src/images-src/screenshots/News.PNG)

#### Map View

![](./src/images-src/screenshots/Store-Map.PNG)

#### Create Category View

![](./src/images-src/screenshots/Create_Category.PNG)

#### Create Topic View

![](./src/images-src/screenshots/Create_Topic.PNG)

#### Register View

![](./src/images-src/screenshots/SignUp.PNG)

#### Log In View

![](./src/images-src/screenshots/Login.PNG)

#### Users View

![](./src/images-src/screenshots/Users.PNG)

#### User Info View

![](./src/images-src/screenshots/User-Details.PNG)

#### About View

![](./src/images-src/screenshots/About.PNG)
