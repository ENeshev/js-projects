import { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { useAuthState } from 'react-firebase-hooks/auth';
import './App.css';
import Breadcrumbs from './components/Breadcrumbs/Breadcrumbs';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import DetailedArticle from './components/SingleNews/DetailedArticle';
import TopicTile from './components/TopicTile/TopicTile';
import AppContext from './providers/app-state';
import AboutView from './views/AboutView/AboutView';
import HomeView from './views/Home/Home';
import PostView from './views/Post/Post';
import DrawerForm from './views/Login/Login';
import NewsView from './views/News/NewsView';
import CreateTopicView from './views/Topics/CreateTopicView';
import TopicsView from './views/Topics/Topics';
import UsersView from './views/Users/UsersView';
import MapView from './views/MapView/MapView';
import CreateCategory from './views/Category/create-category-view';
import UserInfo from './views/Users/UserInfo';

import AccessDenied from './components/Access Denied/AccessDenied';
import { getUserData } from './services/users.services';
import { auth } from './utils/firebase-config';
import SearchResults from './components/Search/search results';
import UserTopics from './views/Topics/User Topics';
import UserPosts from './views/Post/User Posts';
import NotFound from './components/Not Found/Not Found';

//TODO: Search
//TODO: Filter
//TODO: Sort
//TODO: Clean Code
//TODO: Fix CSS
//TODO: Real Topics - Categories - Real DB
//TODO: Create Readme
//TODO: Admin functionality - Ban users or functions
//TODO: Bread crumbs

function App() {
  const [user, setUser] = useState({
    user: null,
    userData: null,
  });

  let [cUser, loading, error] = useAuthState(auth);

  useEffect(() => {
    if (cUser === null) return;

    getUserData(cUser.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setUser({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((e) => console.log(e.message));
  }, [cUser]);

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ user, setUser }}>
        <div className='App'>
          <Header />
          <div className='breadcrumb-container'>
            <Breadcrumbs />
          </div>
          <div className='view-div'>
            <Routes>
              <Route index element={<Navigate replace to='home' />} />
              <Route path='home' element={<HomeView />} />
              {/* <Route path="about" element={<About />} /> */}
              <Route path='login-signUp' element={<DrawerForm />} />{' '}
              {/* Не се отразява */}
              <Route
                path='topics/:cId'
                element={<TopicsView />}
              />
              <Route
                path='post/:tId'
                element={user.user ? <PostView /> : <AccessDenied />}
              />
              <Route
                path='map'
                element={user.user ? <MapView /> : <AccessDenied />}
              />
              <Route
                path='news'
                element={user.user ? <NewsView /> : <AccessDenied />}
              />
              <Route path='users' element={<UsersView />} />
              <Route
                path='userInfo/:handle'
                element={user.user ? <UserInfo /> : <AccessDenied />}
              />
              <Route
                path='article'
                element={<DetailedArticle name='emo' text='poreden opit' />}
              />
              <Route
                path='search'
                element={user.user ? <SearchResults /> : <AccessDenied />}
              />
              <Route path='about' element={<AboutView />} />
              <Route
                path='/topics/:id/create-topic/:cId'
                element={user.user ? <CreateTopicView /> : <AccessDenied />}
              />
              <Route path='users/:handle/topics' element={<UserTopics />} />
              <Route path='users/:handle/posts' element={<UserPosts />} />
              <Route
                path='/home/create-category'
                element={user.user ? <CreateCategory /> : <AccessDenied />}
              />
              <Route path='/access-denied' element={<AccessDenied />} />
              <Route path="*" element={<NotFound />} />
            </Routes>
            {/* <HomeView /> */}
          </div>

          <Footer />
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
