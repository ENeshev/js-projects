export const constants = {
  AVATAR_URL: 'https://robohash.org/',
  USER_FIRST_NAME_MIN_LENGTH: 4,
  USER_FIRST_NAME_MAX_LENGTH: 32,
  ADMIN_FIRST_NAME_MIN_LENGTH: 4,
  ADMIN_FIRST_NAME_MAX_LENGTH: 32,
  POST_TITLE_MIN_LENGTH: 16,
  POST_TITLE_MAX_LENGTH: 64,
  POST_COMMENT_MIN_LENGTH: 32,
  POST_COMMENT_MAX_LENGTH: 8192,
  NEWS_URL: 'https://newsapi.org/v2/everything',
  NEWS_API_KEY: 'de2567ae3f284c1881319c9bd53494b8',
};

export const aboutInfo = {
  milen: {
    name: 'Milen Nenkov',
    town: 'Veliko Tarnovo',
    currentCity: 'Blagoevgrad',
    biography: `Milen has spent his entire career working in the service industry. 
      During the pandemic he has decided it is time to pursue a different path and enrolled in various Udemy courses -
      most notable 100 Days Of Python and The Complete WebDev Bootcamp. He is currently about to graduate from Telerik Academy
      with an Alpha JavaScript diploma.`,
    mobile: '+359 899 758 578',
    email: 'nenkovmilen@gmail.com',
  },
  emo: {
    name: 'Emil Neshev',
    town: 'Sofia',
    currentCity: 'Sofia',
    biography: `Emo is mad about computers. He wanted to become a robot as a child. Alas it didn\'t happend. He spent his
    life fascinated by computers and working with them. Later he studied at the Computer Science School for Micro processors and
    technologies.`,
  },
  miro: {
    name: 'Miroslav Todorov',
    town: 'Vidin',
    currentCity: 'Sofia',
    biography: `Miro has a background in Telecommunications. He has a Mastery Degree of Telecommunications and almost 10 years
    of experience in Optics Communication. In 2021 he decided to start a new chapter and start preparing for the Alpha JS program
    at Telereik Academy. In September 2021 he was accepted for the program and that's how his journey starts. Till now he, as a 
    member of The Binary Three Dudes, has finished his first JS project - 'Mega Awesome Gif Project '`,
    mobile: '+359 889 17 20 26',
    email: 'miro_todorov_@abv.bg',
  },
};
