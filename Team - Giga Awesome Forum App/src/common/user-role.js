import { makeSymmetricalEnum } from './makeSymmetricalEnum';

export const userRole = {
  ADMIN: 1,
  MODERATOR: 2,
  ORDINARY: 3,
  BANNED: 4,
};

makeSymmetricalEnum(userRole);
