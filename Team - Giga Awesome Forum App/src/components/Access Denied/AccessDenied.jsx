import { Drawer } from 'antd';
import { useContext, useState } from 'react';
import AppContext from '../../providers/app-state';
import LogIn from '../Drawer/LogIn Drawer';
import SignUp from '../Drawer/Sign Up Drawer';
import './Access Denied.css'

export default function AccessDenied() {
  const { user, setUser } = useContext(AppContext);
  const [state, setState] = useState(false);
  const [loginState, setLoginState] = useState(false);
  let [drawerTitle, setDrawerTitle] = useState('');

  const handleLoginState = () => {
    setDrawerTitle('Log In');
    setLoginState(true);
    setState(true);
  };

  const handleRegisterState = () => {
    // setRegisterState(true);
    setDrawerTitle('Sign Up');
    setState(true);
  };

  const onClose = () => {
    setLoginState(false);
    setState(false);
  };

  const onSubmit = () => {
    setState(false);
    setUser({ name: 'Pesho' });
    console.log(AppContext.name);
  };

  return (
    <div>
      <div className='denied-access-form'>
        <h2 className='denied-access-msg'>
          Sorry you are not allowed to access this page. The source is restricted
          to private members only.
        </h2>
        <div>
          <div className='suggestions'>
            <div className='login-suggestion'>You can always</div>
            <div className='login-suggestion'>
              <button
                type='button'
                className='switch-btn'
                onClick={handleRegisterState}
              >
                Sign Up
              </button>
            </div>
          </div>
          <div>
            <div className='login-suggestion'>
              Or if you already have an account, please
            </div>
            <div className='login-suggestion'>
              <button
                type='button'
                className='switch-btn'
                onClick={handleLoginState}
              >
                Log In
              </button>
            </div>
          </div>
        </div>
      </div>      
      <Drawer
        className='drawer-form'
        title={drawerTitle}
        width={400}
        style={{ background: (30, 80, 41) }}
        onClose={onClose}
        visible={state}
        bodyStyle={{ paddingBottom: 80 }}
      >
        {loginState ? (
          <LogIn
            onClose={onClose}
            onSubmit={onSubmit}
            setLoginState={setLoginState}
            setDrawerTitle={setDrawerTitle}
          />
        ) : (
          <SignUp
            onClose={onClose}
            onSubmit={onSubmit}
            setLoginState={setLoginState}
            setDrawerTitle={setDrawerTitle}
          />
        )}
      </Drawer>
    </div>
  );
}
