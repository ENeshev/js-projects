import { Breadcrumb } from 'antd';
import './Breadcrumbs.css';
import { useLocation, NavLink } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { getCategoryById } from '../../services/categories.services';
import { getTopicById } from '../../services/topics.services';

export default function Breadcrumbs() {
  const location = useLocation();
  const [pathName, setPathName] = useState(null);
  const [categoryInfo, setCategoryPath] = useState({
    title: null,
    id: null,
  });

  const pathSnippets = location.pathname
    .split('/')
    .filter((i) => i && i !== 'home');

  useEffect(() => {
    switch (pathSnippets[0]) {
      case 'userInfo':
        const name = pathSnippets[pathSnippets.length - 1];
        setPathName(name.slice(0, 1).toUpperCase().concat(name.slice(1)));
        break;

      case 'topics':
        getCategoryById(pathSnippets[pathSnippets.length - 1]).then((res) => {
          setCategoryPath({
            id: pathSnippets[pathSnippets.length - 1],
            title: res.title,
          });
          setPathName(res.title);
        });
        break;

      case 'post':
        getTopicById(pathSnippets[pathSnippets.length - 1]).then((res) =>
          setPathName(res.title)
        );
        break;

      case 'users':
        setPathName('Users');
        break;

      case 'map':
        setPathName('Map');
        break;

      case 'news':
        setPathName('News');
        break;

      case 'search':
        setPathName('Search');
        break;

      case 'about':
        setPathName('About');
        break;

      default:
        setPathName(null);
        break;
    }
  }, [pathSnippets]);

  return (
    <Breadcrumb>
      <Breadcrumb.Item className='breadcrumb-item'>
        <NavLink className='breadcrumb-item' to='/'>
          Home
        </NavLink>
      </Breadcrumb.Item>
      {pathSnippets[0] === 'userInfo' && (
        <Breadcrumb.Item className='breadcrumb-item'>
          <NavLink className='breadcrumb-item' to={`/users`}>
            Users
          </NavLink>
        </Breadcrumb.Item>
      )}
      {pathSnippets[0] === 'post' && (
        <Breadcrumb.Item className='breadcrumb-item'>
          <NavLink
            className='breadcrumb-item'
            to={`/topics/${categoryInfo.id}`}
          >
            {categoryInfo.title}
          </NavLink>
        </Breadcrumb.Item>
      )}
      {pathSnippets.length === 3 ? (
        pathSnippets[2] === 'posts' ? (
          <>
            <Breadcrumb.Item className='breadcrumb-item'>
              <NavLink className='breadcrumb-item' to={'/users'}>
                {pathName?.slice(0, 1).toUpperCase().concat(pathName.slice(1))}
              </NavLink>
            </Breadcrumb.Item>
            <Breadcrumb.Item className='breadcrumb-item'>
              <NavLink
                className='breadcrumb-item'
                to={`/userInfo/${location.state?.accountPosts[0]?.author}`}
              >
                {location.state?.accountPosts[0]?.author}
              </NavLink>
            </Breadcrumb.Item>
            <Breadcrumb.Item className='breadcrumb-item'>
              <NavLink
                className='breadcrumb-item'
                to={`users/${location.state?.accountPosts[0]?.author}/posts`}
              >
                All Posts
              </NavLink>
            </Breadcrumb.Item>
          </>
        ) : (
          <>
            <Breadcrumb.Item className='breadcrumb-item'>
              <NavLink className='breadcrumb-item' to={'users'}>
                {pathName?.slice(0, 1).toUpperCase().concat(pathName.slice(1))}
              </NavLink>
            </Breadcrumb.Item>
            <Breadcrumb.Item className='breadcrumb-item'>
              <NavLink
                className='breadcrumb-item'
                to={`/userInfo/${location.state?.accountTopics[0]?.author}`}
              >
                {location.state?.accountTopics[0]?.author}
              </NavLink>
            </Breadcrumb.Item>
            <Breadcrumb.Item className='breadcrumb-item'>
              <NavLink
                className='breadcrumb-item'
                to={`users/${location.state?.accountTopics[0]?.author}/topics`}
              >
                All Topics
              </NavLink>
            </Breadcrumb.Item>
          </>
        )
      ) : (
        <Breadcrumb.Item className='breadcrumb-item'>
          <NavLink className='breadcrumb-item' to={location}>
            {pathName?.slice(0, 1).toUpperCase().concat(pathName.slice(1))}
          </NavLink>
        </Breadcrumb.Item>
      )}
    </Breadcrumb>
  );
}
