import { Avatar, Card, Popconfirm } from 'antd';
import { constants } from '../../common/constants';
import './CategoryTile.css';
import { faHourglass } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';
import { deleteCategory } from '../../services/categories.services';
import { useContext, useEffect, useState } from 'react';
import AppContext from '../../providers/app-state';
import { getPostById } from '../../services/posts.services';
import { userRole } from '../../common/user-role';
import moment from 'moment';

export default function CategoryTile({
  title,
  subtitle,
  topics,
  id,
  allPosts,
}) {
  const { user } = useContext(AppContext);
  const [lastPostInCategory, setLastPostInCategory] = useState({});
  const cId = id;
  const dateAdded = new Date(lastPostInCategory.createdOn);

  let userRights = false;
  if (user.userData) {
    const currentUserRole = user.userData.role;
    if (currentUserRole === userRole.ADMIN) {
      userRights = true;
    }
  }

  allPosts.sort();

  useEffect(() => {
    if (!allPosts) {
      return setLastPostInCategory({
        author: '-',
        content: 'no posts yet...',
        createdOn: '-',
      });
    }
    const lastPostId = allPosts[allPosts.length - 1];
    if (lastPostId) {
      const lastPostData = async (postId) => {
        try {
          const res = await getPostById(postId);
          return setLastPostInCategory({
            author: `${res.author}`,
            content: `${res.content}`,
            createdOn: `${res.createdOn}`,
          });
        } catch (error) {
          console.log(error.message);
        }
      };
      lastPostData(lastPostId);
    }
  }, []);

  const handleDeleteCategory = (e) => {
    e.preventDefault();
    deleteCategory(topics, cId);
  };

  const hourGlassIcon = <FontAwesomeIcon icon={faHourglass} />;
  return (
    <Card className='CategoryTile' style={{ width: '90%' }}>
      <div className='card-text'>
        <NavLink to={`../topics/${cId}`}>
          <h2 className='card-title'>{title}</h2>
        </NavLink>
        <h4 className='card-subtitle'>{subtitle}</h4>
      </div>
      <div className='card-stats-container'>
        <div className='card-stat'>
          <h2>{topics.length}</h2> <h3>Threads</h3>
        </div>
        <div className='card-stat'>
          <h2>{allPosts.length}</h2> <h3>Posts</h3>
        </div>
      </div>
      {lastPostInCategory.createdOn ? (
        <div className='last-post-container'>
          <div className='last-post-user'>
            <div className='last-post-avatar'>
              <NavLink to={`/userInfo/${lastPostInCategory?.author}`}>
                <Avatar
                  src={
                    constants.AVATAR_URL +
                    `${lastPostInCategory?.author}?size=35x35`
                  }
                  size={40}
                />
              </NavLink>
            </div>
            <h5 className='last-post-author'>{lastPostInCategory.author}</h5>
          </div>
          <div className='last-post-info'>
            <p className='last-post-time'>
              {hourGlassIcon} {moment(dateAdded).fromNow()}
            </p>
            <br></br>
            <p className='last-post-title'>{lastPostInCategory.content.length > 30 ? lastPostInCategory.content.slice(0, 30).concat('...') : lastPostInCategory.content}</p>
          </div>
        </div>
      ) : (
        <div className='last-post-container'>
          <div className='last-post-info'>
            <p className='last-post-title'>
              No posts yet! Be the first to create one
            </p>
          </div>
        </div>
      )}
      {userRights && (
        <Popconfirm
          title='Are you sure?'
          onConfirm={handleDeleteCategory}
          okText='Yes'
          cancelText='No'
        >
          <div className='delete-topic'>
            <button className='nav-btn' name='delete Topic' type='button'>
              X
            </button>
          </div>
        </Popconfirm>
      )}
    </Card>
  );
}
