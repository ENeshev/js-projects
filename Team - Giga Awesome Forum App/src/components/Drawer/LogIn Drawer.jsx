import { Drawer, Form, Button, Row, Input, Space } from "antd";
import { useContext, useState } from "react";
import SignUp from "./Sign Up Drawer";
import { loginUser } from "../../services/auth.services";
import AppContext from "../../providers/app-state";
import { getUserByHandle, getUserData } from "../../services/users.services";

export default function LogIn ({onClose, setLoginState, setDrawerTitle, setState}) {

  const { setUser } = useContext(AppContext);

  const [form, setForm] = useState({
    email: '',
    password: '',
  })

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    })
  }


  const login = (e) => {
    e.preventDefault();

    loginUser(form.email, form.password)
      .then((uc) => {
        setUser({
          user: uc.user,
          userData: null});
        
        return getUserData(uc.user.uid)
          .then((snapshot) => {
            const userObject = snapshot.val();

            setUser({
              user: uc.user,
              userData: userObject[Object.keys(userObject)[0]],
            })
          })
      })
      .catch(console.error);

    setState(false);

  }


  const switchToSignUp = () => {
    setLoginState(false);
    setDrawerTitle('Sign Up')
    return (
      <SignUp />
    )
  }

  return (
    <Form layout="vertical" hideRequiredMark>
          <Row>
            <Form.Item
              name="email"
              label="Email"
              rules={[{ required: true, message: "Please enter your mail" }]}
            >
              <Input
                autoComplete='off'
                className="drawer-form-input"
                placeholder="Please enter your mail"
                onChange={updateForm('email')}
              />
            </Form.Item>
          </Row>
          <Row>
            <Form.Item
              name="password"
              label="Password"
              rules={[
                { required: true, message: "Please enter your password" },
              ]}
            >
              <Input.Password
                className="drawer-form-input"
                placeholder="Please enter your password"
                onChange={updateForm('password')}
              />
            </Form.Item>
          </Row>
          <Row>
            <div className='login-suggestion'>
            Don't have an account?          
            </div>   
            <div className='login-suggestion'>
              <button type='button' className='switch-btn' onClick={switchToSignUp}>Sign Up</button>
              {/* <p onClick={setLoginState(false)}>Sing Up</p> */}
            </div>         
          </Row>
          <Row>
            <Space>
              <Button className='form-btn' onClick={onClose}>Cancel</Button>
              {/* Below onClick prev use function was onSubmit*/}
              <Button className='form-btn' onClick={login} type="primary">
                Login
              </Button>
            </Space>
          </Row>
        </Form>
  );
};

