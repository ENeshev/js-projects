import {Form, Button, Row, Input, Space, notification } from "antd";
import { useContext, useState } from 'react';
import LogIn from "./LogIn Drawer";
import { createUserHandle, getUserByHandle } from '../../services/users.services';
import { registerUser } from '../../services/auth.services';
import AppContext from '../../providers/app-state';
import { constants } from "../../common/constants";
export default function SignUp ({setLoginState, setDrawerTitle, setState}) {

  const {user, setUser} = useContext(AppContext);
  const [f] = Form.useForm();
  const [form, setForm] = useState({
    firstName: '',
    lastName: '',
    email: '',
    handle: '',
    password: '',
    confirmPassword: ''
  })

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    })
  }

  const register = (e) => {
    e.preventDefault();

    if (form.password.length < 6) {
      return alert('Password should be six or more')
    }
    if(form.password !== form.confirmPassword) {
      return alert('Passwords do not match!');
    }


    getUserByHandle(form.handle)
    .then(snapshot => {
      if (snapshot.exists()) {
        console.log(snapshot);
        return alert(`User with ${form.handle} name already exists.`)
      }

      return registerUser(form.email, form.password)
        .then(u => {
          createUserHandle(form.handle, u.user.uid, form.firstName, form.lastName, u.user.email)
            .then(() => {
              notification.success({
                message: 'User created',
                description:`Successfully created user ${form.handle} in Awesome Forum DB`,
                duration: 2,
              });

              getUserByHandle(form.handle)
                .then((s) => {
                  setUser({
                    user: u.user,
                    userData: s.val(),
                  });
                })

            })
            .catch(console.error);

        })
        .catch(console.error);

    })
    .catch(console.error);
    
    setState(false);
  }

  const  handleClose = () => {
    setLoginState(false);
    setState(false);
    f.resetFields();
  }

  const switchToLogIn = () => {
    setLoginState(true);
    setDrawerTitle('Log In')
    return (
      <LogIn />
    );
  }

  return (
    <Form layout="vertical" hideRequiredMark form={f} >
          <Row>
            <Form.Item
              name='First Name'
              label='First Name'
              hasFeedback
              rules={[
                {
                  required: true,
                  message: 'First Name is required',
                },
                () => ({
                  validator(_, value) {
                    if (value.length > constants.USER_FIRST_NAME_MIN_LENGTH && value.length < constants.USER_FIRST_NAME_MAX_LENGTH) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error(`Valid name is between ${constants.USER_FIRST_NAME_MIN_LENGTH} and ${constants.USER_FIRST_NAME_MAX_LENGTH} symbols`));
                  },
                }),
              ]}
              
            >
              <Input
                autoComplete='off'
                className="drawer-form-input"
                onChange={updateForm('firstName')}
              />
            </Form.Item>
          </Row>
          <Row>
            <Form.Item
              name='Last Name'
              label='Last Name'
              rules={[
                {
                  required: true,
                  message: 'Last Name is required',
                },
                () => ({
                  validator(_, value) {
                    if (value.length > constants.USER_FIRST_NAME_MIN_LENGTH && value.length < constants.USER_FIRST_NAME_MAX_LENGTH) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error(`Valid name is between ${constants.USER_FIRST_NAME_MIN_LENGTH} and ${constants.USER_FIRST_NAME_MAX_LENGTH} symbols`));
                  },
                }),
              ]}

            >
              <Input
                autoComplete='off'
                className="drawer-form-input"
                onChange={updateForm('lastName')}
              />
            </Form.Item>
          </Row>
          <Row>
            <Form.Item
              name='User email'
              label='User email'
              rules={[
                {
                  type: 'email',
                  message: 'The input is not valid E-mail!',
                },
                {
                  required: true,
                  message: 'E-mail required',
                },
              ]}
            >
              <Input
                autoComplete='off'
                className="drawer-form-input"
                onChange={updateForm('email')}
              />
            </Form.Item>
          </Row>
          <Row>
            <Form.Item
              name='Username'
              label='Username'
              rules={[{ required: true }]}
            >
              <Input
                autoComplete='off'
                className="drawer-form-input"
                onChange={updateForm('handle')}
              />
            </Form.Item>
          </Row>
          <Row>
            <Form.Item
              name='Password'
              label='Password'
              rules={[{ required: true }]}
            >
              <Input.Password
                className="drawer-form-input"
                onChange={updateForm('password')}
              />
            </Form.Item>
          </Row>
          <Row>
            <Form.Item
              name='Confirm Password'
              label='Confirm Password'
              dependencies={['Password']}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: 'Please confirm your password!',
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('Password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('The two passwords that you entered do not match!'));
                  },
                }),
              ]}
            >
              <Input.Password
                className="drawer-form-input"
                onChange={updateForm('confirmPassword')}
              />
            </Form.Item>
          </Row>
          <Row>
            <div className='login-suggestion'>
            Already have an account?          
            </div>   
            <div className='login-suggestion'>
              <button type='button' className='switch-btn' onClick={switchToLogIn}>Log In</button>
              {/* <p onClick={setLoginState(true)}>Log In</p> */}
            </div>         
          </Row>
          <Row>
            <Space>
              <Button className='form-btn' onClick={handleClose}>Cancel</Button>
              {/* onClick below was initially with onSubmit prop function */}
              <Button className='form-btn' onClick={register} type="primary">
                Submit
              </Button>
            </Space>
          </Row>
        </Form>
  );
};