import { Menu, Dropdown, Button } from 'antd';
import { Fragment } from 'react';
import './Filter.css'

const menu = (
  <Menu
    items={[
      {
        label: (
          <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
            Author
          </a>
        ),
      },
      {
        label: (
          <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
            Likes
          </a>
        ),
      },
      {
        label: (
          <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
            Date
          </a>
        ),
      },
      {
        label: (
          <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
            Tags
          </a>
        ),
      },
    ]}
  />
);


export default function Filter () {
  return (
    <Fragment>
      <Dropdown overlay={menu} placement="bottomLeft" arrow>
        <Button className='filter-btn'>Filter By</Button>
      </Dropdown>
    </Fragment>
  );
}