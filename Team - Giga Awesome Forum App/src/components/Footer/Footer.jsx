import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { Button } from 'antd';
import './Footer.css';

export default function Footer() {
  const heartIcon = <FontAwesomeIcon icon={faHeart} color='red' beat />;

  return (
    <div className='Footer'>
      <p className='attribution'>
        Made with lots of <span className='heart-icon'>{heartIcon}</span> by the
        <Button type='link' size='large' className='about-link'>
          <NavLink to='/about'>
          Binary Three Dudes
          </NavLink>
          {/* Binary Three Dudes */}
        </Button>
        . ©️ {new Date().getFullYear()}
      </p>
    </div>
  );
}
