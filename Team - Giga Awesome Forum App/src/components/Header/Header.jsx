import { Button, Space } from 'antd';
import React, { useContext } from 'react';
import './Header.css';
import { constants } from '../../common/constants';
import 'antd/dist/antd.css';
import DrawerForm from '../../views/Login/Login';
import AppContext from '../../providers/app-state';
import { NavLink } from 'react-router-dom';
import { logoutUser } from '../../services/auth.services';
import Filter from '../Filter/filter';
import SearchField from '../Search/search';

const Header = () => {
  const { user, setUser } = useContext(AppContext);

  const logOut = () => {
    logoutUser().then(() =>
      setUser({
        user: null,
        userData: null,
      })
    );
  };

  return (
    <div className='Header'>
      <h1 className='logo'>BTD Board Games Forum</h1>
      <div className='nav-group'>
        <Space>
          <Button size='large' className='nav-btn'>
            <NavLink to='/home'>Home</NavLink>
          </Button>
          <Button size='large' className='nav-btn'>
            <NavLink to='/news'>News</NavLink>
          </Button>
          <Button size='large' className='nav-btn'>
            <NavLink to='/users'>Users</NavLink>
          </Button>
          {/*Below to be removed after Testing */}
          <Button size='large' className='nav-btn'>
            <NavLink to='/map'>Stores</NavLink>
          </Button>
        </Space>
      </div>
      <div className='search-filter'>
        <SearchField />
        {/* <Filter /> */}
      </div>
      <div className='login-controls'>
        <Space>
          {user.user ? (
            <Button
              size='middle'
              shape='round'
              className='login-btn'
              onClick={logOut}
            >
              LogOut
            </Button>
          ) : (
            <DrawerForm />
          )}
        </Space>
      </div>
      <div className='avatar'>
        <NavLink to={`/userInfo/${user?.userData?.handle}`}>
          <img
            src={
              user?.userData?.handle
                ? constants.AVATAR_URL +
                  `${user?.userData?.handle}.png?size=95x95`
                : constants.AVATAR_URL + 'avatar.png?size=95x95'
            }
            alt='user avatar'
          />
        </NavLink>
      </div>
    </div>
  );
};

export default Header;
