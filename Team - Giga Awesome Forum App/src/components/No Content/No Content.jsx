import './No Content.css'
export default function NoContentYet () {
  return (
    <div className="no-content">
      <p>Opps... It seems there is no content here yet !</p>
      {/* No available content Yet!!! */}
    </div>
  )
};
