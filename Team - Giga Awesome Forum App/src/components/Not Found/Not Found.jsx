import './No Found.css'
export default function NotFound () {
  return (
    <div className="no-content">
      <div>
        <p>404</p>
      </div>
      <p>Opps... It seems there is no such page.</p>
      {/* No available content Yet!!! */}
    </div>
  )
};
