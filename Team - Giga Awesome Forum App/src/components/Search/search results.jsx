import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { getLiveTopics, getTopicsFromSnapshot } from '../../services/topics.services';
import { Fragment } from 'react'
import  TopicTile  from '../TopicTile/TopicTile'
import  NoContentYet  from '../No Content/No Content'
import { filterPostsByProp, getLivePosts, postsFromSnapshot } from '../../services/posts.services';
import { getLiveUsers, getUserByHandle, usersFromSnapshot } from '../../services/users.services';




export default function SearchResults () {
  const location = useLocation()
  const searchTerm = location.state.search;
  const [topics, setTopics] = useState([]);
  const [posts, setPosts] = useState([]);
  const [authors, setAuthors] = useState([])

  
  const [fiteredTopics, setFilteredTopics] = useState([])

  useEffect(()=> {
    console.log(`useEffect topics`);
      getLiveTopics((snapshot) => {
     setTopics(getTopicsFromSnapshot(snapshot));
   });

   console.log(topics);
  }, []);

  useEffect(()=> {
    console.log(`useEffect filtertopics`);

    const result = topics?.filter((topic) => {
      return (topic.title.toLowerCase()).includes(searchTerm.toLowerCase());
    });
    console.log(result);
    return setFilteredTopics(result); 
    }, [topics, searchTerm]);

    useEffect(() => {
      return getLivePosts((snapshot) => {
        setPosts(postsFromSnapshot(snapshot));
      })
    }, []);
    
    useEffect(() => {
      return getLiveUsers((snapshot) => {
        setAuthors(usersFromSnapshot(snapshot));
      });
    }, []);

  const countPostsByTopic = (topicId) => {  

    return posts.filter((post) => {
      return post.topicId === topicId;
    }).length;
  };

  const getLastPost = (topicId) => {
    const topicPosts = posts.filter((post) => {
      return post.topicId === topicId;
    });
    return topicPosts[topicPosts.length -1];
  }

  const getLastPostAuthor = (topicId) => {
    const authorHandle = getLastPost(topicId)?.author;    
    const authorObj = authors.filter((author) => {
      return author.handle === authorHandle;
    });
    return Object.values(authorObj);  
  };

  
  return (
    <Fragment>
       {fiteredTopics.length ? (
      <Fragment>
        {fiteredTopics.map(topic => (
        <TopicTile key={topic.id} {...topic} postsCount={countPostsByTopic(topic.id)}
        lastPost={getLastPost(topic.id)}
        lastPostAuthor={getLastPostAuthor(topic.id)}
        categoryId={topic.categoryId} />
      ))} 
      </Fragment>         
      ) : (
      <NoContentYet />
    )}
    </Fragment>
  //  <div>no content</div>
  );
};