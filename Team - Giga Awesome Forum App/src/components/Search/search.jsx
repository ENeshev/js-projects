import { Input, Space, notification } from 'antd';
import { AudioOutlined } from '@ant-design/icons';
import './search.css';
// import {TopicTile} from '../TopicTile/TopicTile';
// import { getLiveTopics, getTopicsFromSnapshot } from '../../services/topics.services';
import {useNavigate } from 'react-router-dom';
// import { DataSnapshot } from 'firebase/database';
// import { useState } from 'react';

export default function SearchField () {
  const { Search } = Input;
  // const [topics, setTopics] = useState([])
  // let [searchFieldText, setSearchFieldText] = useState('')
  const navigate = useNavigate()

  const suffix = (
    <AudioOutlined
      style={{
        fontSize: 16,
        color: '#1890ff',
      }}
    />
  );


  const onSearch = (value) => {
    if(!value) {
      return notification.error({
        message: 'No word detected',
        description: 'Please enter a topic to search'
      });
    }
  return navigate('search', {state:{search: value}});
  // return setSearchFieldText('') 
  };


  return (
    <Space direction="vertical">
      <Search placeholder='type here to search' onSearch={onSearch} enterButton />
    </Space>
  );
} 