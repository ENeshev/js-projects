import './SingleNews.css';
import { Button, Card } from 'antd';

export default function SingleNews({ title, description, url }) {
  return (
    <div className='single-news'>
      <Card className='single-news-card' title={title} size='small'>
        <p>{description}</p>
        <Button type='link' href={url} target='_blank'>
          Read article
        </Button>
      </Card>
    </div>
  );
}
