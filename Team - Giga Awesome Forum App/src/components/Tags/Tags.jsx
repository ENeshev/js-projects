import { Tag } from 'antd';
import { Fragment, useEffect, useState } from 'react';
import { addTag, getLiveTags, getTagsFromSnapshot } from '../../services/tags.services';
import './Tags.css'

const { CheckableTag } = Tag;

export default function Tags ({handleTags}) {
  // const { CheckableTag } = Tag;
  const [selectedTags, setSelectedTags] = useState([]); // добавя избраните tags към topic-a

  let [newTag, setNewTag] = useState(''); // добавяне на тагове от user-a;
  const [tags, setTags] = useState([]); // взимам тагове от Server


  useEffect(() => {
    return getLiveTags((snapshot) => {
      setTags(getTagsFromSnapshot(snapshot));
    });
  }, []);

  const handleTagName = (e) => {
    console.log();
    setNewTag(e.target.value); 
  };

  const createTag = () => {
    addTag(newTag);
  };

  const handleChange = (tag, checked) => {
    const nextSelectedTags = checked ? [...selectedTags, tag] : selectedTags.filter(t => t !== tag);
    console.log('You are interested in: ', nextSelectedTags);
    setSelectedTags(nextSelectedTags);
    // handleTags(selectedTags);
    handleTags(nextSelectedTags);
  };

    return (
      <>
        <span style={{ marginRight: 8 }}>Categories:</span>
        {!tags.length ? (null) : (
          <Fragment>
            {tags.map(tag => (
            <CheckableTag
              key={tag}
              checked={selectedTags.indexOf(tag) > -1}
              onChange={checked => handleChange(tag, checked)}
            >
              {tag}
            </CheckableTag>
          ))}
          </Fragment>
        )}
        <input type='text' name='tagname' className='tag-input' onChange={handleTagName}></input>
        <button type='button' name='add-tag' className='add-tag-btn' onClick={createTag}>add tag</button>
      </>
    );
  }

