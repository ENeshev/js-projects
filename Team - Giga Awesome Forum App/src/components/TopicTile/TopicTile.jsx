import { Avatar, Card, Popconfirm } from 'antd';
import { constants } from '../../common/constants';
import './TopicTile.css';
import { NavLink } from 'react-router-dom';
import { useContext } from 'react';
import { deleteTopic } from '../../services/topics.services';
import AppContext from '../../providers/app-state';
import { userRole } from '../../common/user-role';
import moment from 'moment';

export default function TopicTile({
  categoryId,
  title,
  author,
  createdOn,
  likedBy,
  dislikedBy,
  postsCount,
  id,
  lastPost,
  lastPostAuthor,
}) {
  const tId = id;
  const { user } = useContext(AppContext);
  const topicDate = new Date(createdOn);
  const lastPostDate = new Date(lastPost?.createdOn);
  let userRights = false;
  if (user.userData) {
    const currentUserRole = user.userData.role;

    if (currentUserRole === userRole.ADMIN || user.userData.handle === author) {
      userRights = true;
    }
  }

  console.log(`userRights: ${userRights}`);
  let likeCount;

  if (likedBy && dislikedBy) {
    likeCount = Object.keys(likedBy).length - Object.keys(dislikedBy).length;
  } else if (likedBy && !dislikedBy) {
    likeCount = Object.keys(likedBy).length;
  } else if (!likedBy && dislikedBy) {
    likeCount = 0 - Object.keys(dislikedBy).length;
  } else if (!likedBy && !dislikedBy) {
    likeCount = 0;
  }

  const handleDeleteTopic = (e) => {
    e.preventDefault();
    deleteTopic(tId, categoryId);
  };

  return (
    <Card className='TopicTile'>
      <div className='left-author'>
        <div className='left-avatar'>
          <NavLink to={`/userInfo/${author}`}>
            <Avatar
              src={constants.AVATAR_URL + `${author}?size=35x35`}
              size={50}
            />
          </NavLink>
        </div>
        <h5 className='left-author-name'>{author}</h5>
      </div>
      <div className='card-text'>
        <NavLink to={`../post/${tId}`}>
          <h2 className='card-title'>{title}</h2>
        </NavLink>
        <h4 className='card-date'>{moment(topicDate).fromNow()}</h4>
      </div>
      <div className='card-stats-container'>
        <div className='card-stat'>
          <h2>{likeCount}</h2> <h3>Likes</h3>
        </div>
        <div className='card-stat'>
          <h2>{postsCount}</h2> <h3>Posts</h3>
        </div>
      </div>
      {postsCount ? (
        <div className='right-topic-container'>
          <div className='right-topic-user'>
            <div className='right-topic-avatar'>
              <NavLink to={`/userInfo/${lastPostAuthor[0]?.handle}`}>
                <Avatar
                  src={
                    constants.AVATAR_URL +
                    `${lastPostAuthor[0]?.handle}?size=35x35`
                  }
                  size={40}
                />
              </NavLink>
            </div>
            <h5 className='right-topic-author'>
              {lastPostAuthor[0]?.handle ? lastPostAuthor[0].handle : null}
            </h5>
          </div>
          <div className='right-topic-info'>
            <p className='right-topic-time'>{moment(lastPostDate).fromNow()}</p>
            <br />
            <p className='right-topic-title'>
              {lastPost && lastPost?.content?.length > 30
                ? lastPost?.content?.slice(0, 30).concat('...')
                : lastPost?.content}
            </p>
          </div>
        </div>
      ) : (
        <div className='right-topic-container'>
          <div className='last-post-info'>
            <p className='last-post-title'>
              No posts yet. Be the first to post here.
            </p>
          </div>
        </div>
      )}
      {userRights && (
        <Popconfirm
          title='Are you sure?'
          onConfirm={handleDeleteTopic}
          okText='Yes'
          cancelText='No'
        >
          <div className='delete-topic'>
            <button className='nav-btn' name='delete Topic' type='button'>
              X
            </button>
          </div>
        </Popconfirm>
      )}
    </Card>
  );
}
