import {
  EditOutlined,
  ThunderboltOutlined,
  GroupOutlined,
  CommentOutlined,
  CloseSquareOutlined,
} from '@ant-design/icons';
import './UserInfoCard.css';
import { Avatar, Input, Form, Button, Radio, notification } from 'antd';
import { useContext, useEffect, useState } from 'react';
import { getUserByHandle } from '../../services/users.services';
import { useParams, useNavigate } from 'react-router-dom';
import { userRole } from '../../common/user-role';
import { updateUserInfo } from '../../services/users.services';
import AppContext from '../../providers/app-state';
import { getTopicsCountByHandle } from '../../services/topics.services';
import { getPostsCountByHandle } from '../../services/posts.services';

const UserInfoCard = () => {
  const { handle } = useParams();
  const navigate = useNavigate();
  // const [owner, setOwner] = useState(false);
  let owner = false;
  const { user } = useContext(AppContext);
  const [editable, setEditable] = useState(false);
  const [topics, setTopics] = useState(0);
  const [posts, setPosts] = useState(0);

  if (
    (user.userData && user?.userData?.handle === handle) ||
    parseInt(user?.userData?.role) === userRole.ADMIN
  ) {
    owner = true;
  }

  const [userInfo, setUserInfo] = useState({
    firstName: null,
    lastName: null,
    email: null,
    handle: null,
    gender: null,
    location: null,
    bio: null,
    uid: null,
    avatar: null,
    createdOn: null,
    role: null,
  });

  useEffect(() => {
    getUserByHandle(handle)
      .then((r) => {
        setUserInfo(r.val());

        getTopicsCountByHandle(handle).then(setTopics);

        getPostsCountByHandle(handle).then(setPosts);
      })
      .catch((e) => console.log(e));
  }, []);

  const [formData, setFormData] = useState({
    gender: '',
    location: '',
    bio: '',
    role: '',
  });

  const updateForm = (prop) => (e) => {
    // e.preventDefault();
    setFormData({
      ...formData,
      [prop]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();

    updateUserInfo(
      userInfo.handle,
      formData.gender || userInfo.gender,
      formData.location || userInfo.location,
      formData.bio || userInfo.bio,
      formData.role || userInfo.role
    )
      .then(() => {
        return getUserByHandle(handle).then((r) => {
          setUserInfo(r.val());
          setEditable(!editable);
          notification.success({
            message: `UPDATED`,
            description: `${userInfo.handle} info updated successfully`,
            duration: 2,
          });
        });
      })
      .catch((e) => console.log(e));
  };

  const onCancel = () => {
    setEditable(!editable);
    // navigate(-1);
  };

  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 16 },
  };

  const edit = () => {
    setEditable(!editable);
  };

  const close = () => {
    navigate(-1);
  };

  const handleTopics = () => {
    return navigate(`/users/${handle}/topics`, {
      state: { accountTopics: topics },
    });
  };

  const handlePosts = () => {
    return navigate(`/users/${handle}/posts`, {
      state: { accountPosts: posts },
    });
  };

  return (
    <div className='UserInfo'>
      {userInfo.firstName !== null && userInfo.firstName !== null && (
        <div className='outer-wrap'>
          <div className='left-content'>
            <div className='avatar-info'>
              <Avatar
                className='userInfo-icon'
                src={userInfo.avatar}
                alt='user-avatar-icon'
              />
            </div>
          </div>
          <div className='right-content'>
            <div className='right-top'>
              <div className='top-tile'>
                <div className='top-content'>
                  <ThunderboltOutlined
                    className='top-icons'
                    style={{ color: 'white' }}
                  />
                  <div className='top-text'>
                    {userRole[userInfo.role]}
                    <br /> Forum Role
                  </div>
                </div>
              </div>
              <div className='top-tile' onClick={handleTopics}>
                <div className='top-content'>
                  <GroupOutlined
                    className='top-icons'
                    style={{ color: 'white' }}
                  />
                  <div className='top-text'>
                    {topics.length}
                    <br /> Topics
                  </div>
                </div>
              </div>
              <div className='top-tile' onClick={handlePosts}>
                <div className='top-content'>
                  <CommentOutlined
                    className='top-icons'
                    style={{ color: 'white' }}
                  />
                  <div className='top-text'>
                    {posts.length}
                    <br />
                    Posts
                  </div>
                </div>
              </div>
            </div>
            <div className='right-bottom'>
              {owner ? (
                <EditOutlined
                  onClick={edit}
                  style={{ color: 'white', fontSize: '200%' }}
                  visible='false'
                />
              ) : null}
              <Form
                {...layout}
                className='user-data'
                name='nest-messages'
                initialValues={{
                  firstName: userInfo.firstName,
                  lastName: userInfo.lastName,
                  location: userInfo.location,
                  gender: userInfo.gender,
                  bio: userInfo.bio,
                  role: userInfo.role,
                }}
              >
                <Form.Item name='firstName' label='First Name'>
                  <Input disabled={true} />
                </Form.Item>
                <Form.Item
                  name='lastName'
                  label='Last Name'
                  rules={[{ required: false }]}
                >
                  <Input disabled={true} />
                </Form.Item>
                <Form.Item name='gender' value={formData.gender} label='Gender'>
                  <Radio.Group
                    onChange={updateForm('gender')}
                    value={formData.gender}
                  >
                    <Radio
                      value='male'
                      style={{ color: 'white' }}
                      disabled={!editable}
                    >
                      male
                    </Radio>
                    <Radio
                      value='female'
                      style={{ color: 'white' }}
                      disabled={!editable}
                    >
                      female
                    </Radio>
                    <Radio
                      value='dragonborn'
                      style={{ color: 'white' }}
                      disabled={!editable}
                    >
                      dragonborn
                    </Radio>
                  </Radio.Group>
                </Form.Item>
                {parseInt(user?.userData?.role) === userRole.ADMIN ? (
                  <Form.Item name='role' value={formData.role} label='Role'>
                    <Radio.Group
                      onChange={updateForm('role')}
                      value={formData.role}
                    >
                      <Radio
                        value={1}
                        style={{ color: 'white' }}
                        disabled={!editable}
                      >
                        Admin
                      </Radio>
                      <Radio
                        value={2}
                        style={{ color: 'white' }}
                        disabled={!editable}
                      >
                        Moderator
                      </Radio>
                      <Radio
                        value={3}
                        style={{ color: 'white' }}
                        disabled={!editable}
                      >
                        Ordinary
                      </Radio>
                      <Radio
                        value={4}
                        style={{ color: 'white' }}
                        disabled={!editable}
                      >
                        Banned
                      </Radio>
                    </Radio.Group>
                  </Form.Item>
                ) : null}

                <Form.Item
                  name='location'
                  label='Location'
                  rules={[{ type: 'address' }]}
                  onChange={updateForm('location')}
                  disabled={!editable}
                >
                  <Input disabled={!editable} />
                </Form.Item>
                <Form.Item name='bio' label='Bio' onChange={updateForm('bio')}>
                  <Input.TextArea disabled={!editable} />
                </Form.Item>
                <Form.Item
                  className='buttons'
                  wrapperCol='{ offset: 8, span: 16 }'
                >
                  <Button
                    type='default'
                    htmlType='submit'
                    onClick={onSubmit}
                    disabled={!editable}
                    style={{ margin: '20px' }}
                  >
                    Submit
                  </Button>
                  <Button
                    type='default'
                    htmlType='button'
                    onClick={onCancel}
                    disabled={!editable}
                  >
                    Cancel
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
          <div className='close-icon'>
            <CloseSquareOutlined
              onClick={close}
              style={{ color: 'white' }}
              fontSize='20px'
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default UserInfoCard;
