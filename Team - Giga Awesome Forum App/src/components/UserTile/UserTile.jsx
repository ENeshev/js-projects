import { Avatar, Card } from 'antd';
import './UserTile.css';
import { NavLink } from 'react-router-dom';

export default function UserTile({
  handle,
  avatar,
  subscribedOn,
  threads,
  posts,
}) {
  return (
    <Card className='UserTile'>
      <div className='user-avatar'>
        <NavLink to={`/userInfo/${handle}`}>
          <Avatar className= 'user-avatar-icon' src={avatar} size={50} />
        </NavLink>
      </div>
      <div className='user-details'>
        <div className='user-handle'>
          <h2>{handle}</h2>
        </div>
        <div className='user-subscribed-on'>
          <h3>Subscribed: {subscribedOn}</h3>
        </div>
      </div>
      <div className='user-activity'>
        <div className='user-threads'>
          <h2>{threads}</h2> <h3>Threads Composed</h3>
        </div>
        <div className='user-posts'>
          <h2>{posts}</h2> <h3>Posts Composed</h3>
        </div>
      </div>
    </Card>
  );
}
