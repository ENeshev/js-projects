import { createContext } from "react";

const AppContext = createContext({
  user: null,
  userData: null,
  setUser: () => {},

});

export default AppContext;