import { ref, push, get, query, equalTo, orderByChild, update, onValue } from 'firebase/database';
import { db } from '../utils/firebase-config';
import { deleteTopic } from './topics.services';

// TODO: get categories from snapshot
export const getCategoriesFromSnapshot = (snapshot) => {
  const categorySnapshot = snapshot.val();

  return Object.keys(categorySnapshot)
    .map((categoryId) => {
      const category = categorySnapshot[categoryId];

      return {
        ...category,
        id: categoryId,
      }
    })
}

// TODO: add category
export const addCategory = (title, subtitle) => {
  // console.log('invoke Add Category');

  return push(
    ref(db, 'categories'),
    {
      title,
      subtitle,
    },
  )
    .then((result) => {
      // console.log(result)
      getCategoryById(result.key);
    });
};


// TODO: get category by Id:
export const getCategoryById = (id) => {

  return get(ref(db, `categories/${id}`))
    .then((result) => {
      if (!result.exists()) {
        throw new Error(`Category with ${id} does not exist`);
      }

      const category = result.val();
      category.id = id;
      // some more logig
      return category;
    });
};

// TODO: get all categories
// export const getAllCategories = () => {
  
//   return get(ref(db, 'categories'))
//     .then((snapshot) => {
//       if (!snapshot.exists()) {
//         return [];
//       }

//       return getCategoriesFromSnapshot(snapshot);
//     });
// };

export const getLiveCategories = (listen) => {
  console.log('gettin live categories')
  return onValue(ref(db, 'categories'), listen);
};

// TODO: get category id by title;

// export const getCategoryIdByTitle = (categoryTitle) => {
//   return get(ref(db, `categories`))
//   .then((result) => {
//     if (!result.exists()) {
//       throw new Error(`No Categories Yet`);
//     }
//     const categories = result.val();
//     const searchId = Object.entries(categories)
//       .filter(([id, {title, subtitle}]) => title === categoryTitle)
//       .pop()[0];
  
//     return searchId;
//   });
// }

// TODO: delete category
export const deleteCategory = (topics, categoryId) => {
  console.log(`topics: ${topics}`);
  console.log(`topics.length: ${topics.length}`);

  console.log(`categoryId: ${categoryId}`);
  topics.map((topic) => {
    if(topic.id) {
      console.log(topic.id);
      deleteTopic(topic.id, categoryId);
    };    
  });

  update(ref(db), {
    [`categories/${categoryId}`]: null,
  });
};




