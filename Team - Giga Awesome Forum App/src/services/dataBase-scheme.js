const dataBase = {
  users: {
    miro: {
      id: 'sdjfoisdfgj49',
      email: 'miro@abv.bg',
      role: 1, // 1, 2, 3
      firstName: 'Miroslav',
      lastName: 'Todorov',
      avatar: 'url',
      registered: 1656261515,
      banned: false, // true, false
      likes: {
        [topicId]: true
      },
      dislikes: {
        [topicId]: true,
      },
    }
  },

  categories: {
    [id]: { // id of the category
      title: 'Games 2022',
      subtitle: 'RnD Games',
      lastPost: 'fjwrfigogwroiferhgione' // id of the last post
    },
  },

  topics: {
    [id]: { // topic id
      title: 'some title here',
      author: 'Milen',
      categoryId: 'kfdjahfuwefowe',
      content: 'some content of the topic',
      createdOn: 16653151166,
      lastPostId: 'dwoidofnoiewfvoiewn',
      likedBy: {
        emo: true,
      },
      tags: {
        [tagname]: true,
        [tagname2]: true,
      },
    },
  },

  posts: {
    [id]: { // post id
      author: 'Emo',
      topicId: 'qoihdwiodiafniofe',
      createOn: 162692626962,
      content: 'here is the content of the post',
      likedBy: {
        emo: true,
        milen: true,
      },
      dislikedBy: {
        miro: true,
      },
    },
  },

  // we should discuss the names of the predefined tags
  tags: {
    sport: {
      [topicId]: true,
    },
    movie: {
      [topic2Id]: true,
    },
    games: {
      [topic3Id]: true,
    },
  },
}
