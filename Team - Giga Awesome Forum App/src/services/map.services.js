import {
  push,
  ref,
  onValue,
  update,
  // eslint-disable-next-line no-unused-vars
  DataSnapshot,
  remove,
} from 'firebase/database';
import { db } from '../utils/firebase-config';

export const getLiveLocations = (callback) => {
  return onValue(ref(db, 'locations'), callback);
};

/**
 *
 * @param {DataSnapshot} snapshot The data snapshot from the database response
 * @returns {{ locationName: string , lat: string, lng: string, city: string, address: string, addedBy: string, id: string }[] } An array of location objects
 */
export const locationsFromSnapshot = (snapshot) => {
  const locations = snapshot.val();

  return Object.keys(locations).map((id) => {
    const location = locations[id];

    return {
      ...location,
      id,
    };
  });
};

/**
 *
 * @param {{ locationName: string , lat: string, lng: string, city: string, address: string, addedBy: string }} location
 */
export const addLocation = (location) => {
  return push(ref(db, 'locations'), location);
};

export const deleteLocation = (locationId) => {
  return remove(ref(db, `locations/${locationId}`));
};

/**
 *
 * @param {string} locationId The uid of the location to update
 * @param {{ locationName: string , lat: string, lng: string, city: string, address: string, addedBy: string }} newLocation
 * The updated values for the location
 * @returns {Promise}
 */
export const updateLocation = (locationId, newLocation) => {
  return update(ref(db, `locations/${locationId}`), newLocation);
};
