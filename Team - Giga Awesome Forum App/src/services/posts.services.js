/* eslint-disable no-unused-vars */

import {
  push,
  get,
  query,
  ref,
  onValue,
  update,
  DataSnapshot,
} from 'firebase/database';
import { db } from '../utils/firebase-config';

/**
 * Gets the post with matching ID from the database
 * @param {string} id The post unique id
 * @returns Promise that resolves with the found post object
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const getPostById = (id) => {
  return get(ref(db, `posts/${id}`))
    .then((res) => {
      if (!res.exists()) {
        throw new Error(`Post with ID ${id} doesn't exist!`);
      }

      const post = res.val();
      post.createdOn = new Date(post.createdOn);

      post.likedBy = post.likedBy ? post.likedBy : {};
      post.dislikedBy = post.dislikedBy ? post.dislikedBy : {};

      return post;
    })
    .catch(console.error);
};

/**
 * Filters the data snapshot objects by given prop and value
 * @param {DataSnapshot} snapshot The snapshot data received by the database
 * @param {string} prop The property to filter by
 * @param {string} value The value to match
 * @returns {{ id: string, author: string, topicId: string, createdOn: Date, content: string, likedBy: [], dislikedBy: [] }[]}
 * An array of the filtered post objects
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const filterPostsByProp = (snapshot, prop, value) => {
  const posts = snapshot.val();

  return Object.keys(posts)
    .filter((post) => {
      return posts[post][prop] === value;
    })
    .map((postID) => {
      const post = posts[postID];
      return {
        ...post,
        id: postID,
        createdOn: new Date(post.createdOn),
        likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
        dislikedBy: post.dislikedBy ? Object.keys(post.dislikedBy) : [],
      };
    });
};

/**
 * Maps the data from the snapshot and returns an array of post objects
 * @param {DataSnapshot} snapshot
 * @returns {{ id: string, author: string, topicId: string, createdOn: Date, content: string, likedBy: [], dislikedBy: [] }[]}
 * An array of the filtered post objects
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const postsFromSnapshot = (snapshot) => {
  const posts = snapshot.val();

  return Object.keys(posts).map((postID) => {
    const post = posts[postID];

    return {
      ...post,
      id: postID,
      createdOn: new Date(post.createdOn),
      likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
      dislikedBy: post.dislikedBy ? Object.keys(post.dislikedBy) : [],
    };
  });
};

/**
 * Listens for changes to the posts database
 * @param {Function} listen The callback to handle the data
 * @returns Unsubscribe from the database
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const getLivePosts = (listen) => {
  return onValue(ref(db, 'posts'), listen);
};

/**
 * Adds the post to the Data Base
 * @param {string} content The text content
 * @param {string} topicID The ID of the corresponding topic
 * @param {string} authorUserName The username of the author
 * @returns Promise that resolves with the added post's unique ID
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const addPost = (content, topicID, authorUserName) => {
  return push(ref(db, 'posts'), {
    content,
    topicId: topicID,
    author: authorUserName,
    createdOn: Date.now(),
  }).then((res) => res.key); // getPostById(res.key)
};

/**
 * Links the post ID to the corresponding topic in the DB
 * @param {string} postId The ID of the post
 * @param {string} topicId The ID of the topic
 *
 * @author Miro Todorov
 */
export const addPostToTopic = (postId, topicId) => {
  return update(ref(db), {
    [`topics/${topicId}/posts/${postId}`]: true,
  });
};

/**
 * Deletes the post and removes the topic reference to it
 * @param {string} postId The Post ID
 * @param {string} topicId The Topic ID
 * @returns Promise that resolves when the DB update is successful
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const removePost = (postId, topicId) => {
  return update(ref(db), {
    [`posts/${postId}`]: null,
    [`topics/${topicId}/posts/${postId}`]: null,
  });
};

/**
 * Updates the text content of the post
 * @param {string} postId The Post ID
 * @param {string} newContent The new content to replace
 * @returns Promise that resolves when the DB update is successful
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const editPostContent = (postId, newContent) => {
  return update(ref(db, 'posts'), {
    [`${postId}/content`]: newContent,
  });
};

/**
 * Adds a reference to the user that liked the post
 * @param {string} postId The Post ID
 * @param {string} userHandle The username to add
 * @returns Promise that resolves when the DB update is successful
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const addLikeToPost = (postId, userHandle) => {
  return getPostById(postId).then((res) => {
    const allLikes = res.likedBy;
    return update(ref(db, `posts/${postId}`), {
      likedBy: {
        ...allLikes,
        [userHandle]: true,
      },
    });
  });
};

/**
 * Removes a reference to the user that liked the post
 * @param {string} postId The Post ID
 * @param {string} userHandle The username to remove
 * @returns Promise that resolves when the DB update is successful
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const removeLikeFromPost = (postId, userHandle) => {
  return update(ref(db, `posts/${postId}/likedBy`), {
    [userHandle]: null,
  });
};

/**
 * Adds a reference to the user that disliked the post
 * @param {string} postId The Post ID
 * @param {string} userHandle The username to add
 * @returns Promise that resolves when the DB update is successful
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const addDislikeToPost = (postId, userHandle) => {
  return getPostById(postId).then((res) => {
    const allLikes = res.dislikedBy;
    return update(ref(db, `posts/${postId}`), {
      dislikedBy: {
        ...allLikes,
        [userHandle]: true,
      },
    });
  });
};

/**
 * Removes a reference to the user that disliked the post
 * @param {string} postId The Post ID
 * @param {string} userHandle The username to remove
 * @returns Promise that resolves when the DB update is successful
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const removeDislikeFromPost = (postId, userHandle) => {
  return update(ref(db, `posts/${postId}/dislikedBy`), {
    [userHandle]: null,
  });
};

/**
 * Removes the post with the given ID
 * @param {string} id The Post ID
 * @returns Promise that resolves when the DB update is successful
 *
 * @author Miro Todorov
 */
export const deletePost = (id) => {
  console.log(`i will delete post with id: ${id}`);
  return update(ref(db), {
    [`posts/${id}`]: null,
  });
};

/**
 * Gets all posts
 * @returns {DataSnapshot} Data snapshot containing all posts from the DB
 *
 * @author Emil Neshev
 */
export const getAllPosts = () => {
  return get(query(ref(db, 'posts')));
};

/**
 * Gets the number of posts written by the user with given handle
 * @param {string} handle The username to filter by
 * @returns Promise that resolves with the number of posts
 *
 * @author Emil Neshev
 */
export const getPostsCountByHandle = (handle) => {
  return get(query(ref(db, 'posts'))).then((snapshot) => {
    return filterPostsByProp(snapshot, 'author', handle);
  });
};
