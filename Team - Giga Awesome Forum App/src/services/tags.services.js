import { ref, update, onValue } from 'firebase/database';
import { db } from '../utils/firebase-config';

export const getTagsFromSnapshot = (snapshot) => {
  const tagSnapshot = snapshot.val();
  return Object.keys(tagSnapshot);
};

export const addTag = (tagName) => {
  const updates = {};

  updates[`tags/${tagName}`] = true;
  return update(ref(db), updates);
};

export const getLiveTags = (listen) => {
  return onValue(ref(db, 'tags'), listen);
};
