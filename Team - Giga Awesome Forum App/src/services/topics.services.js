import {
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from 'firebase/database';
import { constants } from '../common/constants';
import { db } from '../utils/firebase-config';
import { getCategoryById, getCategoryIdByTopicId, removeTopicFromCategory } from './categories.services';
import { deletePost } from './posts.services';


// TODO: get topics from snapshot !!! - No need it . Now Using getTopicsFromSnapshotByCategoryID
export const getTopicsFromSnapshot = (snapshot) => {
  // console.log(`Snapshot: ${snapshot}`);
  const topicSnapshot = snapshot.val();
  // console.log(`topicSnapshot: ${topicSnapshot}`);

  return Object.keys(topicSnapshot).map((topicId) => {
    // console.log(topicId);
    const topic = topicSnapshot[topicId];
    // console.log(topicSnapshot[topicId]);

    return {
      ...topic,
      id: topicId,
      createdOn: new Date(topic.createdOn),
      likedBy: topic.likedBy ? Object.keys(topic.likedBy) : [],
      tags: topic.tags ? Object.keys(topic.tags) : [],
    };
  });
};

// TODO: add topic
export const addTopic = (title, author, content, categoryId, tags) => {
  return push(ref(db, 'topics'), {
    title,
    author,
    content,
    categoryId: categoryId,
    createdOn: Date.now(),
    tags: tags,
  }).then((result) => {
    console.log(`result.key: ${result.key}`);
    return result.key;
    // getTopicById(result.key);
  });
};

// TODO: get topic by Id:
export const getTopicById = (id) => {
  return get(ref(db, `topics/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Topic with ${id} does not exist`);
    }

    const topic = result.val();
    topic.createdOn = new Date(topic.createdOn);
    topic.likedBy = topic.likedBy ? topic.likedBy : {};
    topic.dislikedBy = topic.dislikedBy ? topic.dislikedBy : {};
    // some more logic ?!?!
    return topic;
  });
};

// TODO: get topic by categoryID
export const getTopicByCategoryId = (topicsList, id) => {
  return topicsList.filter((topic) => topic.categoryId === id);
};

// TODO: get topic by tag
// TODO: get topic by title

// TODO: get topic by author !?!?!
// export const getTopicsByAuthor = (author) => {
//   return get(query(ref(db, 'topics'), orderByChild('author'), equalTo(author)))
//     .then((snapshot) => (snapshot.exists() ? getTopicsFromSnapshot(snapshot) : []));
// };

// get all topics
// export const getAllTopics = () => {

//   return get(ref(db, 'topics'))
//     .then((snapshot) => {
//       if (!snapshot.exists()) {
//         return [];
//       }

//       return getTopicsFromSnapshot(snapshot);
//     });
// };

export const getLiveTopics = (listen) => {
  console.log('gettin live topics');
  return onValue(ref(db, 'topics'), listen);
};


export const deleteTopic = async (topicId, categoryId) => {
  try {  
    const topic = await getTopicById(topicId);
    const postsInTopic = Object.keys(topic.posts);
    postsInTopic.map((postId) => {
      return deletePost(postId);    
    });

    update(ref(db), {
      [`categories/${categoryId}/topics/${topicId}`]: null,
      [`topics/${topicId}`]: null,
    });

  } catch (error) {
    console.log(error.message);
  }
};

export const getTopicsFromSnapshotByCategoryID = (snapshot, catId) => {
  const topicSnapshot = snapshot.val();

  return Object.keys(topicSnapshot)
    .filter((topicId) => {
      const topic = topicSnapshot[topicId];
      return topic.categoryId === catId;
    })
    .map((topicId) => {
      const topic = topicSnapshot[topicId];

      return {
        ...topic,
        id: topicId,
        createdOn: new Date(topic.createdOn),
        likedBy: topic.likedBy ? Object.keys(topic.likedBy) : [],
        dislikedBy: topic.dislikedBy ? Object.keys(topic.dislikedBy) : [],
        tags: topic.tags ? Object.keys(topic.tags) : [],
      };
    });
};

// TODO: add topic to category
export const addTopicToCategory = (topicId, categoryId) => {
  return update(ref(db), {
    [`categories/${categoryId}/topics/${topicId}`]: true,
  });
};

export const filterTopicsById = (snapshot, value) => {
  const topics = snapshot.val();

  return Object.keys(topics)
    .filter((topic) => topic === value)
    .map((id) => {
      const topic = topics[id];
      return {
        ...topic,
        id,
        createdOn: new Date(topic.createdOn),
        likedBy: topic.likedBy ? topic.likedBy : {},
        dislikedBy: topic.dislikedBy ? topic.dislikedBy : {},
      };
    })[0];
};

export const addLikeToTopic = (topicId, userHandle) => {
  return getTopicById(topicId).then((res) => {
    const allLikes = res.likedBy;
    return update(ref(db, `topics/${topicId}`), {
      likedBy: {
        ...allLikes,
        [userHandle]: true,
      },
    });
  });
};

export const removeLikeFromTopic = (topicId, userHandle) => {
  return update(ref(db, `topics/${topicId}/likedBy`), {
    [userHandle]: null,
  });
};

export const addDislikeToTopic = (topicId, userHandle) => {
  return getTopicById(topicId).then((res) => {
    const allLikes = res.dislikedBy;

    return update(ref(db, `topics/${topicId}`), {
      dislikedBy: {
        ...allLikes,
        [userHandle]: true,
      },
    });
  });
};

export const removeDislikeFromTopic = (topicId, userHandle) => {
  return update(ref(db, `topics/${topicId}/dislikedBy`), {
    [userHandle]: null,
  });
};

export const getAllTopics = () => {
  return get(query(ref(db, 'topics')));
}

export const getTopicsCountByHandle = (handle) => {
  return get(query(ref(db, 'topics')))
  .then((snapshot) => {
    const topics = snapshot.val();
    const topicArr = Object.keys(topics);
    const result = topicArr.map((topicId) => {
      return {
        id: topicId,
        ...topics[topicId]
      };
    })
    return result.filter(topic => topic.author === handle);
  })
}