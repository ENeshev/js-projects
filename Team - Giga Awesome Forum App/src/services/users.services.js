import { get, set, ref, query, equalTo, orderByChild, update, onValue } from 'firebase/database';
import { db } from '../utils/firebase-config';
import { userRole } from '../common/user-role';


export const getUserFromSnapshot = (snapshot) => {
  const userSnapshot = snapshot.val();
  return userSnapshot;
}

export const usersFromSnapshot = (snapshot) => {
    const userSnapshot = snapshot.val();    
  
    return Object.keys(userSnapshot)
      .map((userId) => {
        const user = userSnapshot[userId];
  
        return {
          ...user,
          uid: userId,
          handle: user.handle,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          role: user.role,
        };
      });
}

export const getUserByHandle = (handle) => {

  return get(ref(db, `users/${handle}`))
  // .then(snapshot => usersFromSnapshot(snapshot));
};

export const createUserHandle = (handle, uid, firstName, lastName, email) => {

  return set(ref(db, `users/${handle}`), { handle,
    uid,
    firstName,
    lastName,
    email,
    createdOn: new Date().toLocaleDateString(),
    posts: {},
    role: userRole.ORDINARY });
};

export const getUserData = (uid) => {

  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const getAllUsersData = () => {
  return get(query(ref(db, 'users')))
}

export const updateUserRole = (handle, role) => {
  return update(ref(db), {
    [`users/${handle}/role`]: role,
  });
};

export const getLiveUsers = (listen) => {
  return onValue(ref(db, 'users'), listen);
};

export const updateUserInfo = (handle, gender, location, bio, role) => {
  return update(ref(db), {
    [`users/${handle}/gender`]: gender,
    [`users/${handle}/location`]: location,
    [`users/${handle}/bio`]: bio,
    [`users/${handle}/role`]: role,
  });
}
