import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyA9pcgoQpfBimMyEHe_ofhj1JB5eGdo7lQ',
  authDomain: 'awesome-forum-app.firebaseapp.com',
  projectId: 'awesome-forum-app',
  storageBucket: 'awesome-forum-app.appspot.com',
  messagingSenderId: '539062731054',
  appId: '1:539062731054:web:71bb0b74fe9e4cd149fabf',
  databaseURL:
    'https://awesome-forum-app-default-rtdb.europe-west1.firebasedatabase.app/',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// the Firebase authentication handler
export const auth = getAuth(app);

// the Realtime Database handler
export const db = getDatabase(app);
