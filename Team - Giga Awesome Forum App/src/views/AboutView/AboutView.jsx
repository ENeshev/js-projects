import './AboutView.css';

import milen from '../../images-src/team pics/Milen pic.jpeg';
import emo from '../../images-src/team pics/Emo.jpeg';
import miro from '../../images-src/team pics/Miro.png';
import { aboutInfo } from '../../common/constants';

export default function AboutView() {
  return (
    <div>
      <div className='about-title'>
        <h1 className='about-heading'>The binary Three Dudes</h1>
      </div>
      <div className='user-about'>
        <div className='img'>
          <img className='img-about' src={milen} alt='Milen Nenkov' />
        </div>
        <div className='user-info all-user-info'>
          <p className='user-info'>Name: {aboutInfo.milen.name}</p>
          <br />
          <p className='user-info'>Place of birth: {aboutInfo.milen.town}</p>
          <br />
          <p className='user-info'>
            Current City: {aboutInfo.milen.currentCity}
          </p>
          <br />
          <br />
          <p className='user-info'>Biography: {aboutInfo.milen.biography}</p>
          <br />
          <br />
          <p className='user-info'>Contacts:</p>
          <br />
          <p className='user-info'>mobile: {aboutInfo.milen.mobile}</p>
          <br />
          <p className='user-info'>email: {aboutInfo.milen.email}</p>
          <br />
        </div>
      </div>
      <div className='user-about'>
        <div className='img'>
          <img className='img-about' src={emo} alt='Emil Neshev' />
        </div>
        <div className='user-info all-user-info'>
          <p className='user-info'>Name: {aboutInfo.emo.name}</p>
          <br />
          <p className='user-info'>Place of birth: {aboutInfo.emo.town}</p>
          <br />
          <p className='user-info'>Current City: {aboutInfo.emo.currentCity}</p>
          <br />
          <br />
          <p className='user-info'>Biography: {aboutInfo.emo.biography}</p>
          <br />
          <br />
          <p className='user-info'>Contacts:</p>
          <br />
          <p className='user-info'>mobile: {aboutInfo.emo.mobile}</p>
          <br />
          <p className='user-info'>email: {aboutInfo.emo.email}</p>
          <br />
        </div>
      </div>
      <div className='user-about'>
        <div className='img'>
          <img className='img-about' src={miro} alt='Miroslav Todorov' />
        </div>
        <div className='user-info all-user-info'>
          <p className='user-info'>Name: {aboutInfo.miro.name}</p>
          <br />
          <p className='user-info'>Place of birth: {aboutInfo.miro.town}</p>
          <br />
          <p className='user-info'>
            Current City: {aboutInfo.miro.currentCity}
          </p>
          <br />
          <br />
          <p className='user-info'>Biography: {aboutInfo.miro.biography}</p>
          <br />
          <br />
          <p className='user-info'>Contacts:</p>
          <br />
          <p className='user-info'>mobile: {aboutInfo.miro.mobile}</p>
          <br />
          <p className='user-info'>email: {aboutInfo.miro.email}</p>
          <br />
        </div>
      </div>
    </div>
  );
}
