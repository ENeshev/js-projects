// import Tags from "../../components/Tags/Tags";
import { Input } from 'antd';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { addCategory } from '../../services/categories.services';
// import './CreateTopicView.css';
export default function CreateCategory () {
  const {TextArea} = Input;
  const navigate = useNavigate();

  let [title, setTitle] = useState('');
  let [subTitle, setSubTitle] = useState('');

  const handleTitle = (e) => {
    setTitle(e.target.value);
  };

  const handleSubTitle = (e) => {
    setSubTitle(e.target.value);
  }
  
  const createCategory = () => {
    console.log('invoke createCategory');
    
    return addCategory(title, subTitle)
      .then(() => {
        navigate('/home');
      })
      .catch(console.error);
  };

  return (
    <div className='create-topic-body'>
      <div className='create-topic-title-input'>
        <label htmlFor={TextArea}>Category Title</label>
        <TextArea rows={1} placeholder={`Title:`} onChange={handleTitle} />
      </div>
      <div className='create-topic-content-input'>
        <label htmlFor={TextArea}>Category Description</label>
        <TextArea rows={5} placeholder={`Description:`} onChange={handleSubTitle} />
      </div>
      <div className='create-btn'>
        <button type='ordinary' name='create' onClick={createCategory}>Create</button>
      </div>
    </div>
  );
}