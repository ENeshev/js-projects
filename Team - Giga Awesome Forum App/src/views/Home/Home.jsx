import CategoryTile from '../../components/CategoryTile/CategoryTile';
import { Fragment, useContext, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from 'antd';
import './Home.css';
import {
  getCategoriesFromSnapshot,
  getLiveCategories,
} from '../../services/categories.services';
import AppContext from '../../providers/app-state';
import NoContentYet from '../../components/No Content/No Content';
import {
  getLiveTopics,
  getTopicByCategoryId,
  getTopicsFromSnapshot,
} from '../../services/topics.services';
import { userRole } from '../../common/user-role';

export default function HomeView() {
  const { user } = useContext(AppContext);
  const [categories, setCategories] = useState([]);
  const [topics, setTopics] = useState(null);

  let userRights = false;
  if (user.userData) {
    if (user.userData.role === userRole.ADMIN) {
      userRights = true;
    }
  }

  useEffect(() => {
    console.log('useEffect categories');
    return getLiveCategories((snapshot) => {
      setCategories(getCategoriesFromSnapshot(snapshot));
    });
  }, []);

  useEffect(() => {
    console.log('useEffect topics');
    getLiveTopics((snapshot) => {
      setTopics(getTopicsFromSnapshot(snapshot));
    });
  }, []);

  const getTopicsByCategory = (categoryId) => {
    if (!topics) return [];
    return topics.filter((topic) => topic.categoryId === categoryId);
  };

  const allPostsFromCategory = (categoryId) => {
    console.log(`allPostsFromCategory`);
    if (!topics) {
      return [];
    }
    const categoryTopics = getTopicByCategoryId(topics, categoryId);
    const postsInTopic = categoryTopics.map((topic) => {
      if (!topic.posts) return [];
      return Object.keys(topic.posts);
    });
    return postsInTopic.flat();
  };

  return (
    <Fragment>
      {userRights ? (
        <div className='add-category'>
          <Button className='add-category-btn' type='primary'>
            <NavLink to='create-category'>Add Category</NavLink>
          </Button>
        </div>
      ) : null}
      <div className='category-list'>
        <Fragment>
          {categories.length ? (
            <Fragment>
              {topics &&
                categories.map((category) => (
                  <CategoryTile
                    key={category.id}
                    {...category}
                    topics={getTopicsByCategory(category.id)}
                    allPosts={allPostsFromCategory(category.id)}
                  />
                ))}
            </Fragment>
          ) : (
            <NoContentYet />
          )}
        </Fragment>
      </div>
    </Fragment>
  );
}
