import { useState } from 'react';
import './Login.css';

import { Drawer, Button } from 'antd';
import SignUp from '../../components/Drawer/Sign Up Drawer';
import LogIn from '../../components/Drawer/LogIn Drawer';

const DrawerForm = () => {
  const [state, setState] = useState(false);
  const [loginState, setLoginState] = useState(false);
  let [drawerTitle, setDrawerTitle] = useState('');

  const handleLoginState = () => {
    setDrawerTitle('Log In');
    setLoginState(true);
    setState(true);
  };

  const handleRegisterState = () => {
    setDrawerTitle('Sign Up');
    setState(true);
  };

  const onClose = () => {
    setLoginState(false);
    setState(false);
    
  };

  return (
    <>
      <Button
        size='middle'
        shape='round'
        className='login-btn'
        onClick={handleLoginState}
      >
        Login
      </Button>
      <Button
        size='middle'
        shape='round'
        className='register-btn'
        onClick={handleRegisterState}
      >
        Sign Up
      </Button>
      <Drawer
        className='drawer-form'
        title={drawerTitle}
        width={400}
        style={{ background: (30, 80, 41) }}
        onClose={onClose}
        visible={state}
        bodyStyle={{ paddingBottom: 80 }}
      >
        {loginState ? (
          <LogIn
            onClose={onClose}
            setLoginState={setLoginState}
            setDrawerTitle={setDrawerTitle}
            setState={setState}
          />
        ) : (
          <SignUp
            setLoginState={setLoginState}
            setDrawerTitle={setDrawerTitle}
            setState={setState}
          />
        )}
      </Drawer>
    </>
  );
};

export default DrawerForm;
