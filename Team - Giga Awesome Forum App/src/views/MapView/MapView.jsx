import React, { memo, useContext, useEffect, useState } from 'react';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';
import Geocode from 'react-geocode';
import { Button, Input, Modal, notification } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './MapView.css';
import {
  faCity,
  faEnvelopesBulk,
  faFileSignature,
  faLocationCrosshairs,
} from '@fortawesome/free-solid-svg-icons';
import {
  addLocation,
  deleteLocation,
  getLiveLocations,
  locationsFromSnapshot,
} from '../../services/map.services';
import AppContext from '../../providers/app-state';
import { userRole } from '../../common/user-role';

Geocode.setLocationType('ROOFTOP');
Geocode.setApiKey('AIzaSyDYvJQWLtmL2p47CbaNaL_K54jAIOKF27A');

const containerStyle = {
  width: '50vw',
  height: '75vh',
  margin: '0 auto',
};

const center = {
  lat: 42.76650811050641,
  lng: 25.32422501607913,
};

function MapView() {
  const {
    user: { userData },
  } = useContext(AppContext);

  /**
   * @type {[{ locationName: string , lat: string, lng: string, city: string, address: string, addedBy: string, id: string }[], Function]}
   */
  const [locations, setLocations] = useState(null);
  const [shownLocation, setShownLocation] = useState(null);
  const [infoModalVisible, setInfoModalVisible] = useState(false);
  const [addLocationName, setAddLocationName] = useState('');
  const [addLocationCoords, setAddLocationCoords] = useState({
    lat: '',
    lng: '',
  });
  const [locationModalVisible, setLocationModalVisible] = useState(false);
  const [inputValues, setInputValues] = useState({
    locationName: '',
    address: '',
    city: '',
    postCode: '',
  });

  useEffect(() => {
    return getLiveLocations((snapshot) => {
      setLocations(locationsFromSnapshot(snapshot));
    });
  }, []);

  const showInfo = (location) => {
    setShownLocation(location);
    setInfoModalVisible(true);
  };

  const onMapClick = (e) => {
    const { lat, lng } = e.latLng;

    setLocationModalVisible(true);
    setAddLocationCoords({ lat: lat(), lng: lng() });
  };

  const closeModal = () => {
    setInfoModalVisible(false);
  };

  const createLocation = (e) => {
    e.preventDefault();

    const locationName = inputValues.locationName;
    const street = inputValues.address;
    const city = inputValues.city;
    const postCode = inputValues.postCode;

    const fullAddress = `${street}, ${city} ${postCode}`;

    Geocode.fromAddress(fullAddress)
      .then((res) => {
        const { lat, lng } = res.results[0].geometry.location;
        addLocation({
          locationName,
          lat,
          lng,
          city,
          address: fullAddress,
          addedBy: userData.handle,
        }).then(() => {
          notification.success({
            message: `${locationName} successfully added`,
            description: 'Thank you for contributing to our database',
            placement: 'top',
            duration: 2,
          });

          setInputValues({
            locationName: '',
            address: '',
            city: '',
            postCode: '',
          });
        });
      })
      .catch((err) =>
        notification.error({
          message: 'There was an error adding the location',
          description: `We could not add this location due to: ${
            err.message.endsWith('ZERO_RESULTS')
              ? 'no results found on Google Maps. Please double check your input'
              : err.message
          }`,
          duration: 4,
          placement: 'top',
        })
      );
  };

  const createClickLocation = (e) => {
    e.preventDefault();

    const { lat, lng } = addLocationCoords;
    Geocode.fromLatLng(lat, lng)
      .then((res) => {
        const locationName = e.target[0].value;
        const city = res.results[0].address_components.find((component) =>
          component.types.includes('locality')
        ).long_name;
        const fullAddress = res.results[0].address_components
          .map((component) => component.long_name)
          .join(', ');

        addLocation({
          locationName,
          lat,
          lng,
          city,
          address: fullAddress,
          addedBy: userData.handle,
        }).then(() => {
          notification.success({
            message: `${locationName} successfully added`,
            description: 'Thank you for contributing to our database',
            placement: 'top',
            duration: 2,
          });
        });

        setAddLocationName('');
        setLocationModalVisible(false);
      })
      .catch((err) =>
        notification.error({
          message: 'Error adding location',
          description: `We could not add the location because: ${err.message}`,
          placement: 'top',
          duration: 4,
        })
      );
  };

  const handleInputChange = (prop, newValue) => {
    setInputValues({ ...inputValues, [prop]: newValue });
  };

  const removeLocation = () => {
    deleteLocation(shownLocation.id)
      .then(() =>
        notification.success({
          message: 'Success',
          description: 'This location was removed from the database',
          placement: 'top',
          duration: 2,
        })
      )
      .catch((err) =>
        notification.error({
          message: 'Error removing',
          description: `The location could not be deleted because: ${err.message}`,
          placement: 'top',
          duration: 4,
        })
      );

    setShownLocation(null);
    closeModal();
  };

  return (
    <div className='MapContainer'>
      {/* Main Map Component */}
      <LoadScript googleMapsApiKey='AIzaSyDYvJQWLtmL2p47CbaNaL_K54jAIOKF27A'>
        <GoogleMap
          mapContainerStyle={containerStyle}
          center={center}
          zoom={7}
          clickableIcons={false}
          onClick={onMapClick}
        >
          {locations &&
            locations.map((location) => {
              return (
                <Marker
                  key={location.id}
                  position={location}
                  onClick={() => showInfo(location)}
                />
              );
            })}
        </GoogleMap>
      </LoadScript>

      {/* Location Info Modal */}
      {shownLocation && (
        <Modal
          visible={infoModalVisible}
          title={shownLocation.locationName}
          onOk={closeModal}
          onCancel={closeModal}
          footer={[
            <Button key='back' className='nav-btn' onClick={closeModal}>
              Close
            </Button>,
            <Button
              key='link'
              className='nav-btn'
              href={`https://www.google.com/maps/place/${shownLocation.address}`}
              onClick={closeModal}
              target='_blank'
            >
              See on Google Maps
            </Button>,
          ]}
        >
          <h1 className='map-modal-heading'>{shownLocation.locationName}</h1>
          <h2>{shownLocation.address}</h2>
          {userData.role === userRole.ADMIN && (
            <Button className='nav-btn' onClick={removeLocation}>
              Remove Location
            </Button>
          )}
        </Modal>
      )}

      {/* Store name setup on map click modal */}
      <Modal
        visible={locationModalVisible}
        title='Add store name'
        footer={null}
        onOk={() => setLocationModalVisible(false)}
        onCancel={() => setLocationModalVisible(false)}
      >
        <h2>Please enter the store name</h2>
        <form
          onSubmit={createClickLocation}
          style={{ display: 'flex', flexDirection: 'column' }}
        >
          <Input
            required={true}
            size='large'
            style={{ width: '50%' }}
            value={addLocationName}
            placeholder='Enter the store name'
            prefix={<FontAwesomeIcon icon={faFileSignature} />}
            onChange={(e) => setAddLocationName(e.target.value)}
          />
          <Button
            style={{ width: '30%' }}
            htmlType='submit'
            className='nav-btn'
          >
            Add Location
          </Button>
        </form>
      </Modal>

      {/* Manual address input form */}
      <form className='map-form' onSubmit={createLocation}>
        <h1>Do us all a favor and add any board game locations you know</h1>
        <Input
          required={true}
          size='large'
          placeholder='Enter the store name'
          autoComplete='off'
          style={{ width: '70%' }}
          value={inputValues.locationName}
          onChange={(e) => handleInputChange('locationName', e.target.value)}
          prefix={<FontAwesomeIcon icon={faFileSignature} />}
        />
        <Input
          required={true}
          size='large'
          placeholder='Enter the street address'
          autoComplete='off'
          style={{ width: '70%' }}
          value={inputValues.address}
          onChange={(e) => handleInputChange('address', e.target.value)}
          prefix={<FontAwesomeIcon icon={faLocationCrosshairs} />}
        />
        <Input.Group compact>
          <Input
            required={true}
            size='large'
            placeholder='Enter the town/city'
            autoComplete='off'
            value={inputValues.city}
            onChange={(e) => handleInputChange('city', e.target.value)}
            prefix={<FontAwesomeIcon icon={faCity} />}
            style={{ width: '50%' }}
          />
          <Input
            required={true}
            size='large'
            placeholder='Post Code'
            autoComplete='off'
            value={inputValues.postCode}
            onChange={(e) => handleInputChange('postCode', e.target.value)}
            style={{ width: '20%' }}
            prefix={<FontAwesomeIcon icon={faEnvelopesBulk} />}
          />
        </Input.Group>

        <Button htmlType='submit' className='nav-btn'>
          Add Location
        </Button>
      </form>
    </div>
  );
}

export default memo(MapView);
