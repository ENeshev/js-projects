import './NewsView.css';
import { Space } from 'antd';
import SingleNews from '../../components/SingleNews/SingleNews';
import { useEffect, useState } from 'react';
import { constants } from '../../common/constants';

export default function NewsView() {
  const [newsFeed, setNewsFeed] = useState([]);

  useEffect(() => {
    fetch(
      `${constants.NEWS_URL}?apiKey=${constants.NEWS_API_KEY}&q="board games"&pageSize=20&sortBy=publishedAt`
    )
      .then((res) => res.json())
      .then((data) => {
        if (data.status === 'ok') {
          setNewsFeed(data.articles);
        }
      });
  }, []);

  return (
    <Space>
      <div className='news-feed'>
        {newsFeed.map((news) => (
          <SingleNews key={news.title} {...news} />
        ))}
      </div>
    </Space>
  );
}
