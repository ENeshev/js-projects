import {
  DislikeFilled,
  DislikeOutlined,
  LikeFilled,
  LikeOutlined,
} from '@ant-design/icons';
import {
  Avatar,
  Button,
  Comment,
  Divider,
  Drawer,
  Popconfirm,
  Tooltip,
} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { userRole } from '../../common/user-role';
import AppContext from '../../providers/app-state';
import {
  addDislikeToPost,
  addLikeToPost,
  addPost,
  addPostToTopic,
  editPostContent,
  filterPostsByProp,
  getLivePosts,
  removeDislikeFromPost,
  removeLikeFromPost,
  removePost,
} from '../../services/posts.services';
import {
  addDislikeToTopic,
  addLikeToTopic,
  filterTopicsById,
  getLiveTopics,
  removeDislikeFromTopic,
  removeLikeFromTopic,
} from '../../services/topics.services';
import './Post.css';
import moment from 'moment';
import { NavLink } from 'react-router-dom';
import { constants } from '../../common/constants';

/** @typedef {{ id: string, author: string, topicId: string, createdOn: number, content: string, likedBy: {}, dislikedBy: {}}} Post */

export default function PostView() {
  const { tId } = useParams();

  const {
    user: { userData },
  } = useContext(AppContext);
  const handle = userData?.handle ? userData.handle : null;

  const [isReplyOpen, setIsReplyOpen] = useState(false);
  const [replyValue, setReplyValue] = useState('');
  const [isEditOpen, setIsEditOpen] = useState(false);
  const [editValue, setEditValue] = useState('');
  const [editPostId, setEditPostId] = useState(null);
  /** @type {[Post[], Function]}*/
  const [posts, setPosts] = useState([]);
  const [topic, setTopic] = useState(null);
  const [isTopicLiked, setIsTopicLiked] = useState(false);
  const [isTopicDisliked, setIsTopicDisliked] = useState(false);

  const dislikeTopic = () => {
    isUserDislikedTopic()
      ? removeDislikeFromTopic(tId, handle)
      : addDislikeToTopic(tId, handle);
  };

  const likeTopic = () => {
    isUserLikedTopic()
      ? removeLikeFromTopic(tId, handle)
      : addLikeToTopic(tId, handle);
  };

  const isUserLikedTopic = () => {
    return Object.keys(topic.likedBy).includes(handle);
  };

  const isUserDislikedTopic = () => {
    return Object.keys(topic.dislikedBy).includes(handle);
  };

  useEffect(() => {
    return getLivePosts((snapshot) => {
      setPosts(filterPostsByProp(snapshot, 'topicId', tId));
    });
  }, [tId]);

  useEffect(() => {
    return getLiveTopics((snapshot) => {
      setTopic(filterTopicsById(snapshot, tId));
    });
  }, [tId]);

  useEffect(() => {
    if (topic) {
      setIsTopicLiked(Object.keys(topic.likedBy).includes(handle));
      setIsTopicDisliked(Object.keys(topic.dislikedBy).includes(handle));
    }
  }, [handle, topic]);

  const createPost = (e) => {
    e.preventDefault();
    setReplyValue(e.target[0].value);
    addPost(replyValue, tId, handle)
      .then((postId) => {
        return addPostToTopic(postId, tId);
      })
      .catch(console.error);

    setReplyValue('');
    setIsReplyOpen(false);
  };

  const startEdit = (oldContent, id) => {
    setEditValue(oldContent);
    setEditPostId(id);
    setIsEditOpen(true);
  };

  const editPost = (e) => {
    e.preventDefault();
    editPostContent(editPostId, editValue);
    setEditPostId(null);
    setIsEditOpen(false);
    setEditValue('');
  };

  return (
    <>
      {/* Topic Display */}
      <Comment
        actions={[
          !isTopicDisliked && (
            <Tooltip key='comment-basic-like' title='Like'>
              <span onClick={likeTopic}>
                {isTopicLiked ? (
                  <LikeFilled style={{ color: 'lightgreen' }} />
                ) : (
                  <LikeOutlined style={{ color: 'lightgreen' }} />
                )}
                <span
                  className='comment-action'
                  style={{ color: 'lightgreen' }}
                >
                  {topic?.likedBy ? Object.keys(topic.likedBy).length : 0}
                </span>
              </span>
            </Tooltip>
          ),
          !isTopicLiked && (
            <Tooltip key='comment-basic-dislike' title='Dislike'>
              <span onClick={dislikeTopic}>
                {isTopicDisliked ? (
                  <DislikeFilled style={{ color: 'lightgreen' }} />
                ) : (
                  <DislikeOutlined style={{ color: 'lightgreen' }} />
                )}
                <span
                  className='comment-action'
                  style={{ color: 'lightgreen' }}
                >
                  {topic?.dislikedBy ? Object.keys(topic.dislikedBy).length : 0}
                </span>
              </span>
            </Tooltip>
          ),
          <span
            onClick={() => setIsReplyOpen(true)}
            key='comment-basic-reply-to'
            style={{ color: 'lightgreen' }}
          >
            Reply to
          </span>,
        ]}
        author={topic?.author}
        avatar={
          <NavLink to={`/userInfo/${topic?.author}`}>
            <Avatar
              src={constants.AVATAR_URL + `${topic?.author}?size=35x35`}
              size={40}
            />
          </NavLink>
        }
        content={
          <>
            <h3>{topic?.title}</h3>
            <p>{topic?.content}</p>
          </>
        }
        datetime={
          <Tooltip title={topic?.createdOn?.toLocaleString()}>
            <span>{moment(topic?.createdOn).fromNow()}</span>
          </Tooltip>
        }
      />

      {/* Divider */}
      <Divider
        orientation='left'
        style={{
          color: 'var(--Light-green-color)',
          borderTopColor: 'var(--Light-green-color)',
        }}
      >
        {posts.length} Replies
      </Divider>

      {/* All topic posts */}
      {posts.length === 0 ? (
        <div>No ones has replied yet</div>
      ) : (
        posts.map((post) => {
          return (
            <Comment
              key={post.id}
              actions={[
                !post.dislikedBy.includes(handle) && (
                  <Tooltip key='comment-basic-like' title='Like'>
                    <span
                      onClick={() =>
                        post.likedBy.includes(handle)
                          ? removeLikeFromPost(post.id, handle)
                          : addLikeToPost(post.id, handle)
                      }
                    >
                      {post.likedBy.includes(handle) ? (
                        <LikeFilled style={{ color: 'lightgreen' }} />
                      ) : (
                        <LikeOutlined style={{ color: 'lightgreen' }} />
                      )}
                      <span
                        className='comment-action'
                        style={{ color: 'lightgreen' }}
                      >
                        {post.likedBy.length}
                      </span>
                    </span>
                  </Tooltip>
                ),
                !post.likedBy.includes(handle) && (
                  <Tooltip key='comment-basic-dislike' title='Dislike'>
                    <span
                      onClick={() =>
                        post.dislikedBy.includes(handle)
                          ? removeDislikeFromPost(post.id, handle)
                          : addDislikeToPost(post.id, handle)
                      }
                    >
                      {post.dislikedBy.includes(handle) ? (
                        <DislikeFilled style={{ color: 'lightgreen' }} />
                      ) : (
                        <DislikeOutlined style={{ color: 'lightgreen' }} />
                      )}
                      <span
                        className='comment-action'
                        style={{ color: 'lightgreen' }}
                      >
                        {post.dislikedBy.length}
                      </span>
                    </span>
                  </Tooltip>
                ),
                <span
                  onClick={() => setIsReplyOpen(true)}
                  key='comment-basic-reply-to'
                  style={{ color: 'lightgreen' }}
                >
                  Reply
                </span>,
                (userData?.role === userRole.ADMIN ||
                  handle === post.author) && (
                  <>
                    <span
                      onClick={() => startEdit(post.content, post.id)}
                      key='comment-basic-edit'
                      style={{ color: 'lightgreen' }}
                    >
                      Edit Post
                    </span>
                    <Popconfirm
                      title='Are you sure?'
                      onConfirm={() => removePost(post.id, tId)}
                      okText='Yes'
                      cancelText='No'
                    >
                      <span
                        key='comment-basic-delete'
                        style={{ color: 'lightgreen' }}
                      >
                        Delete Post
                      </span>
                    </Popconfirm>
                    ,
                  </>
                ),
              ]}
              author={post.author}
              avatar={
                <NavLink to={`/userInfo/${post.author}`}>
                  <Avatar
                    src={constants.AVATAR_URL + `${post.author}?size=35x35`}
                    size={40}
                  />
                </NavLink>
              }
              content={post.content}
              datetime={
                <Tooltip title={post.createdOn.toLocaleString()}>
                  <span>{moment(post.createdOn).fromNow()}</span>
                </Tooltip>
              }
            />
          );
        })
      )}

      {/* Reply form drawer */}
      <div className='drawer-container'>
        <Drawer
          title='Reply to post'
          placement='bottom'
          visible={isReplyOpen}
          onClose={() => setIsReplyOpen(false)}
        >
          <form onSubmit={createPost}>
            <TextArea
              showCount
              maxLength={8192}
              autoSize={{ minRows: 4 }}
              style={{ width: '60vw', margin: '0 auto' }}
              value={replyValue}
              onChange={(e) => setReplyValue(e.target.value)}
            />
            <Button
              className='form-btn'
              style={{ margin: '1em 40em', width: '5em' }}
              htmlType='submit'
            >
              Reply
            </Button>
          </form>
        </Drawer>
      </div>

      {/* Edit form drawer */}
      <div className='drawer-container'>
        <Drawer
          title='Edit post'
          placement='bottom'
          visible={isEditOpen}
          onClose={() => setIsEditOpen(false)}
        >
          <form onSubmit={editPost}>
            <TextArea
              showCount
              maxLength={8192}
              autoSize={{ minRows: 4 }}
              style={{ width: '60vw', margin: '0 auto' }}
              value={editValue}
              onChange={(e) => setEditValue(e.target.value)}
            />
            <Button
              className='form-btn'
              style={{ margin: '1em 40em', width: '5em' }}
              htmlType='submit'
            >
              Edit
            </Button>
          </form>
        </Drawer>
      </div>
    </>
  );
}
