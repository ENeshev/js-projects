import { useLocation } from 'react-router-dom';
import { Avatar, Comment, Tooltip } from 'antd';
import React from 'react';
import { NavLink } from 'react-router-dom';
import { constants } from '../../common/constants';
import moment from 'moment';

export default function UserPosts() {
  const location = useLocation();
  const userPosts = location.state.accountPosts;

  return (
    <>
      {userPosts.length === 0 ? (
        <div>The user has not written any posts yet</div>
      ) : (
        userPosts.map((post) => {
          return (
            <Comment
              key={post.id}
              author={post.author}
              avatar={
                <NavLink to={`/userInfo/${post.author}`}>
                  <Avatar
                    src={constants.AVATAR_URL + `${post.author}?size=35x35`}
                    size={40}
                  />
                </NavLink>
              }
              content={post.content}
              datetime={
                <Tooltip title={post.createdOn.toLocaleString()}>
                  <span>{moment(post.createdOn).fromNow()}</span>
                </Tooltip>
              }
            />
          );
        })
      )}
    </>
  );
}
