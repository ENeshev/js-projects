import Tags from '../../components/Tags/Tags';
import { Input, notification } from 'antd';
import './CreateTopicView.css';
import { constants } from '../../common/constants';
import { useContext, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { addTopic, addTopicToCategory } from '../../services/topics.services';
import AppContext from '../../providers/app-state';

export default function CreateTopicView() {
  const { TextArea } = Input;
  const { cId } = useParams();
  // console.log(cId);
  const navigate = useNavigate();

  const {
    user: { userData },
  } = useContext(AppContext);
  const handle = userData.handle;
  let [title, setTitle] = useState('');
  let [description, setDescription] = useState('');
  const [attachedTags, setAttachedTags] = useState([]);

  const handleTitle = (e) => {
    console.log('handle Title');
    setTitle(e.target.value);
  };

  const handleDescription = (e) => {
    console.log('handle Title');
    setDescription(e.target.value);
  };

  const handleTags = (tag) => {
    setAttachedTags(tag);
  };

  const createTopic = () => {
    
    if(title.length < constants.POST_TITLE_MIN_LENGTH) {
      return notification.error({
        message: `Title name too small`,
        description: `Title name should be at least ${constants.POST_TITLE_MIN_LENGTH} symbols`
      });
    }

    if(description.length < constants.POST_COMMENT_MIN_LENGTH) {
      return notification.error({
        message: `Description too small`,
        description: `Description should be at least ${constants.POST_COMMENT_MIN_LENGTH} symbols`
      });
    }

    const tags = {};
    attachedTags.map((el) => (tags[el] = true));
    console.log(`tags from create: ${tags}`);

    // const newTopicId = addTopic(title, handle, description, cId, tags);
    // addTopicToCategory(newTopicId, cId);
    addTopic(title, handle, description, cId, tags)
      .then((newTopicId) => {
        return addTopicToCategory(newTopicId, cId);
      })
      .catch(console.error);
    navigate(-1);
  };

  return (
    <div className='create-topic-body'>
      <div className='create-topic-title-input'>
        <label htmlFor={TextArea}>Topic Title</label>
        <TextArea
          rows={1}
          placeholder={`Title should be in range 
        ${constants.POST_TITLE_MIN_LENGTH} - ${constants.POST_TITLE_MAX_LENGTH} symbols`}
          maxLength={constants.POST_TITLE_MAX_LENGTH}
          onChange={handleTitle}
        />
      </div>
      <Tags handleTags={handleTags} />
      <div className='create-topic-content-input'>
        <label htmlFor={TextArea}>Topic Description</label>
        <TextArea
          rows={5}
          placeholder={`Title should be in range
        ${constants.POST_COMMENT_MIN_LENGTH} - ${constants.POST_COMMENT_MAX_LENGTH} symbols`}
          maxLength={constants.POST_COMMENT_MAX_LENGTH}
          onChange={handleDescription}
        />
      </div>
      <div className='create-btn'>
        <button type='button' name='create' onClick={createTopic}>
          Create
        </button>
      </div>
    </div>
  );
}
