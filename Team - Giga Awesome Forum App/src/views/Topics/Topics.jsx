import TopicTile from '../../components/TopicTile/TopicTile';
import './Topics.css';
import { Button } from 'antd';
import { NavLink, useParams } from 'react-router-dom';
import { Pagination } from 'antd';
import { Fragment, useContext, useEffect, useState } from 'react';
import AppContext from '../../providers/app-state';
import {
  getLiveTopics,
  getTopicsFromSnapshotByCategoryID,
} from '../../services/topics.services';
import NoContentYet from '../../components/No Content/No Content';
import { getLivePosts, postsFromSnapshot } from '../../services/posts.services';
import { getLiveUsers, usersFromSnapshot } from '../../services/users.services';
import { userRole } from '../../common/user-role';
import Filter from '../../components/Filter/filter';
export default function TopicsView() {
  const { cId } = useParams();

  const { user, setUser } = useContext(AppContext);
  const [topics, setTopics] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPerPage, setTotalPerPage] = useState(3);
  const [posts, setPosts] = useState([]);
  const [authors, setAuthors] = useState([]);
  const [allData, setAllData] = useState(false);
  const [sortDirection, setSortDirection] = useState(true);


  let userRights = false;
  if (user.userData) {
    // console.log(user.userData);
    const currentUserRole = user.userData.role;
    // console.log((currentUserRole === userRole.ORDINARY));

    if (currentUserRole !== userRole.BANNED) {
      userRights = true;
    }
  }

  useEffect(() => {
    return getLiveUsers((snapshot) => {
      setAuthors(usersFromSnapshot(snapshot));
    });
  }, []);

  useEffect(() => {
    if (posts && authors) {
      console.log('setting allData');
      setAllData(true);
    }
  }, [posts, authors]);

  useEffect(() => {
    return getLivePosts((snapshot) => {
      setPosts(postsFromSnapshot(snapshot));
    });
  }, []);

  useEffect(() => {
    console.log('useEffect in topics');
    return getLiveTopics((snapshot) => {
      // setTopics(getTopicsFromSnapshot(snapshot));
      setTopics(getTopicsFromSnapshotByCategoryID(snapshot, cId));
    });
  }, [cId]);

  const getPaginationData = () => {
    const sIndex = page * totalPerPage - totalPerPage;
    const endIndex = sIndex + totalPerPage;
    return topics.slice(sIndex, endIndex);
  };

  const onShowSizeChange = (current, pageSize) => {
    setTotalPerPage(pageSize);
  };

  const getLastPost = (topicId) => {
    const topicPosts = posts.filter((post) => {
      return post.topicId === topicId;
    });
    return topicPosts[topicPosts.length - 1];
  };

  const countPostsByTopic = (topicId) => {
    return posts.filter((post) => {
      return post.topicId === topicId;
    }).length;
  };

  const getLastPostAuthor = (topicId) => {
    const authorHandle = getLastPost(topicId)?.author;
    const authorObj = authors.filter((author) => {
      return author.handle === authorHandle;
    });
    return Object.values(authorObj);
  };

  const onSort = (sortKey) => {
    let sortTopics = [...topics];
    if(Array.isArray(sortTopics[0][sortKey])) {
      sortTopics.sort(function(a, b){
        if(a[sortKey].length < b[sortKey].length) { return -1; }
        if(a[sortKey].length > b[sortKey].length) { return 1; }
        return 0;
      })
    } else {
      sortTopics.sort(function(a, b){
        if(a[sortKey].toLowerCase() < b[sortKey].toLowerCase()) { return -1; }
        if(a[sortKey].toLowerCase() > b[sortKey].toLowerCase()) { return 1; }
        return 0;
      })
    }
    
    if (sortDirection) {
      sortTopics.reverse()
    }
    setSortDirection(!sortDirection);
    setTopics(sortTopics);
  }

  return (
    <div className='Topics'>
      <div className='topics-controls'>
        {topics.length ? (
          <Pagination
            onChange={(value) => setPage(value)}
            pageSize={totalPerPage}
            total={topics.length}
            current={page}
            showSizeChanger
            pageSizeOptions={[3, 7, 9, 13]}
            showQuickJumper
            onShowSizeChange={onShowSizeChange}
          />
        ) : null}
        {/* <Filter className='filter-component' /> */}

        {userRights ? (
          <Button className='add-btn' type='primary'>
            {/* <NavLink to='create-topic'> */}
            <NavLink to={`./create-topic/${cId}`}>Add Topic</NavLink>
          </Button>
        ) : null}
      </div>
      {topics.length ? (
        <Fragment>
          <div className="sort">
            Sort by : 
            <Button type="link" onClick={() => onSort('author')}>Author</Button> /
            <Button type="link" onClick={() => onSort('title')}>Title</Button> /
            <Button type="link" onClick={() => onSort('likedBy')}>Likes</Button>
          </div>
          {allData &&
            getPaginationData().map((topic) => (
              <TopicTile
                key={topic.id}
                {...topic}
                postsCount={countPostsByTopic(topic.id)}
                lastPost={getLastPost(topic.id)}
                lastPostAuthor={getLastPostAuthor(topic.id)}
                categoryId={cId}
              />
            
            ))}
            
        </Fragment>
        
      ) : (
        <NoContentYet />
      )}
    </div>
  );
}
