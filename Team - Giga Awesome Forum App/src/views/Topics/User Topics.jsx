import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { getLivePosts, postsFromSnapshot } from "../../services/posts.services";
import { getLiveUsers, usersFromSnapshot } from "../../services/users.services";
import { Fragment } from "react";
import TopicTile from "../../components/TopicTile/TopicTile";
import NoContentYet from "../../components/No Content/No Content";

export default function UserTopics() {
  const location = useLocation();
  const userTopics = location.state.accountTopics;

  const [posts, setPosts] = useState([]);
  const [authors, setAuthors] = useState([]);

  useEffect(() => {
    return getLivePosts((snapshot) => {
      setPosts(postsFromSnapshot(snapshot));
    })
  }, []);
  
  useEffect(() => {
    return getLiveUsers((snapshot) => {
      setAuthors(usersFromSnapshot(snapshot));
    });
  }, []);

  const countPostsByTopic = (topicId) => {  

    return posts.filter((post) => {
      return post.topicId === topicId;
    }).length;
  };

  const getLastPost = (topicId) => {
    const topicPosts = posts.filter((post) => {
      return post.topicId === topicId;
    });
    return topicPosts[topicPosts.length -1];
  }

  const getLastPostAuthor = (topicId) => {
    const authorHandle = getLastPost(topicId)?.author;    
    const authorObj = authors.filter((author) => {
      return author.handle === authorHandle;
    });
    return Object.values(authorObj);  
  };

  
  return (
    <Fragment>
       {userTopics.length ? (
      <Fragment>
        {userTopics.map(topic => (
        <TopicTile key={topic.id} {...topic} postsCount={countPostsByTopic(topic.id)}
        lastPost={getLastPost(topic.id)}
        lastPostAuthor={getLastPostAuthor(topic.id)}
        categoryId={topic.categoryId} />
      ))} 
      </Fragment>         
      ) : (
      <NoContentYet />
    )}
    </Fragment>
  //  <div>no content</div>
  );
};