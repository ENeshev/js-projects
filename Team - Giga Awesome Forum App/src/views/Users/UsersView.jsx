import { Fragment, useEffect, useState } from 'react';
import UserTile from '../../components/UserTile/UserTile';
import {
  getAllUsersData,
  usersFromSnapshot,
} from '../../services/users.services';
import {
  getTopicsFromSnapshot,
  getAllTopics,
} from '../../services/topics.services';
import { getAllPosts, postsFromSnapshot } from '../../services/posts.services';
import { Input } from 'antd';
import './UserView.css'
export default function UsersView() {
  const {TextArea} = Input;
  const [users, setUsers] = useState();
  const [topics, setTopics] = useState();
  const [posts, setPosts] = useState();
  let [filteredUsers, setFilteredUsers] = useState('');

  useEffect(() => {
    getAllUsersData()
      .then((snapshot) => setUsers(usersFromSnapshot(snapshot)))
      .catch((error) => console.log(error));

    getAllTopics()
      .then((snapshot) => setTopics(getTopicsFromSnapshot(snapshot)))
      .catch((error) => console.log(error));

    getAllPosts()
      .then((snapshot) => setPosts(postsFromSnapshot(snapshot)))
      .catch((error) => console.log(error));
  }, []);

  const handleUsers = (e) => {
    const filterUser = users.filter((user) => {
      return (user.handle.toLowerCase()).includes((e.target.value).toLowerCase());
    })
    // setUsers(filterUser);
    setFilteredUsers(filterUser);
  }

  return (
    <>
      <div className='user-search-input'>
        <label htmlFor={TextArea}>User search</label>
        <TextArea rows={1} placeholder={`User name`} onChange={handleUsers} />
      </div>
      <div className='user-total'>
        <h2>{users?.length} users registered</h2>
      </div>
      <div>
        {filteredUsers.length ? (
          <Fragment>
            {filteredUsers &&
            topics &&
            posts &&
            filteredUsers.map((user) => {
              const topicsCount =
                topics.filter((t) => t.author === user.handle).length || 0;
              const postsCount =
                posts.filter((p) => p.author === user.handle).length || 0;
              return (
                <UserTile
                  key={user.uid}
                  handle={user.handle}
                  avatar={`${user.avatar}?size=50x50`}
                  subscribedOn={user.createdOn}
                  threads={topicsCount}
                  posts={postsCount}
                />
              );
            })}
          </Fragment>
        ):(
          <Fragment>
            {users &&
            topics &&
            posts &&
            users.map((user) => {
              const topicsCount =
                topics.filter((t) => t.author === user.handle).length || 0;
              const postsCount =
                posts.filter((p) => p.author === user.handle).length || 0;
              return (
                <UserTile
                  key={user.uid}
                  handle={user.handle}
                  avatar={`${user.avatar}?size=50x50`}
                  subscribedOn={user.createdOn}
                  threads={topicsCount}
                  posts={postsCount}
                />
              );
            })}
          </Fragment>)}       
      </div>
    </>
  );
}
