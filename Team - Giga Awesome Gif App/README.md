# Mega Awesome Gif App

![home screen view](images/screenshots/home-view.png)

### Authors: _Binary Three Dudes_

- #### Emil Neshev
- #### Miroslav Todorov
- #### Milen Nenkov

## Getting started

MAGA(Mega Awesome Gif App) is a single page app built for the Telerik Academy Web Project.  
The main requirements for the app are:

- Must be able to display trending gifs
- Must be able to search gifs
- Must be able to display gif details
- Must be able to upload gifs
- Should be able to display the uploaded gifs
- Should be able to add gifs to favorites
- Should be able to display favorite gifs and should display random gif if nothing favored yet

It has no dependencies needed to run.  
You will need to spin it up with `live-server` or `http-server`.

```
npm i -g live-server
live-server
```

Then open `localhost:8080` in your browser of choice

## Functionality

You can browse gifs supplied by the GIPHY API.  
The home page loads trending gifs.  
You can also use the categories for a quick access or just search for anything you fancy. We implemented autocomplete for the search strings to help out with choosing.  
Also included is the possibility to upload your own gifs to the GIPHY website.  
If you click on any of the gifs, a pop up modal will display some basic details(name and author) and give you the options to add to your favorites or copy the link and share with the world.  
A basic user authentication is built in. If you register and log in, you will have the opportunity to add your own custom categories for ease of access. The user avatar is unique for each user.
The theme can be changed between dark and light.

## Tech Stack used

The project is written in:

- HTML5
- CSS3
- JavaScript Native DOM API

We also used the following libraries:

- Masonry - cascading grid layout tool
- Infinite Scroll - automatically adds more gifs
- FireBase Authentication - basic email/password log in tool
- Lodash - debouncing the search autocomplete dropdown showing up
- Bootstrap5 - pre-built responsive components
- Font Awesome - pre-built icons

---

## App views

All views either get gifs and display them in the main grid or use hardcoded gifs to humorously give you directions for what is going on.

### Main views

- #### Display trending gifs

  ![display trending gifs](./images/screenshots/home-view.png)

- #### Modal displaying gif details and share/favorite icons

  ![modal view with gif details, share and favorite icons](./images/screenshots/modal-view.png)

- #### Search dropdown with autocomplete

  ![search autocomplete dropdown options](./images/screenshots/autocomplete-search-dropdown.png)

- #### Clicking search button with no query entered

  ![no search query given view](./images/screenshots/no-search-value-view.png)

- #### Login / Register view. It also shows the custom categories are only available for logged in users

  ![login and register view](./images/screenshots/login-view.png)

- #### Light Theme home view
  ![light theme view of home page](./images/screenshots/light-theme.png)

### Upload

- #### The main upload view

  ![main upload view](./images/screenshots/upload-view.png)

- #### Waiting for response from server when uploading

  ![upload waiting for response](./images/screenshots/upload-waiting.png)

- #### Error returned from the server when uploading

  ![uploading error](./images/screenshots/upload-error.png)

- #### Successfully uploaded gif

  ![upload success](./images/screenshots/upload-success.png)

- #### Trying to upload a file with wrong extension

  ![wrong file extension warning](./images/screenshots/upload-wrong-file.png)

- #### Trying to upload an empty file

  ![empty file warning](./images/screenshots/upload-empty-file.png)

- #### Trying to upload a file larger than 100MB
  ![bigger than limit size file](./images/screenshots/upload-larger-file.png)

### Categories

- #### Main add category view

  ![add category view](./images/screenshots/add-category-view.png)

- #### Category successfully added

  ![category added](./images/screenshots/category-added.png)

- #### Delete category view

  ![delete category view](./images/screenshots/remove-category-view.png)

- #### Custom categories displayed in the dropdown menu
  ![custom categories in dropdown menu](./images/screenshots/custom-categories-dropdown.png)

### About Page

- #### About Page View
  ![view of about page](images/screenshots/About-Page-View.PNG)
