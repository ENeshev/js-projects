import { constants } from './constants.js';

/**
 * The Query Parameters for API requests
 * @prop {string} api_key The API key for the app
 * @prop {number} limit The Maximum number of returned GIFS
 * @prop {string} q The search query
 * @prop {string} ids The GIF ids separated by comma
 * @prop {number} offset The starting position for the result
 */
export const apiQueryParams = {
  api_key: constants.API_KEY,
  limit: constants.GIF_LIMITER,
  q: '',
  ids: '',
  offset: 0,
};
