/**
 * The constants used in the App
 * @prop {string} API_KEY The GIPHY API Key
 * @prop {number} GIF_LIMITER The Maximum number for returned GIFs
 * @prop {number} BYTES_PER_MB The bite size of a MB
 * @prop {number} MAX_FILE_SIZE The Maximum file size accepted
 * @prop {number} SCROLL_THRESHOLD The scroll threshold used by Infinite Scroll to load more gifs
 * @prop {number} DEBOUNCE_WAIT_MS The wait time in ms for the debounce function
 * @prop {number} INFINITE_SCROLL_THROTTLE_MS The wait time in ms for the InfiniteScroll throttle
 * @prop {number} MODAL_TOAST_HIDE_MS The wait time before the modal toast message disappears
 * @prop {number} MODAL_TOAST_CLEAR_MS The wait time in ms before the toast message wording is reset
 * @prop {number} RESET_LINK_ICON_ANIMATION_MS The wait time in ms before the share link icon transition clears
 * @prop {string} DARK_COLOR The hex code for the dark color in use
 * @prop {string} LIGHT_COLOR The hex code for the light color in use
 */
export const constants = {
  API_KEY: 'NqAki8vG9QXZ2XVk1rFjw2g8obU1JnCO',
  GIF_LIMITER: 24,
  BYTES_PER_MB: 1_048_576,
  MAX_FILE_SIZE: 100,
  SCROLL_THRESHOLD: 100,
  DEBOUNCE_WAIT_MS: 600,
  INFINITE_SCROLL_THROTTLE_MS: 1200,
  MODAL_TOAST_HIDE_MS: 1200,
  MODAL_TOAST_CLEAR_MS: 1500,
  RESET_LINK_ICON_ANIMATION_MS: 1000,
  DARK_COLOR: '#222831',
  LIGHT_COLOR: '#ddd',
};
