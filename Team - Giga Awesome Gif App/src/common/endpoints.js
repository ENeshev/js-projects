/** An object that holds string representation of all endpoints for GIPHY API*/
export const endpoints = {
  SEARCH: 'https://api.giphy.com/v1/gifs/search',
  TRENDING: 'https://api.giphy.com/v1/gifs/trending',
  GET_BY_ID: 'https://api.giphy.com/v1/gifs',
  UPLOAD: 'https://upload.giphy.com/v1/gifs',
  AUTOCOMPLETE: 'https://api.giphy.com/v1/gifs/search/tags',
  RANDOM: 'https://api.giphy.com/v1/gifs/random',
};
