/** Holds URLs for all hardcoded GIFs in use */
export const gifURLs = {
  LEARN_TO_READ:
    'https://media0.giphy.com/media/3o6ZtdGOKZajklFYEo/giphy.gif?cid=18ab49d864d08ee44b2d7eda4e910fbae6bc9aacb11e3818&rid=giphy.gif&ct=g',
  GIVE_ME_NOW:
    'https://media0.giphy.com/media/nh9k1qzeLf99S/giphy.gif?cid=18ab49d8cf3064683db3e9b41e86f718a3595374cb7145a0&rid=giphy.gif&ct=g',
  OOPS: 'https://media3.giphy.com/media/i0oItICmCfTfepABOM/giphy.gif?cid=18ab49d8ec23129ab3e58612668c7b225927c2209a9f633a&rid=giphy.gif&ct=g',
  WRONG:
    'https://media2.giphy.com/media/EWJZAWeAFgpaw/giphy.gif?cid=18ab49d8864da1dd54c46dff87845af4f86664c9fd63cf30&rid=giphy.gif&ct=g',
  SUCCESS:
    'https://media4.giphy.com/media/3otPoS81loriI9sO8o/giphy.gif?cid=18ab49d87550cd3bf891ee7d32cbcc1d741be97cd8abadd5&rid=giphy.gif&ct=g',
  WAITING:
    'https://media0.giphy.com/media/QhjR3MG9ZFfjB6BtIZ/giphy.gif?cid=18ab49d89e8e1b418223f0b0b1de146236e82e5549bc26d7&rid=giphy.gif&ct=g',
  EMPTY:
    'https://media4.giphy.com/media/kHyGifPyLVBZBHcBX7/giphy.gif?cid=18ab49d8de93a386945950d5ee23382b6e19191969151ca6&rid=giphy.gif&ct=g',
  NAME: 'https://media4.giphy.com/media/xT0xeKwam7V2CPL4cg/giphy.gif?cid=ecf05e478t52r2guixb6cqn4v1fes1ll4163z0on1om4l9f7&rid=giphy.gif&ct=g',
  NO_NAME:
    'https://media0.giphy.com/media/x9cDy0c3bOxxwDZmJm/giphy.gif?cid=ecf05e47od5e1k21hu0qchvpl7jj1cagzu2q097irz4d17l6&rid=giphy.gif&ct=g',
  YOU_DID_IT:
    'https://media3.giphy.com/media/6mBnSxxldVZYusjhtP/giphy.webp?cid=ecf05e47lttdyb9ugc3zu9dmg1upafkxxerzixdif1y1548y&rid=giphy.webp&ct=g',
  ARE_YOU_SURE:
    'https://media1.giphy.com/media/Ty9Sg8oHghPWg/giphy.webp?cid=ecf05e47bk6ktb8jqx4xoitx7qwgfh912ax3tz03zomllls3&rid=giphy.webp&ct=g',
  NO_STRING:
    'https://media0.giphy.com/media/5D2vFWphQgS1KDFewK/giphy.gif?cid=790b7611667e7fc29506b9571c5203b58f2764f6b575d946&rid=giphy.gif&ct=g',
  NO_UPLOADS:
    'https://media4.giphy.com/media/UoeaPqYrimha6rdTFV/giphy.gif?cid=18ab49d845857cc5d51175a124c0cd041fa67654cfbec7c5&rid=giphy.gif&ct=g',
};
