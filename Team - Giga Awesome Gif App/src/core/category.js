import { capitalize, localStorageCategories, select, userCategories } from '../utils/utils.js';


/**
 * Checks if the data includes a specific string
 * @param { array} data 
 * @param { string } value 
 * @return { boolean } Returns true / false depends on the result
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const hasCategory = (data, value) => {
  if ( data.map((el) => el.toLowerCase()).includes(value.toLowerCase()) ) {
    return true;
  }
  return false;
};

/**
 * If there are categories saved in the Local storage, the function pass them to @function addNewCategory
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const showCategories = () => {
  const allCategories = localStorageCategories();
  if (allCategories.length) {
    allCategories.map((item) => addNewCategory(item));
  }
};

/**
 * Takes a string. Make the first letter Uppercase and then push it to an array that holds all the
 * elements of the 'categories' property of the Local storage. At the end passes the capitalized
 * string to @function addNewCategory
 * @param { array } data 
 * @param { string } value 
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const addCategoryToStorage = (data, value) => {
  const capitalizeValue = capitalize(value);
  data.push(capitalizeValue);
  data.sort();
  localStorage.setItem(userCategories, JSON.stringify(data));
  addNewCategory(capitalizeValue);
};

/**
 * Make a <li> element with a specific string and append it to a parent element in the HTML.
 * In this case it adds a category in the Category drop-down menu
 * @param { string} name The name of the category to add 
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const addNewCategory = (name) => {
  const newNode = document.createElement('li');
  newNode.classList.add('dropdown-item');
  newNode.classList.add('category-menu');
  newNode.classList.add('personal');
  
  const textNode = document.createTextNode(`${name}`);
  newNode.appendChild(textNode);
  
  const list = document.getElementById('personal');
  list.appendChild(newNode);
};

/**
 * Make a <option> element and append in to its parent element in the HTML.
 * In this case it fills a list with existing categories in the remove category menu
 * @param { string } categoryName 
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const addCategoryToRemoveCategory = (categoryName) => {
  const newOption = document.createElement('option');
  newOption.value = `${categoryName}`;
  newOption.text = `${categoryName}`;

  const selectList = select('#mySelect');
  selectList.add(newOption);
};

/**
 * Gets a specific HTML element and remove if from its parent element
 * @param { HTMLElement } item 
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const delItemFromRemoveCategory = (item) => {
  const selectList = select('#mySelect');
  selectList.remove(item.selectedIndex);
};
