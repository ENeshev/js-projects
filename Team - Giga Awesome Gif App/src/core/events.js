import { endpoints } from '../common/endpoints.js';
import {
  isLocalStorageCategory,
  localStorageCategories,
  select,
  userCategories,
} from '../utils/utils.js';
import { displayHeader } from '../views/header-view.js';
import { generateGifs, getItemHTML } from './infinite-scroll.js';
import { displayUpload } from '../views/Upload/upload-view.js';
import { findPartialString, searchSuggest } from './search.js';
import { apiQueryParams } from '../common/api-queries.js';
import { addCategoryView } from '../views/Category/add-category-view.js';
import {
  addCategoryToRemoveCategory,
  addCategoryToStorage,
  delItemFromRemoveCategory,
  hasCategory,
} from './category.js';
import { addCategoryErrorView } from '../views/Category/add-category-error-view.js';
import { addNewCategoryView } from '../views/Category/add-new-category-view.js';
import { existingCategoryView } from '../views/Category/existing-category-view.js';
import {
  noCategoryAvailableView,
  removeCategoryView,
} from '../views/Category/remove-category-view.js';
import { getFavorites } from './favorites.js';
import { constants } from '../common/constants.js';
import { displayDefaultGif } from '../views/no-upload-default-view.js';
import { noSearchStringView } from '../views/no-search-string-view.js';
import { getUploads } from './uploads.js';
import { resetGrid } from '../helpers/reset-grid.js';
import { aboutPageView } from '../views/About Page/about-page-view.js';
import { updateHeading } from '../helpers/update-heading.js';
import { gifURLs } from '../common/hardcoded-gifs.js';

export const displayTrendingGifs = async () => {
  try {
    const params = { ...apiQueryParams };
    updateHeading('Trending');
    generateGifs(endpoints.TRENDING, params);
  } catch (error) {
    console.log(error.message);
  }
};

export const populateHeader = () => {
  select('header').innerHTML = displayHeader();
};

export const searchGif = async (searchString) => {
  try {
    const params = { ...apiQueryParams, q: searchString };
    updateHeading(`Searching for: ${searchString}`);
    generateGifs(endpoints.SEARCH, params);
  } catch (error) {
    console.log(error.message);
  }
};

export const noSearchString = () => {
  resetGrid();
  select('.grid').innerHTML = noSearchStringView();
};

export const searchPartialStr = async (str) => {
  try {
    const searchPartialData = await findPartialString(str);
    searchPartialData.map((element) => searchSuggest(element.name));
  } catch (error) {
    console.log(error.message);
  }
};

export const displayFavoriteGifs = async () => {
  try {
    const favoriteIds = getFavorites().join(',');
    const params = { ...apiQueryParams, ids: favoriteIds };
    if (favoriteIds) {
      updateHeading('Favorites');
      generateGifs(endpoints.GET_BY_ID, params, getFavorites().length);
    } else {
      const img = await getRandomGif();
      const target = select('.grid');
      resetGrid();

      target.innerHTML = getItemHTML(img);
      const message = document.createElement('div');

      message.innerText = `No favorites Added.\nHere's a random Image.\nMaybe you like it - maybe I like ya!`;
      message.setAttribute('id', 'no-favorites-message');
      target.appendChild(message);
    }
  } catch (error) {
    console.log(error.message);
  }
};

export const displayUploads = async () => {
  try {
    const uploads = getUploads().join(',');
    // console.log(uploads);
    const params = { ...apiQueryParams, ids: uploads };
    if (uploads) {
      updateHeading('Uploaded by YOU!!!');
      generateGifs(endpoints.GET_BY_ID, params, getUploads().length);
    } else {
      select('.grid').innerHTML = displayDefaultGif(gifURLs.NO_UPLOADS);
    }
  } catch (error) {
    console.log(error.message);
  }
};

const getRandomGif = async () => {
  try {
    const link = `${endpoints.RANDOM}?api_key=${constants.API_KEY}`;
    const response = await fetch(link);
    const { data } = await response.json();
    return data;
  } catch (error) {
    return error.message;
  }
};

export const populateUpload = () => {
  select('.grid').innerHTML = displayUpload();
};

export const addCategory = () => {
  select('.grid').innerHTML = addCategoryView();
};

export const addToCategories = (value) => {
  const allCategories = localStorageCategories();
  if (hasCategory(allCategories, value))
    return (select('.grid').innerHTML = existingCategoryView());
  addCategoryToStorage(allCategories, value);
  return (select('.grid').innerHTML = addNewCategoryView());
};

export const addCategoryError = () => {
  select('.grid').innerHTML = addCategoryErrorView();
};

export const removeCategory = () => {
  if (!isLocalStorageCategory()) {
    select('.grid').innerHTML = noCategoryAvailableView();
  } else {
    select('.grid').innerHTML = removeCategoryView();
    const existingCategories = localStorageCategories();
    existingCategories.map((el) => addCategoryToRemoveCategory(el));
  }
};

export const removeCategoryItem = (item, itemValue) => {
  const accountCategoriesList = localStorageCategories();
  const removeFromCategories = accountCategoriesList.filter(
    (el) => el !== itemValue
  );
  localStorage.setItem(userCategories, JSON.stringify(removeFromCategories));
  delItemFromRemoveCategory(item);
  removeCategory();
};

export const aboutPage = () => {
  select('.grid').innerHTML = aboutPageView();
};
