import { select } from '../utils/utils.js';

/**
 * @type {string[]} The array of favorite gifs' ids
 */
let favorites = JSON.parse(localStorage.getItem('favorites') || '[]');

/**
 * Adds the passed ID to local storage if not already there
 * @param {string} gifID The unique ID of the gif
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const addToFavorites = (gifID) => {
  if (idInFavorites(gifID)) {
    return;
  }

  favorites.push(gifID);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * Removes the passed gif ID from the local storage
 * @param {string} gifID
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const removeFromFavorites = (gifID) => {
  favorites = favorites.filter((favID) => favID !== gifID);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * Checks if the ID is already in local storage
 * @param {string} gifID
 * @return {boolean} Wether the gif id is saved in local storage
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const idInFavorites = (gifID) => favorites.includes(gifID);

/**
 * Returns the markup code for the relevant icon based on wether in local storage or not
 * @param {string} gifID
 * @return {string}
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const renderFavoriteIcon = (gifID) => {
  return idInFavorites(gifID)
    ? `<span class="heart" data-gif-id="${gifID}">
    <i class="fa-solid fa-heart fa-beat"></i>
    </span>`
    : `<span class="heart active" data-gif-id="${gifID}">
    <i class="fa-solid fa-heart-crack "></i>
    </span>`;
};

/**
 * Returns a copy of the favorites array
 * @return {string[]} Copy of the array containing all favored gif ids
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const getFavorites = () => [...favorites];

/**
 * Checks if the passed ID is already in local storage and removes/adds it based on result. Selects
 * the favorite icon and updates the inner HTML appropriately
 * @param {string} gifId
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const toggleFavoriteStatus = (gifId) => {
  if (idInFavorites(gifId)) {
    removeFromFavorites(gifId);
  } else {
    addToFavorites(gifId);
  }

  select(`span[data-gif-id="${gifId}"`).innerHTML = renderFavoriteIcon(gifId);
};
