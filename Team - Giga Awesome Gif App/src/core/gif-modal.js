import { select } from '../utils/utils.js';
import { renderFavoriteIcon } from './favorites.js';

/**
 * Modal gives a detail view of the gallery image
 * in a modal fashion. It has "share link" & favorite toggle
 * capability
 * @param {HTMLElement.img} img - the image Object from the
 * tile image
 *
 * @author Emil Neshev <e.neshev@gmail.com>
 */
export const modalView = (img) => {
  const modal = select('#gif-modal');

  // Get the image and insert it inside the modal - use its "alt" text as a caption
  const modalImg = select('#modal-image');
  const captionText = select('#caption');
  const favoriteIcon = select('#favorite-icon');

  const shareIcon = select('#share-icon');

  const originalWidth = img.dataset.originalSize.split('/')[0];
  const originalHeight = img.dataset.originalSize.split('/')[1];

  shareIcon.dataset.gifUrl = img.src;

  // render favorite icon
  favoriteIcon.dataset.gifId = img.dataset.gifId;
  favoriteIcon.innerHTML = renderFavoriteIcon(img.dataset.gifId);

  modal.style.display = 'block';

  modalImg.src = img.dataset.originalUrl;
  modalImg.style.width = `${originalWidth}px`;
  modalImg.style.height = `${originalHeight}px`;
  captionText.innerHTML = img.dataset.gifInfo;

  // Get the <span> element that closes the modal
  const span = select('.close');

  // When the user clicks on <span> (x), close the modal
  span.onclick = () => {
    modal.style.display = 'none';
  };
};
