/* eslint-disable no-undef */
import { constants } from '../common/constants.js';
import { stringifyQueryParams } from '../utils/utils.js';

/**
 * Function used by InfiniteScroll and Masonry to display the GIFs received by the fetch.
 *
 * @param {{ title: string, images: { original: { url:string, width: string, height:string }, fixed_width: { url:string }},
 * user: { display_name: string }, id: string, username: string }} param0 Destructured object returned by the API fetch
 * @return {string} Markup code for the GIF element
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const getItemHTML = ({ title, images, user, id, username }) => {
  return `<div class="photo-item">
    <img class="photo-item__image" src="${
      images.fixed_width.url
    }" alt="GIF by ${user?.display_name}" data-gif-info="${
    title.split('GIF')[0]
  } GIF by ${
    user?.display_name
      ? user.display_name
      : username
      ? username
      : 'an Anonymous User'
  }"
  data-gif-id="${id}" data-original-url=${
    images.original.url
  }/ data-original-size=${images.original.width}/${images.original.height}>
  </div>`;
};

/**
 * Initializes Masonry and InfiniteScroll
 *
 * @param {string} endpoint The endpoint for the fetch request
 * @param {{ api_key: string, limit: number, q: string, ids: string, offset: number }} params
 * Object containing fetch request query parameters
 * @param {number} idCount The number of gif ids in the respective array
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const generateGifs = (endpoint, params, idCount = 0) => {
  const msnry = new Masonry('.grid', {
    itemSelector: '.photo-item',
    columnWidth: '.grid__col-sizer',
    gutter: '.grid__gutter-sizer',
    percentPosition: true,
    stagger: 30,
    // nicer reveal transition
    visibleStyle: { transform: 'translateY(0)', opacity: 1 },
    hiddenStyle: { transform: 'translateY(100px)', opacity: 0 },
  });

  /**
   * Initializes InfiniteScroll. Makes a fetch request to the passed endpoint
   * with the passed parameters and calculates offset based on how many GIFs were loaded so far.
   */
  const infScroll = new InfiniteScroll('.grid', {
    path: function () {
      params.offset = this.loadCount * constants.GIF_LIMITER;
      return `${endpoint}?${stringifyQueryParams(params)}`;
    },
    // load response as JSON
    responseBody: 'json',
    outlayer: msnry,
    status: '.page-load-status',
    history: false,
    scrollThreshold: constants.SCROLL_THRESHOLD,
  });

  // use element to turn HTML string into elements
  const proxyElem = document.createElement('div');

  /** Disables the load on scroll option for 2 seconds to limit the fetch */
  infScroll.on('scrollThreshold', () => {
    infScroll.options.loadOnScroll = false;
    setTimeout(
      () => (infScroll.options.loadOnScroll = true),
      constants.INFINITE_SCROLL_THROTTLE_MS
    );
  });

  infScroll.on('load', ({ data }) => {
    // If loaded more gifs than the length of the ids array, exit
    if (idCount && infScroll.loadCount * idCount > idCount) {
      return;
    }
    // compile body data into HTML
    const itemsHTML = data.map(getItemHTML).join('');
    // convert HTML string into elements
    proxyElem.innerHTML = itemsHTML;
    // append item elements
    const items = proxyElem.querySelectorAll('.photo-item');
    imagesLoaded(items, () => {
      infScroll.appendItems(items);
      msnry.appended(items);
    });
  });

  // load initial page
  infScroll.loadNextPage();
};
