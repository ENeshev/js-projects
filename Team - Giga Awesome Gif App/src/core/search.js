import { constants } from '../common/constants.js';
import { endpoints } from '../common/endpoints.js';
import { select } from '../utils/utils.js';

/**
 * Takes a string, make a list element with its value and append it to a list with suggestions
 * @param { string } value String that comes from GifApi as a search suggestion
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const searchSuggest = (value) => {
  const listItem = document.createElement('li');
  listItem.classList.add('list-group-item');
  listItem.innerHTML = value;
  select('.suggestions').appendChild(listItem);
};


/**
 * Clear all fields and <li> elements that holds search suggestions on each new keypress
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const removeElements = () => {
  const items = document.querySelectorAll('.list-group-item');
  items.forEach((item) => {
    item.remove();
  });
};

/**
 * Gets a string from from search input field and make a fetch to GifApi
 * @async
 * @param { string } text Gets a string from from search input field;
 * @return { Promise } Five different strings that starts with the same value as the @param string
 * 
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const findPartialString = async (text) => {
  const link = `${endpoints.AUTOCOMPLETE}?api_key=${constants.API_KEY}&q=${text}`;
  const { data } = await fetch(link).then((d) => d.json());
  return data;
};
