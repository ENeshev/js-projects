import { constants } from '../common/constants.js';
import { endpoints } from '../common/endpoints.js';
import { select } from '../utils/utils.js';
import { handleEmptyFile } from '../views/Upload/empty-file-view.js';
import { clearError } from '../views/Upload/reset-upload-view.js';
import { handleSuccess } from '../views/Upload/success-upload-view.js';
import { handleUploadError } from '../views/Upload/upload-error-view.js';
import { handleUploadWait } from '../views/Upload/waiting-upload-view.js';
import { handleWrongFile } from '../views/Upload/wrong-file-format.js';
import { handleWrongSizeError } from '../views/Upload/wrong-size-view.js';

/**
 * Adds the passed GIF ID to local storage if not already included
 * @param {string} gifId The GIF IF to add to local storage after validation
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const addToUploadStorage = (gifId = '') => {
  const uploads = JSON.parse(localStorage.getItem('uploads')) || [];
  if (uploads.includes(gifId)) {
    return;
  }

  uploads.push(gifId);
  localStorage.setItem('uploads', JSON.stringify(uploads));
};

/**
 * Creates a new formData with the api_key and file. Adds a
 * click event listener to the button and sends a POST fetch to the API.
 * Checks response status and calls the correct method depending on result
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const uploadFile = () => {
  const input = select('#gif-upload');

  const formData = new FormData();
  formData.append('file', input.files[0]);
  formData.append('api_key', constants.API_KEY);

  select('#upload-btn').addEventListener('click', async () => {
    if (input.files[0]) {
      handleUploadWait();
      try {
        const response = await fetch(`${endpoints.UPLOAD}`, {
          method: 'POST',
          body: formData,
        });

        const { data, meta } = await response.json();

        if (response.ok) {
          addToUploadStorage(data.id);
          return handleSuccess();
        }
        return handleUploadError(meta.description);
      } catch (error) {
        return console.log(error.message);
      }
    } else {
      return handleWrongFile();
    }
  });
};

/**
 * Selects the file and checks size and file type. Calls
 * the relevant method that handles the result.
 *
 * @return {void}
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleUpload = () => {
  clearError();

  const file = select('#gif-upload').files[0];
  const fileSizeMB = +file?.size / constants.BYTES_PER_MB;

  if (fileSizeMB > constants.MAX_FILE_SIZE) {
    return handleWrongSizeError(fileSizeMB);
  }

  if (file?.size === 0) return handleEmptyFile();

  if (file?.type.includes('gif') || file?.type.includes('video')) {
    return uploadFile();
  }
  return handleWrongFile();
};

/**
 * @return {string[]} The array of uploaded GIF's IDs
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const getUploads = () => [
  ...JSON.parse(localStorage.getItem('uploads') || '[]'),
];
