/**
 * Makes a fetch request to the Avatars API and returns the SVG text
 * @async
 * @param {string} uid The user's unique ID
 * @return {Promise} The SVG representation of the user's avatar
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const getUserAvatar = async (uid) => {
  try {
    const avatarResponse = await fetch(
      `https://avatars.dicebear.com/api/bottts/${uid}.svg?r=50`
    );
    const avatar = await avatarResponse.text();
    return avatar;
  } catch (e) {
    return '';
  }
};
