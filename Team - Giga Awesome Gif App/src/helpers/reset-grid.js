/* eslint-disable no-undef */
import { hideLoginForm } from '../utils/firebase-ui.js';
import { select } from '../utils/utils.js';

/** Checks if there is an active instance of Infinite Scroll and destroys it. Clears the grid
 * contents and fills it up again with the sizing divs.
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const resetGrid = () => {
  hideLoginForm();
  select('#grid-heading').innerText = '';

  const scroll = InfiniteScroll.data('.grid');
  if (scroll) {
    scroll.destroy();
  }

  const grid = select('.grid');
  grid.innerHTML = '';
  grid.style.maxWidth = '60vw';
  grid.style.height = '80vh';
  grid.style.marginTop = '0';

  grid.innerHTML = populateGridDivs();
};

/**
 *
 * @return {string} The markup code for the column and gutter sizer divs
 * used by Masonry
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const populateGridDivs = () => {
  return `
<div class="grid__col-sizer"></div>
<div class="grid__gutter-sizer"></div>
  `;
};
