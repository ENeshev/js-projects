import { constants } from '../common/constants.js';
import { selectAll, select } from '../utils/utils.js';

/**
 * Selects all relevant DOM objects and updates their styles to match the light theme
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const makeLightTheme = () => {
  selectAll('.btn-outline-light').forEach((btn) => {
    btn.classList.add('btn-outline-dark');
    btn.classList.remove('btn-outline-light');
  });
  selectAll('label').forEach((label) => {
    label.classList.toggle('light');
  });
  select('.navbar').classList.add('navbar-light');
  select('.navbar').classList.add('bg-light');
  select('.navbar').classList.remove('navbar-dark');
  select('.navbar').classList.remove('bg-black');
  select('body').classList.toggle('light');
  select('footer').classList.toggle('light');
  select('#grid-heading').classList.toggle('dark');

  const userInfo = select('a.user-info');
  if (userInfo) {
    userInfo.style.color = constants.DARK_COLOR;
  }

  const loginForm = select('#login-form');

  if (loginForm) {
    loginForm.classList.remove('dark');
    loginForm.classList.add('light');
    select('form > h1').style.color = constants.DARK_COLOR;
  }

  select('#about').style.color = constants.DARK_COLOR;
};

/**
 * Selects all relevant DOM objects and updates their styles to match the dark theme
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
const makeDarkTheme = () => {
  selectAll('.btn-outline-dark').forEach((btn) => {
    btn.classList.add('btn-outline-light');
    btn.classList.remove('btn-outline-dark');
  });
  selectAll('label').forEach((label) => {
    label.classList.toggle('light');
  });
  select('.navbar').classList.add('navbar-dark');
  select('.navbar').classList.add('bg-black');
  select('.navbar').classList.remove('navbar-light');
  select('.navbar').classList.remove('bg-light');
  select('body').classList.toggle('light');
  select('footer').classList.toggle('light');
  select('#grid-heading').classList.toggle('dark');

  const userInfo = select('a.user-info');
  if (userInfo) {
    userInfo.style.color = constants.LIGHT_COLOR;
  }

  const loginHeading = select('form > h1');
  if (loginHeading) {
    loginHeading.style.color = constants.LIGHT_COLOR;
  }

  select('#about').style.color = constants.LIGHT_COLOR;
};

/**
 * Checks if the theme switch is checked and calls the relevant theme function
 * @return {void}
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleThemeSwitch = () => {
  const switchBtn = select('#switch-input');

  if (switchBtn.checked) {
    return makeLightTheme();
  }

  return makeDarkTheme();
};
