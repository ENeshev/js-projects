import { select } from '../utils/utils.js';

/**
 * Selects the grid heading and updates the inner text to match the currently opened view
 * @param {string} term The term to display in the heading
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const updateHeading = (term) => {
  select('#grid-heading').innerText = term;
};
