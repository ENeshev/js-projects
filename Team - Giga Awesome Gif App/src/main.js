import {
  addCategory,
  addCategoryError,
  addToCategories,
  displayTrendingGifs,
  displayFavoriteGifs,
  displayUploads,
  populateHeader,
  populateUpload,
  removeCategory,
  removeCategoryItem,
  searchGif,
  noSearchString,
  aboutPage,
} from './core/events.js';
import { resetGrid } from './helpers/reset-grid.js';
import { handleUpload } from './core/uploads.js';
import { delayAutocomplete, select } from './utils/utils.js';
import { removeElements } from './core/search.js';
import { modalView } from './core/gif-modal.js';
import { toggleFavoriteStatus, getFavorites } from './core/favorites.js';
import {
  createAccount,
  loginEmailPassword,
  logout,
  monitorAuthState,
} from './utils/firebase-auth.js';
import { showLoginForm } from './utils/firebase-ui.js';
import { showCategories } from './core/category.js';
import { modalViewMessage } from './views/Modal/icon-toast-view.js';
import { handleThemeSwitch } from './helpers/theme-switch.js';
import { constants } from './common/constants.js';

monitorAuthState();

document.addEventListener('DOMContentLoaded', () => {
  populateHeader();
  showCategories();
  displayTrendingGifs();

  document.addEventListener('click', (e) => {
    removeElements();

    if (e.target.id === 'login-link') {
      e.preventDefault();
      showLoginForm();
    }

    if (e.target.id === 'btnLogin') {
      loginEmailPassword();
      e.preventDefault();
    }

    if (e.target.id === 'btnSignup') {
      createAccount();
      e.preventDefault();
    }

    if (e.target.id === 'btnLogout') {
      logout();
    }

    if (e.target.id === 'trending') {
      resetGrid();
      displayTrendingGifs();

      e.preventDefault();
    }

    if (e.target.id === 'show-uploads') {
      resetGrid();
      e.preventDefault();
      displayUploads();
    }

    if (e.target.id === 'search-btn') {
      resetGrid();
      const searchString = select('#search-input').value.trim();
      searchString ? searchGif(searchString) : noSearchString();
      e.preventDefault();
    }

    if (e.target.id === 'upload') {
      resetGrid();
      populateUpload();
      select('#gif-upload').addEventListener('input', handleUpload);
      e.preventDefault();
    }

    // Listen for every letter typed in the search box
    if (e.target.className === 'list-group-item') {
      resetGrid();
      const partialString = e.target.textContent;
      searchGif(partialString);
      e.preventDefault();
      select('#search-input').value = '';
    }

    // Listen for Gif Gallery image click
    if (e.target.className === 'photo-item__image') {
      // render A detailed info in modal + favorite icon
      modalView(e.target);
    }

    // Listen for favorite icon click
    if (e.target.parentElement?.classList.contains('heart')) {
      toggleFavoriteStatus(e.target.parentElement.dataset.gifId);

      const isImageInFavorite = getFavorites().includes(
        e.target.parentElement.dataset.gifId
      );
      modalViewMessage(
        isImageInFavorite ? 'Added to Favorites' : 'Removed from Favorites'
      );
    }

    // Listen for share gif icon click- copy to clipboard
    if (e.target.parentElement?.id === 'share-icon') {
      select('.fa-link').classList.add('transform-link');
      modalViewMessage('Linked Copied to Clip Board');

      setTimeout(
        () => select('.fa-link').classList.remove('transform-link'),
        constants.RESET_LINK_ICON_ANIMATION_MS
      );
      navigator.clipboard.writeText(e.target.parentElement.dataset.gifUrl);
    }

    // Listen from Favorite click
    if (e.target.id === 'favorites') {
      e.preventDefault();
      resetGrid();
      displayFavoriteGifs();
    }

    // Listen for selected category from the category menu
    if (e.target.classList.contains('category-menu')) {
      resetGrid();
      const category = e.target.textContent;
      searchGif(category);
      e.preventDefault();
    }

    // Listen for Add Category click
    if (e.target.classList.contains('add-category-menu')) {
      resetGrid();
      addCategory();
      e.preventDefault();
    }

    // Listen for Add Category Button click
    if (e.target.classList.contains('add-category-btn')) {
      const categoryName = select('#new-category-name').value;
      categoryName ? addToCategories(categoryName) : addCategoryError();
      populateHeader();
      showCategories();
      e.preventDefault();
    }

    // Listen for Remove Category Button click
    if (e.target.classList.contains('remove-category-menu')) {
      resetGrid();
      removeCategory();
    }

    // Listen for remove category Delete btn click
    if (e.target.id === 'del-category-btn') {
      const selectItem = select('#mySelect');
      const selectItemValue = selectItem.value;
      removeCategoryItem(selectItem, selectItemValue);
      populateHeader();
      showCategories();
    }

    // Listen for add More category click
    if (e.target.id === 'add-more-category') {
      addCategory();
      e.preventDefault();
    }

    // Listen for add More category view -> go to home page click
    if (e.target.id === 'home-page') {
      resetGrid();
      displayTrendingGifs();
      e.preventDefault();
    }

    if (e.target.id === 'switch-input') {
      handleThemeSwitch();
    }

    if (e.target.id === 'about') {
      resetGrid();
      aboutPage();
      e.preventDefault();
    }

    select('#search-input').value = '';
  });

  document.addEventListener('keypress', (e) => {
    if (e.target.id === 'search-input') {
      removeElements();
      delayAutocomplete();
    }
  });
});
