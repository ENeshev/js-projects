import {
  showLoginState,
  showLoginError,
  handleLogout,
  hideLoginForm,
} from './firebase-ui.js';
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.6.11/firebase-app.js';
import {
  getAuth,
  onAuthStateChanged,
  signOut,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from 'https://www.gstatic.com/firebasejs/9.6.11/firebase-auth.js';
import { select } from './utils.js';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyDLuNPQTSgym_i-OhlbgCOmZecSqlsvZWs',
  authDomain: 'mega-awesome-gif-app.firebaseapp.com',
  projectId: 'mega-awesome-gif-app',
  storageBucket: 'mega-awesome-gif-app.appspot.com',
  messagingSenderId: '250255625599',
  appId: '1:250255625599:web:6997bf4d1d3b575e11de4a',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication
const auth = getAuth(app);

/**
 * Handles logging in with Email and Password
 * @async Asynchronously signs in the user using an email and password.
 * Fails if wrong details given
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const loginEmailPassword = async () => {
  const loginEmail = select('#txtEmail').value;
  const loginPassword = select('#txtPassword').value;

  try {
    await signInWithEmailAndPassword(auth, loginEmail, loginPassword);
  } catch (error) {
    showLoginError(error);
  }
};

/**
 * Handles creating new user accounts
 * @async On successful creation of the user account, this user will also be signed in to your application.
 * User account creation can fail if the account already exists or the password is invalid.
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const createAccount = async () => {
  const email = select('#txtEmail').value;
  const password = select('#txtPassword').value;

  try {
    await createUserWithEmailAndPassword(auth, email, password);
  } catch (error) {
    showLoginError(error);
  }
};

/**
 * Adds an observer for changes to the user's sign-in state.
 * @async Checks if user is logged in and calls the relevant methods
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const monitorAuthState = async () => {
  await onAuthStateChanged(auth, (user) => {
    if (user) {
      showLoginState(user);
      hideLoginForm();
    } else {
      handleLogout();
    }
  });
};

/**
 * Signs out the current user
 * @async
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const logout = async () => {
  await signOut(auth);
};
