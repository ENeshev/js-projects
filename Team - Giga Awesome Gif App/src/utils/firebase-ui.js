import { AuthErrorCodes } from 'https://www.gstatic.com/firebasejs/9.6.11/firebase-auth.js';
import { getUserAvatar } from '../helpers/avatar-fetch.js';
import { resetGrid } from '../helpers/reset-grid.js';
import { select, selectAll } from './utils.js';

/**
 * Resets the grid and makes the login form visible
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const showLoginForm = () => {
  resetGrid();
  select('#login-form').style.display = 'block';
};

/**
 * Selects the login form and hides it
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const hideLoginForm = () => {
  select('#login-form').style.display = 'none';
};

/**
 * Receives the error after login failure. Selects the login error div and displays the message
 * @param {string} error The error returned if logging in fails
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const showLoginError = (error) => {
  if (error.code === AuthErrorCodes.INVALID_PASSWORD) {
    select('#lblLoginErrorMessage').innerHTML = `Wrong password. Try again.`;
  } else if (error.code === AuthErrorCodes.USER_DELETED) {
    select('#lblLoginErrorMessage').innerHTML = `You need to register first`;
  } else {
    select('#lblLoginErrorMessage').innerHTML = `Error: ${error.message}`;
  }
};

/**
 * On logout, resets the grid and hides the category controls.
 * Displays the login / signup button in the navbar
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleLogout = () => {
  resetGrid();
  selectAll('.personal').forEach((el) => (el.style.display = 'none'));
  select(
    '.user-info'
  ).innerHTML = `<button class="btn btn-outline-light" type="submit" id="login-link">Log In</button>`;
};

/**
 * Clears out the login error displayed
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const hideLoginError = () => {
  select('.login-error').innerHTML = '';
};

/**
 * Updates the navbar with currently logged user's email and unique ID.
 * Passes the UID to get the user's avatar. Displays the SVG and username in navbar
 * @param {{ email: string, uid: string }} user A User Account
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const showLoginState = async (user) => {
  const avatar = await getUserAvatar(user.uid);
  selectAll('.personal').forEach((el) => (el.style.display = 'block'));
  select(
    '.user-info'
  ).innerHTML = `<a class="dropdown-toggle user-info" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
  <svg class="user-avatar">${avatar ? avatar : ''}</svg> Hi, ${
    user.email.split('@')[0]
  }
  </a>
  
  <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <li><a class="dropdown-item" id="btnLogout" href="">Log Out</a></li>
  </ul>`;
};
