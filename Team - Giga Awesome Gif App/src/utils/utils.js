/* eslint-disable no-undef */
import { constants } from '../common/constants.js';
import { searchPartialStr } from '../core/events.js';

export const select = (selector) => document.querySelector(selector);

export const selectAll = (selector) => [...document.querySelectorAll(selector)];

export const userCategories = 'categories';

/**
 * Utility function to transform all key/value pairs into valid request parameter strings
 * concatenated with &
 *
 * @param {{ api_key: string, limit: number, q: string, ids: string, offset: number }} queries
 * The request parameters needed for all fetch requests
 * @return {string} The correctly formatted query parameters
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const stringifyQueryParams = (queries) =>
  Object.entries(queries)
    .map(([key, value]) => `${key}=${value}`)
    .join('&');


/**
 * Gets a string and change the first letter to Uppercase
 * @param { string } String A destructured string which will be capitalized 
 * @return { string } The same string with Uppercase
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const capitalize = ([first, ...rest]) => {
  return first.toUpperCase() + rest.join('').toLowerCase();
};

/**
 * Checks if there are items in 'categories' property of the Local Storage
 * @return { boolean } True / False
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const isLocalStorageCategory = () => {
  const allCategories = JSON.parse(
    localStorage.getItem(userCategories) || '[]'
  );
  if (allCategories.length) {
    return true;
  }
  return false;
};


/**
 * @return { array } Array with the content of 'categories' property of the Local Storage
 * @author Miroslav Todorov <miro_todorov_@abv.bg>
 */
export const localStorageCategories = () => {
  return JSON.parse(localStorage.getItem(userCategories) || '[]');
};

/**
 * Delays calling the autocomplete search result function for 1sec after last input
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const delayAutocomplete = _.debounce(() => {
  const partialStr = select('#search-input').value;
  searchPartialStr(partialStr);
}, constants.DEBOUNCE_WAIT_MS);
