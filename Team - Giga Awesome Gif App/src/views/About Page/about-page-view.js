export const aboutPageView = () => {
  return `
  <div class="about-page">
  <div class="team-name">
    <h2>Binary Three Dudes Team</h2>
  </div>
  <div class="all-team-members">
    <div class="team-member">
      <div class="speakers">
        <div class="spk-img">
          <img class="about-img-fluid" src="../../../images/Team Members/Milen pic.jpeg" alt="trainer-img" alt="trainer-img" />
        </div>
        <div class="spk-info">
          <h3>Milen Nenkov</h3>
          <p>CTO, BTDT Inc</p>
        </div>
      </div>
    </div>
    <div class="team-member">
      <div class="speakers">
        <div class="spk-img">
          <img class="about-img-fluid" src="../../../images/Team Members/Emo.jpeg" alt="trainer-img" />
        </div>
        <div class="spk-info">
          <h3>Emil Neshev</h3>
          <p>CEO, BTDT Inc</p>
        </div>
      </div>
    </div>
    <div class="team-member">
      <div class="speakers">
        <div class="spk-img">
          <img class="about-img-fluid" src="../../../images/Team Members/Miro.png" alt="trainer-img" />
        </div>
        <div class="spk-info">
          <h3>Miroslav Todorov</h3>
          <p>Board Chairman, BTDT Inc</p>
        </div>
      </div>
    </div>
  </div>
  <div class="all-team-members">
    <div class="team-member">
      <div class="speakers">
        <div class="spk-img">
          <img class="about-img-fluid" src="https://bootdey.com/img/Content/avatar/avatar4.png" alt="trainer-img" />
        </div>
        <div class="spk-info">
          <h3>Stoyan Peshev</h3>
          <p>Team Lead, BTDT Inc</p>
          <p>Devs Team</p>
        </div>
      </div>
    </div>
    <div class="team-member">
      <div class="speakers">
        <div class="spk-img">
          <img class="about-img-fluid" src="https://bootdey.com/img/Content/avatar/avatar3.png" alt="trainer-img" />
        </div>
        <div class="spk-info">
          <h3>Nadezhda Hristova</h3>
          <p>Team Lead, BTDT Inc</p>
          <p>Devs Team</p>
        </div>
      </div>
    </div>
  </div>
</div>
  `;
};
