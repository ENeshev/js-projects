import { gifURLs } from '../../common/hardcoded-gifs.js';

export const addCategoryErrorView = () => {
  return `
  <div class="add-category-section grid">
    <img class="give-me-gif" src="${gifURLs.NO_NAME}">
    <div class="add-err-msg">
      <p>No category name detected !!!</p>
      <p>Please enter a category name here:</p>
    </div>
    <div class="category-group">
        <label for="new-category-name">Category Name:</label>
        <input type="text" id="new-category-name" name="category-name">
        <input class="btn btn-outline-light add-category-btn" type="submit" value="Submit">
    </div>
  </div>
  `;
};
