import { gifURLs } from '../../common/hardcoded-gifs.js';

export const addCategoryView = () => {
  return `
  <div class="add-category-section grid">
    <img class="give-me-gif" src="${gifURLs.NAME}">
    
      <div class="category-group">
        <label for="new-category-name">Category Name:</label>
        <input type="text" id="new-category-name" autocomplete="off" name="category-name">
        <input class="btn btn-outline-light add-category-btn" type="submit" value="Submit">
            
      </div>
  </div>
  `;
};
