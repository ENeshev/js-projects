import { gifURLs } from '../../common/hardcoded-gifs.js';

export const addNewCategoryView = () => {
  return `
  <div class="add-category-section grid">
    <img class="give-me-gif" src="${gifURLs.YOU_DID_IT}">
    
    <div class="add-new-category-view">      
      <button type="button" class="btn btn-outline-light btn-lg" id="add-more-category">Add more categories</button>
      <button type="button" class="btn btn-outline-light btn-lg" id="home-page">Back to Home Page</button>     
  </div>
  </div>  
  `;
};
