import { gifURLs } from '../../common/hardcoded-gifs.js';

export const existingCategoryView = () => {
  return `
  <div class="add-category-section grid">
    <img class="give-me-gif" src="${gifURLs.NO_NAME}">
    <div>
      <div class="add-err-msg">
      <p class="add-err-msg">The category is already in your list</p>
      </div>
    </div>
      <div class="category-group">
        <label for="new-category-name">Category Name:</label>
        <input type="text" id="new-category-name" name="category-name">
        <input class="btn btn-outline-light add-category-btn" type="submit" value="Submit">
      </div>
  </div>
  `;
};
