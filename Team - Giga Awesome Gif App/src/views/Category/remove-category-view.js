import { gifURLs } from '../../common/hardcoded-gifs.js';

export const removeCategoryView = () => {
  return `
  <div class="add-category-section grid">
    <img class="give-me-gif" src="${gifURLs.ARE_YOU_SURE}">
    
      <div class="category-group">
        <div class="remove-category-select">
        
          <select id="mySelect" class="form-select" aria-label="Default select example">
            <option selected>Select a category to remove</option>
          </select> 
      
          
        </div> 
        <div class="remove-category-select">
          <button id="del-category-btn" type="button" class="btn btn-outline-light">Delete</button>
        </div>          
      </div>
  </div>
  `;
};

// Returns view that says that there are no categories to remove
export const noCategoryAvailableView = () => {
  return `
  <div class="add-category-section grid">
    <img class="give-me-gif" src="${gifURLs.ARE_YOU_SURE}">
    
      <div class="category-group">
        <div class="remove-category-select">
          <select class="form-select" aria-label="Default select example">
            <option selected>No categories to remove</option>
          </select> 
        </div> 
        <div class="remove-category-select">
          <button id="del-category-btn" type="button" class="btn btn-outline-light">Delete</button>
        </div>          
      </div>
  </div>
  `;
};
