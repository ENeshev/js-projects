import { constants } from '../../common/constants.js';

/* eslint-disable no-undef */
export const modalViewMessage = (message) => {
  const modalMessage = document.getElementById('icon-message');
  modalMessage.innerHTML = renderMessage(message);
  showModalToast();
  setTimeout(() => {
    modalMessage.innerHTML = '';
  }, constants.MODAL_TOAST_CLEAR_MS);
};

const renderMessage = (message) => {
  return `
<div class="toast-div position-relative" style="z-index: 999">
  <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay=${constants.MODAL_TOAST_HIDE_MS}>
    <div class="toast-body">
    ${message}
    </div>
  </div>
</div>`;
};

const showModalToast = () => {
  const toastElList = [].slice.call(document.querySelectorAll('.toast'));
  const toastList = toastElList.map(function (toastEl) {
    return new bootstrap.Toast(
      toastEl,
      `data-bs-delay=${constants.MODAL_TOAST_HIDE_MS}`
    );
  });
  toastList.forEach((toast) => toast.show());
};
