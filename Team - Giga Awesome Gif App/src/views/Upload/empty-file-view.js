import { gifURLs } from '../../common/hardcoded-gifs.js';
import { select } from '../../utils/utils.js';

/**
 * Changes the displayed GIF and info label to notify the file
 * is empty
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleEmptyFile = () => {
  select('.give-me-gif').src = gifURLs.EMPTY;
  select('.upload-info').innerText =
    'UMMM, Why You Trying To Trick Me With Your Empty File???';
  select('.upload-info').classList.add('file-size-error');
};
