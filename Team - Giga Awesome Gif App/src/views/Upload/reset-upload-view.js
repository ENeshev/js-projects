import { gifURLs } from '../../common/hardcoded-gifs.js';
import { select } from '../../utils/utils.js';

/**
 * Changes the displayed GIF and info label to default state
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const clearError = () => {
  select('.give-me-gif').src = gifURLs.GIVE_ME_NOW;
  select('.upload-info').innerText =
    'Please select a .gif or a video file up to 100MB';
  select('.upload-info').classList.remove('file-size-error');
};
