import { gifURLs } from '../../common/hardcoded-gifs.js';
import { select } from '../../utils/utils.js';

/**
 * Changes the displayed GIF and info label to notify the file
 * is successfully uploaded
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleSuccess = () => {
  select('.give-me-gif').src = gifURLs.SUCCESS;
  select('.upload-info').innerText = 'YOU DID IT. YOU ACTUALLY DID IT';
  select('.upload-info').classList.remove('file-size-error');
};
