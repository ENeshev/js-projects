import { gifURLs } from '../../common/hardcoded-gifs.js';
import { select } from '../../utils/utils.js';

/**
 * Changes the displayed GIF and info label to notify there is
 * an response status code is not OK
 * @param {string} statusMessage The error description returned
 * by the fetch
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleUploadError = (statusMessage) => {
  select('.give-me-gif').src = gifURLs.OOPS;
  select('.upload-info').innerText = `Error Error Error: ${statusMessage}`;
  select('.upload-info').classList.add('file-size-error');
};
