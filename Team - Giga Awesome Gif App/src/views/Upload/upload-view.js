import { gifURLs } from '../../common/hardcoded-gifs.js';

export const displayUpload = () => {
  return `
  <div class="upload-section grid">
    <img class="give-me-gif" src="${gifURLs.GIVE_ME_NOW}">

    <div class="input-group mb-3">
      <input type="file" class="form-control" name="upload-input" id="gif-upload" aria-describedby="gif-upload" aria-label="Upload" accept="video/*,.gif">
      <button class="btn btn-outline-light" type="button" id="upload-btn">Upload Me NOW!!!</button>
    </div>

    <div class="upload-info form-text">Please select a .gif or a video file up to 100MB</div>
  </div>
  `;
};
