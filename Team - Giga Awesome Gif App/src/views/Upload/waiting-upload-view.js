import { gifURLs } from '../../common/hardcoded-gifs.js';
import { select } from '../../utils/utils.js';

/**
 * Changes the displayed GIF and info label to notify the file
 * is being uploaded and response has not come back yet
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleUploadWait = () => {
  select('.give-me-gif').src = gifURLs.WAITING;
  select('.upload-info').innerText = 'BEEP BOOP BOP. WAITING. FOR. RESPONSE';
  select('.upload-info').classList.remove('file-size-error');
};
