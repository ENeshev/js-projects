import { gifURLs } from '../../common/hardcoded-gifs.js';
import { select } from '../../utils/utils.js';

/**
 * Changes the displayed GIF and info label to notify the file
 * is wrong format
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleWrongFile = () => {
  select('.give-me-gif').src = gifURLs.WRONG;
  select('.upload-info').innerText =
    'Can\t you read? I SAID GIF OR MOVIE FILE!!!';
  select('.upload-info').classList.add('file-size-error');
};
