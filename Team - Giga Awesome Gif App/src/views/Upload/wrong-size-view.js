import { gifURLs } from '../../common/hardcoded-gifs.js';
import { select } from '../../utils/utils.js';

/**
 * Changes the displayed GIF and info label to notify the file is larger
 *  than the set limit
 * @param {number} fileSize The File size in MBs
 *
 * @author Milen Nenkov <nenkovmilen@gmail.com>
 */
export const handleWrongSizeError = (fileSize) => {
  select('.give-me-gif').src = gifURLs.LEARN_TO_READ;
  select(
    '.upload-info'
  ).innerText = `You have got to learn to read. I SAID NO MORE THAN 100MB and your file is ${fileSize.toFixed(
    0
  )}MB, DUUUH`;
  select('.upload-info').classList.add('file-size-error');
};
