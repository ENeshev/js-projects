import { monitorAuthState } from '../utils/firebase-auth.js';
import { isLocalStorageCategory } from '../utils/utils.js';

export const displayHeader = () => {
  return `
  <nav class="navbar navbar-expand-xl navbar-dark bg-black">
  <div class="container-fluid">
    <a class="navbar-brand" href="" id="team-logo" >
      <img id="trending"
        src="./images/team logo.png"
        width="35"
        height="35"
        alt="The Binary Trio logo"
      />
    </a>
    <button
      class="navbar-toggler"
      type="button"
      data-bs-toggle="collapse"
      data-bs-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      <!-- <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="" id="trending">Trending</a>
        </li> --->
        <li class="nav-item">
          <a class="nav-link" href="" id="favorites">Favorites</a>
        </li>
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href=""
            id="navbarDropdown"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Upload
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li>
              <a class="dropdown-item" id="upload" href="">Upload New Image</a>
            </li>
            <li><a class="dropdown-item" id="show-uploads" href="">Show Uploads</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href=""
            id="navbarDropdownCategories"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            Categories
          </a>
          <ul class="dropdown-menu" id="category-menu" aria-labelledby="navbarDropdown">
            <div>
              <li><a class="dropdown-item category-menu" id="search-animals" href="">Animals</a></li>
                <li>
                  <a class="dropdown-item category-menu" id="search-cartoons" href="">Cartoons & Comics</a>
                </li>
              <li><a class="dropdown-item category-menu" id="seach-emotions" href="">Emotions</a></li>
              <li><a class="dropdown-item category-menu" id="search-food" href="">Food & Drink</a></li>
              <li><a class="dropdown-item category-menu" id="search-memes" href="">Memes</a></li>
              <li><a class="dropdown-item category-menu" id="search-reactions" href="">Reactions</a></li>
            </div>
            
            <div class="personal" id="personal">
              <li>${
                !isLocalStorageCategory()
                  ? ``
                  : `<hr class="dropdown-divider personal" id="categoryDivider"/>`
              }</li>
            </div>
            <div class="personal">
              <li><hr class="dropdown-divider personal"></li>
              <li><a class="dropdown-item add-category-menu" href="#">Add Category</a></li>
              <li><a class="dropdown-item remove-category-menu" href="#">Remove Category</a></li>
            </div>  
          </ul>
        </li>
      </ul>
      <form class="d-flex" id="search-form">
        <input
          class="form-control me-2"
          type="search"
          placeholder="Search"
          aria-label="Search"
          id="search-input"
          autocomplete="off"
        />
        
        <span class="magnifying-glass-icon">
        <i class="fa-solid fa-magnifying-glass" id="search-btn"></i>
        </span>
        <ul class="suggestions"></ul>
      </form>
      <div class="dropdown user-info">
              ${monitorAuthState()}
      </div>
    </div>
  </div>
</nav>
  `;
};
