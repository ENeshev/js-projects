import { gifURLs } from '../common/hardcoded-gifs.js';

export const noSearchStringView = () => {
  return `
  <div class="add-category-section grid">
    <img class="give-me-gif" src="${gifURLs.NO_STRING}">
    <div>
      <p> In order to use the Search functionality, you should type type something, dude !!!</p>
    </div>
    
      
  </div>  
  `;
};
