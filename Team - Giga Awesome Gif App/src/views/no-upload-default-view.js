export const displayDefaultGif = (url) => {
  return `
  <div id="defaultGif-view">
    <img class="photo-item__image" src="${url}" id="no-uploads"
    data-/>
    <div style="margin: 0 auto">...Someone forgot to upload an image.
    <br>You are not stupid.<br>I said Someone!</div>
  </div>
  `;
};
